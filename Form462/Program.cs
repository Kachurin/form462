﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text;

namespace Form462
{
    /// <summary>
    /// Статический класс для формирования ответов в терминах модели
    /// </summary>
    public static class StringsTemplateGenerator
    {
        /// <summary>
        /// Обозначение новой строки
        /// </summary>
        public const string newLine = "\r\n";
        /// <summary>
        /// Обозначение отступа
        /// </summary>
        public const string tab = "\t";
        /// <summary>
        /// Отступ определённой длины
        /// </summary>
        /// <param name="_cnt">Количество отступов в строке</param>
        /// <returns></returns>
        public static string tabN(int _cnt)
        {
            if (_cnt < 0)
                return null;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < _cnt; i++)
                sb.Append(tab);
            return sb.ToString();
        }
        /// <summary>
        /// Падеж
        /// </summary>
        public enum Pad : byte {
            /// <summary>
            /// Именительный
            /// </summary>
            Im,
            /// <summary>
            /// Родительный
            /// </summary>
            Rd,
            /// <summary>
            /// Дательный
            /// </summary>
            Dt,
            /// <summary>
            /// Винительный
            /// </summary>
            Vn,
            /// <summary>
            /// Творительный
            /// </summary>
            Tv,
            /// <summary>
            /// Предложный
            /// </summary>
            Pr
        };
        /// <summary>
        /// Род или число
        /// </summary>
        public enum RdM : byte {
            /// <summary>
            /// Мужской род, единственной число
            /// </summary>
            m,
            /// <summary>
            /// Женский род единственной число
            /// </summary>
            j,
            /// <summary>
            /// Средний род, единственной число
            /// </summary>
            s,
            /// <summary>
            /// Множественное число, род не определяем
            /// </summary>
            M
        };
        /// <summary>
        /// Начала числительных
        /// </summary>
        private static readonly string[] Nums_base =
            { "нулев", "перв", "втор", "трет", "четвёрт", "пят", "шест", "седьм", "восьм", "девят", "десят" };
        
        /// <summary>
        /// Окончание числительного, именительный падеж, мужской род, 0,2,6,7,8
        /// </summary>
        private const string end_Im_m_0 = "ой";
        /// <summary>
        /// Окончание числительного, именительный падеж, мужской род, 1,4,5,9,10
        /// </summary>
        private const string end_Im_m_1 = "ый";
        /// <summary>
        /// Окончание числительного, именительный падеж, мужской род, 3
        /// </summary>
        private const string end_Im_m_3 = "ий";
        /// <summary>
        /// Окончание числительного, именительный падеж, мужской род, для числа
        /// </summary>
        private const string end_Im_m_x = "й";

        /// <summary>
        /// Окончание числительного, именительный падеж, женский род, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Im_j_0 = "ая";
        /// <summary>
        /// Окончание числительного, именительный падеж, женский род, 3
        /// </summary>
        private const string end_Im_j_3 = "ья";
        /// <summary>
        /// Окончание числительного, именительный падеж, женский род, для числа
        /// </summary>
        private const string end_Im_j_x = "я";

        /// <summary>
        /// Окончание числительного, именительный падеж, средний род, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Im_s_0 = "ое";
        /// <summary>
        /// Окончание числительного, именительный падеж, средний род, 3
        /// </summary>
        private const string end_Im_s_3 = "ье";
        /// <summary>
        /// Окончание числительного, именительный падеж, средний род, для числа
        /// </summary>
        private const string end_Im_s_x = "е";

        /// <summary>
        /// Окончание числительного, именительный падеж, множественное число, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Im_M_0 = "ые";
        /// <summary>
        /// Окончание числительного, именительный падеж, множественное число, 3
        /// </summary>
        private const string end_Im_M_3 = "ьи";
        /// <summary>
        /// Окончание числительного, именительный падеж, множественное число, для числа, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Im_M_x_0 = "е";
        /// <summary>
        /// Окончание числительного, именительный падеж, множественное число, для числа, 3
        /// </summary>
        private const string end_Im_M_x_3 = "и";


        /// <summary>
        /// Окончание числительного, родительный падеж, мужской род, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Rd_m_0 = "ого";
        /// <summary>
        /// Окончание числительного, родительный падеж, мужской род, 3
        /// </summary>
        private const string end_Rd_m_3 = "его";
        /// <summary>
        /// Окончание числительного, родительный падеж, мужской род, для числа
        /// </summary>
        private const string end_Rd_m_x = "го";

        /// <summary>
        /// Окончание числительного, родительный падеж, женский род, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Rd_j_0 = "ой";
        /// <summary>
        /// Окончание числительного, родительный падеж, женский род, 3
        /// </summary>
        private const string end_Rd_j_3 = "ей";
        /// <summary>
        /// Окончание числительного, родительный падеж, женский род, для числа
        /// </summary>
        private const string end_Rd_j_x = "й";

        /// <summary>
        /// Окончание числительного, родительный падеж, средний род, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Rd_s_0 = end_Rd_m_0;
        /// <summary>
        /// Окончание числительного, родительный падеж, средний род, 3
        /// </summary>
        private const string end_Rd_s_3 = end_Rd_m_3;
        /// <summary>
        /// Окончание числительного, родительный падеж, средний род, для числа
        /// </summary>
        private const string end_Rd_s_x = end_Rd_m_x;

        /// <summary>
        /// Окончание числительного, родительный падеж, множественное число, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Rd_M_0 = "ых";
        /// <summary>
        /// Окончание числительного, родительный падеж, множественное число, 3
        /// </summary>
        private const string end_Rd_M_3 = "ьих";
        /// <summary>
        /// Окончание числительного, родительный падеж, множественное число, для числа
        /// </summary>
        private const string end_Rd_M_x = "х";


        /// <summary>
        /// Окончание числительного, дательный падеж, мужской род, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Dt_m_0 = "ому";
        /// <summary>
        /// Окончание числительного, дательный падеж, мужской род, 3
        /// </summary>
        private const string end_Dt_m_3 = "ьему";
        /// <summary>
        /// Окончание числительного, дательный падеж, мужской род, для числа
        /// </summary>
        private const string end_Dt_m_x = "му";

        /// <summary>
        /// Окончание числительного, дательный падеж, женский род, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Dt_j_0 = end_Rd_j_0;
        /// <summary>
        /// Окончание числительного, дательный падеж, женский род, 3
        /// </summary>
        private const string end_Dt_j_3 = end_Rd_j_3;
        /// <summary>
        /// Окончание числительного, дательный падеж, женский род, для числа
        /// </summary>
        private const string end_Dt_j_x = end_Rd_j_x;

        /// <summary>
        /// Окончание числительного, дательный падеж, средний род, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Dt_s_0 = end_Dt_m_0;
        /// <summary>
        /// Окончание числительного, дательный падеж, средний род, 3
        /// </summary>
        private const string end_Dt_s_3 = end_Dt_m_3;
        /// <summary>
        /// Окончание числительного, дательный падеж, средний род, для числа
        /// </summary>
        private const string end_Dt_s_x = end_Dt_m_x;

        /// <summary>
        /// Окончание числительного, дательный падеж, множественное число, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Dt_M_0 = "ым";
        /// <summary>
        /// Окончание числительного, дательный падеж, множественное число, 3
        /// </summary>
        private const string end_Dt_M_3 = "ьим";
        /// <summary>
        /// Окончание числительного, дательный падеж, множественное число, для числа
        /// </summary>
        private const string end_Dt_M_x = "м";


        /// <summary>
        /// Окончание числительного, винительный падеж, мужской род, одушевлённый, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Vn_m_od_0 = end_Rd_m_0;
        /// <summary>
        /// Окончание числительного, винительный падеж, мужской род, одушевлённый, 3
        /// </summary>
        private const string end_Vn_m_od_3 = end_Rd_m_3;
        /// <summary>
        /// Окончание числительного, винительный падеж, мужской род, одушевлённый, для числа
        /// </summary>
        private const string end_Vn_m_od_x = end_Rd_m_x;

        /// <summary>
        /// Окончание числительного, винительный падеж, мужской род, неодушевлённый, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Vn_m_neod_0 = end_Im_m_0;
        /// <summary>
        /// Окончание числительного, винительный падеж, мужской род, неодушевлённый, 3
        /// </summary>
        private const string end_Vn_m_neod_3 = end_Im_m_3;
        /// <summary>
        /// Окончание числительного, винительный падеж, мужской род, неодушевлённый, для числа
        /// </summary>
        private const string end_Vn_m_neod_x = end_Im_m_x;

        /// <summary>
        /// Окончание числительного, винительный падеж, женский род, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Vn_j_0 = "ую";
        /// <summary>
        /// Окончание числительного, винительный падеж, женский род, 3
        /// </summary>
        private const string end_Vn_j_3 = "ью";
        /// <summary>
        /// Окончание числительного, винительный падеж, женский род, для числа
        /// </summary>
        private const string end_Vn_j_x = "ю";

        /// <summary>
        /// Окончание числительного, винительный падеж, средний род, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Vn_s_0 = end_Im_s_0;
        /// <summary>
        /// Окончание числительного, винительный падеж, средний род, 3
        /// </summary>
        private const string end_Vn_s_3 = end_Im_s_3;
        /// <summary>
        /// Окончание числительного, винительный падеж, средний род, для числа
        /// </summary>
        private const string end_Vn_s_x = end_Im_s_x;

        /// <summary>
        /// Окончание числительного, родительный падеж, множественное число, одушевлённый, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Vn_M_od_0 = end_Rd_M_0;
        /// <summary>
        /// Окончание числительного, родительный падеж, множественное число, одушевлённый, 3
        /// </summary>
        private const string end_Vn_M_od_3 = end_Rd_M_3;
        /// <summary>
        /// Окончание числительного, родительный падеж, множественное число, одушевлённый, для числа
        /// </summary>
        private const string end_Vn_M_od_x = end_Rd_M_x;

        /// <summary>
        /// Окончание числительного, родительный падеж, множественное число, неодушевлённый, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Vn_M_neod_0 = end_Im_M_0;
        /// <summary>
        /// Окончание числительного, родительный падеж, множественное число, неодушевлённый, 3
        /// </summary>
        private const string end_Vn_M_neod_3 = end_Im_M_3;
        /// <summary>
        /// Окончание числительного, родительный падеж, множественное число, неодушевлённый, для числа, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Vn_M_neod_x_0 = end_Im_M_x_0;
        /// <summary>
        /// Окончание числительного, родительный падеж, множественное число, неодушевлённый, для числа, 3
        /// </summary>
        private const string end_Vn_M_neod_x_3 = end_Im_M_x_3;


        /// <summary>
        /// Окончание числительного, творительный падеж, мужской род, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Tv_m_0 = end_Dt_M_0;
        /// <summary>
        /// Окончание числительного, творительный падеж, мужской род, 3
        /// </summary>
        private const string end_Tv_m_3 = end_Dt_M_3;
        /// <summary>
        /// Окончание числительного, творительный падеж, мужской род, для числа
        /// </summary>
        private const string end_Tv_m_x = end_Dt_m_x;

        /// <summary>
        /// Окончание числительного, творительный падеж, женский род, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Tv_j_0 = end_Rd_j_0;
        /// <summary>
        /// Окончание числительного, творительный падеж, женский род, 3
        /// </summary>
        private const string end_Tv_j_3 = end_Rd_j_3;
        /// <summary>
        /// Окончание числительного, творительный падеж, женский род, для числа
        /// </summary>
        private const string end_Tv_j_x = end_Rd_j_x;

        /// <summary>
        /// Окончание числительного, творительный падеж, средний род, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Tv_s_0 = end_Tv_m_0;
        /// <summary>
        /// Окончание числительного, творительный падеж, средний род, 3
        /// </summary>
        private const string end_Tv_s_3 = end_Tv_m_3;
        /// <summary>
        /// Окончание числительного, творительный падеж, средний род, для числа
        /// </summary>
        private const string end_Tv_s_x = end_Tv_m_x;

        /// <summary>
        /// Окончание числительного, творительный падеж, множественное число, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Tv_M_0 = end_Dt_M_0;
        /// <summary>
        /// Окончание числительного, творительный падеж, множественное число, 3
        /// </summary>
        private const string end_Tv_M_3 = end_Dt_M_3;
        /// <summary>
        /// Окончание числительного, творительный падеж, множественное число, для числа
        /// </summary>
        private const string end_Tv_M_x = end_Dt_M_x;


        /// <summary>
        /// Окончание числительного, предложный падеж, мужской род, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Pr_m_0 = "ом";
        /// <summary>
        /// Окончание числительного, предложный падеж, мужской род, 3
        /// </summary>
        private const string end_Pr_m_3 = "ьем";
        /// <summary>
        /// Окончание числительного, предложный падеж, мужской род, для числа
        /// </summary>
        private const string end_Pr_m_x = "м";

        /// <summary>
        /// Окончание числительного, предложный падеж, женский род, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Pr_j_0 = end_Rd_j_0;
        /// <summary>
        /// Окончание числительного, предложный падеж, женский род, 3
        /// </summary>
        private const string end_Pr_j_3 = end_Rd_j_3;
        /// <summary>
        /// Окончание числительного, предложный падеж, женский род, для числа
        /// </summary>
        private const string end_Pr_j_x = end_Rd_j_x;

        /// <summary>
        /// Окончание числительного, предложный падеж, средний род, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Pr_s_0 = end_Pr_m_0;
        /// <summary>
        /// Окончание числительного, предложный падеж, средний род, 3
        /// </summary>
        private const string end_Pr_s_3 = end_Pr_m_3;
        /// <summary>
        /// Окончание числительного, предложный падеж, средний род, для числа
        /// </summary>
        private const string end_Pr_s_x = end_Pr_m_x;

        /// <summary>
        /// Окончание числительного, предложный падеж, множественное, 0,1,2,4,5,6,7,8,9
        /// </summary>
        private const string end_Pr_M_0 = end_Dt_M_0;
        /// <summary>
        /// Окончание числительного, предложный падеж, множественное, 3
        /// </summary>
        private const string end_Pr_M_3 = end_Dt_M_3;
        /// <summary>
        /// Окончание числительного, предложный падеж, множественное, для числа
        /// </summary>
        private const string end_Pr_M_x = end_Dt_M_x;

        /// <summary>
        /// Возвращает строковое представление числительного, для чисел от -10 до 10 включительно выводится словестное представление, для остальных - добавение окончания к числу
        /// </summary>
        /// <param name="_num">Порядковый номер</param>
        /// <param name="_pad">Падеж</param>
        /// <param name="_rdm">Род или число</param>
        /// <param name="_od">Одушевлённость</param>
        /// <param name="_zagl">Наличие заглавной буквы</param>
        /// <returns>Строковое представление числительного</returns>
        public static string getNum(int _num, Pad _pad, RdM _rdm, bool _od, bool _zagl)
        {
            bool negative = _num < 0;
            int unum = negative ? (-_num) : _num;

            StringBuilder sb = new StringBuilder();
            if (unum <= 10)
            {
                if (negative)
                    sb.Append("минус ");
                sb.Append(Nums_base[unum]);
                switch (_pad)
                {
                    case Pad.Im:
                        {
                            switch (_rdm)
                            {
                                case RdM.m:
                                    if (unum == 3)
                                        sb.Append(end_Im_m_3);
                                    else
                                    {
                                        if ((unum == 0) || (unum == 2) || ((unum >= 6) && (unum <= 8)))
                                            sb.Append(end_Im_m_0);
                                        else
                                            sb.Append(end_Im_m_1);
                                    }
                                    break;
                                case RdM.j:
                                    if (unum == 3)
                                        sb.Append(end_Im_j_3);
                                    else
                                        sb.Append(end_Im_j_0);
                                    break;
                                case RdM.s:
                                    if (unum == 3)
                                        sb.Append(end_Im_s_3);
                                    else
                                        sb.Append(end_Im_s_0);
                                    break;
                                case RdM.M:
                                    if (unum == 3)
                                        sb.Append(end_Im_M_3);
                                    else
                                        sb.Append(end_Im_M_0);
                                    break;
                            }
                        }
                        break;

                    case Pad.Rd:
                        {
                            switch (_rdm)
                            {
                                case RdM.m:
                                    if (unum == 3)
                                        sb.Append(end_Rd_m_3);
                                    else
                                        sb.Append(end_Rd_m_0);
                                    break;
                                case RdM.j:
                                    if (unum == 3)
                                        sb.Append(end_Rd_j_3);
                                    else
                                        sb.Append(end_Rd_j_0);
                                    break;
                                case RdM.s:
                                    if (unum == 3)
                                        sb.Append(end_Rd_s_3);
                                    else
                                        sb.Append(end_Rd_s_0);
                                    break;
                                case RdM.M:
                                    if (unum == 3)
                                        sb.Append(end_Rd_M_3);
                                    else
                                        sb.Append(end_Rd_M_0);
                                    break;
                            }
                        }
                        break;

                    case Pad.Dt:
                        {
                            switch (_rdm)
                            {
                                case RdM.m:
                                    if (unum == 3)
                                        sb.Append(end_Dt_m_3);
                                    else
                                        sb.Append(end_Dt_m_0);
                                    break;
                                case RdM.j:
                                    if (unum == 3)
                                        sb.Append(end_Dt_j_3);
                                    else
                                        sb.Append(end_Dt_j_0);
                                    break;
                                case RdM.s:
                                    if (unum == 3)
                                        sb.Append(end_Dt_s_3);
                                    else
                                        sb.Append(end_Dt_s_0);
                                    break;
                                case RdM.M:
                                    if (unum == 3)
                                        sb.Append(end_Dt_M_3);
                                    else
                                        sb.Append(end_Dt_M_0);
                                    break;
                            }
                        }
                        break;

                    case Pad.Vn:
                        {
                            switch (_rdm)
                            {
                                case RdM.m:
                                    if (_od)
                                    {
                                        if (unum == 3)
                                            sb.Append(end_Vn_m_od_3);
                                        else
                                            sb.Append(end_Vn_m_od_0);
                                    }
                                    else
                                    {
                                        if (unum == 3)
                                            sb.Append(end_Vn_m_neod_3);
                                        else
                                            sb.Append(end_Vn_m_neod_0);
                                    }
                                    break;
                                case RdM.j:
                                    if (unum == 3)
                                        sb.Append(end_Vn_j_3);
                                    else
                                        sb.Append(end_Vn_j_0);
                                    break;
                                case RdM.s:
                                    if (unum == 3)
                                        sb.Append(end_Vn_s_3);
                                    else
                                        sb.Append(end_Vn_s_0);
                                    break;
                                case RdM.M:
                                    if (_od)
                                    {
                                        if (unum == 3)
                                            sb.Append(end_Vn_M_od_3);
                                        else
                                            sb.Append(end_Vn_M_od_0);
                                    }
                                    else
                                    {
                                        if (unum == 3)
                                            sb.Append(end_Vn_M_neod_3);
                                        else
                                            sb.Append(end_Vn_M_neod_0);
                                    }
                                    break;
                            }
                        }
                        break;

                    case Pad.Tv:
                        {
                            switch (_rdm)
                            {
                                case RdM.m:
                                    if (unum == 3)
                                        sb.Append(end_Tv_m_3);
                                    else
                                        sb.Append(end_Tv_m_0);
                                    break;
                                case RdM.j:
                                    if (unum == 3)
                                        sb.Append(end_Tv_j_3);
                                    else
                                        sb.Append(end_Tv_j_0);
                                    break;
                                case RdM.s:
                                    if (unum == 3)
                                        sb.Append(end_Tv_s_3);
                                    else
                                        sb.Append(end_Tv_s_0);
                                    break;
                                case RdM.M:
                                    if (unum == 3)
                                        sb.Append(end_Tv_M_3);
                                    else
                                        sb.Append(end_Tv_M_0);
                                    break;
                            }
                        }
                        break;

                    case Pad.Pr:
                        {
                            switch (_rdm)
                            {
                                case RdM.m:
                                    if (unum == 3)
                                        sb.Append(end_Pr_m_3);
                                    else
                                        sb.Append(end_Pr_m_0);
                                    break;
                                case RdM.j:
                                    if (unum == 3)
                                        sb.Append(end_Pr_j_3);
                                    else
                                        sb.Append(end_Pr_j_0);
                                    break;
                                case RdM.s:
                                    if (unum == 3)
                                        sb.Append(end_Pr_s_3);
                                    else
                                        sb.Append(end_Pr_s_0);
                                    break;
                                case RdM.M:
                                    if (unum == 3)
                                        sb.Append(end_Pr_M_3);
                                    else
                                        sb.Append(end_Pr_M_0);
                                    break;
                            }
                        }
                        break;
                }
                if (_zagl)
                    sb[0] = sb[0].ToString().ToUpper()[0];
            }
            else
            {
                sb.Append(_num);
                switch (_pad)
                {
                    case Pad.Im:
                        {
                            switch (_rdm)
                            {
                                case RdM.m:
                                    sb.Append(end_Im_m_x);
                                    break;
                                case RdM.j:
                                    sb.Append(end_Im_j_x);
                                    break;
                                case RdM.s:
                                    sb.Append(end_Im_s_x);
                                    break;
                                case RdM.M:
                                    if (unum == 3)
                                        sb.Append(end_Im_M_x_3);
                                    else
                                        sb.Append(end_Im_M_x_0);
                                    break;
                            }
                        }
                        break;

                    case Pad.Rd:
                        {
                            switch (_rdm)
                            {
                                case RdM.m:
                                    sb.Append(end_Rd_m_x);
                                    break;
                                case RdM.j:
                                    sb.Append(end_Rd_j_x);
                                    break;
                                case RdM.s:
                                    sb.Append(end_Rd_s_x);
                                    break;
                                case RdM.M:
                                    sb.Append(end_Rd_M_x);
                                    break;
                            }
                        }
                        break;

                    case Pad.Dt:
                        {
                            switch (_rdm)
                            {
                                case RdM.m:
                                    sb.Append(end_Dt_m_x);
                                    break;
                                case RdM.j:
                                    sb.Append(end_Dt_j_x);
                                    break;
                                case RdM.s:
                                    sb.Append(end_Dt_s_x);
                                    break;
                                case RdM.M:
                                    sb.Append(end_Dt_M_x);
                                    break;
                            }
                        }
                        break;

                    case Pad.Vn:
                        {
                            switch (_rdm)
                            {
                                case RdM.m:
                                    if (_od)
                                        sb.Append(end_Vn_m_od_x);
                                    else
                                        sb.Append(end_Vn_m_neod_x);
                                    break;
                                case RdM.j:
                                    sb.Append(end_Vn_j_x);
                                    break;
                                case RdM.s:
                                    sb.Append(end_Vn_s_x);
                                    break;
                                case RdM.M:
                                    if (_od)
                                        sb.Append(end_Vn_M_od_x);
                                    else
                                    {
                                        if (unum == 3)
                                            sb.Append(end_Vn_M_neod_x_3);
                                        else
                                            sb.Append(end_Vn_M_neod_x_0);
                                    }
                                    break;
                            }
                        }
                        break;

                    case Pad.Tv:
                        {
                            switch (_rdm)
                            {
                                case RdM.m:
                                    sb.Append(end_Tv_m_x);
                                    break;
                                case RdM.j:
                                    sb.Append(end_Tv_j_x);
                                    break;
                                case RdM.s:
                                    sb.Append(end_Tv_s_x);
                                    break;
                                case RdM.M:
                                    sb.Append(end_Tv_M_x);
                                    break;
                            }
                        }
                        break;

                    case Pad.Pr:
                        {
                            switch (_rdm)
                            {
                                case RdM.m:
                                    sb.Append(end_Pr_m_x);
                                    break;
                                case RdM.j:
                                    sb.Append(end_Pr_j_x);
                                    break;
                                case RdM.s:
                                    sb.Append(end_Pr_s_x);
                                    break;
                                case RdM.M:
                                    sb.Append(end_Pr_M_x);
                                    break;
                            }
                        }
                        break;
                }
            }
            return sb.ToString();
        }

        // Часть 0 - Выбор критерия
        /// <summary>
        /// Названия критериев
        /// </summary>
        private static readonly string[][] Tmpl0_Ans0 =
        {
            new string[]{"Лапласа"},
            new string[]{"максиМакса",
                "миниМина" },
            new string[]{"максиМина",
                "миниМакса" },
            new string[]{"Сэвиджа"},
            new string[]{"произведений"},
            new string[]{"Гурвица"},
            new string[]{"Байеса"},
            new string[]{"Гермейера"},
            new string[]{"Ходжа-Лемана"},
            new string[]{"BL (MM)"}
        };
        /// <summary>
        /// Описания обоснований 
        /// </summary>
        private static readonly string[][] Tmpl0_Ans1 =
        {
            new string[]{"с предположением, что априорные вероятности состояний \"природы\" равны"},
            new string[]{"со стремлением максимизировать минимальный выигрыш",
                "со стремлением минимизировать максимальный выигрыш"},
            new string[]{"с ожиданием максимального выигрыша",
                "ожиданием минимальных потерь"},
            new string[]{"с необходимостью минимизировать возможный недополученный выигрыш",
                "необходимостью максимизировать разницу между наименьшим значением потерь и возможным"},
            new string[]{"с максимизацией произведений выигрышей",
                "с минимизацией произведений потерь"},
            new string[]{"с указанной степенью уверенности в преобладании выигрышной ситуации над проигрышной"},
            new string[]{"с используемым распределением вероятностей"},
            new string[]{"с максимизацией произведения выигрыша на вероятность",
                "с минимизацией произведения проигрыша на вероятность"},
            new string[]{"со степенью доверия используемому распределению вероятностей"},
            new string[]{"с BLMM-критерием"}
        };
        
        /// <summary>
        /// Вывод вещестенного числа с предварительным форматированием
        /// </summary>
        /// <param name="_val">Вещественное число</param>
        /// <returns>Строковое представление форматированного вещественного числа</returns>
        private static string dsf(double _val)
        {
            return String.Format("{0:0.00}", _val);
        }

        /// <summary>
        /// Текст для выбора критерия: выбор критерия принятия решений и его обоснования
        /// </summary>
        /// <param name="_dec">Выбранный критерий</param>
        /// <param name="_rsn">Обоснование критерия</param>
        /// <param name="_loss">Если потери</param>
        /// <param name="_val">Значение параметра критерия</param>
        /// <returns>Текст с обозначением выбора критерия</returns>
        public static string generateTemplateTextCriterion(int _dec, int _rsn, bool _loss, double _val)
        {
            if ((_dec < 0) || (_dec >= Tmpl0_Ans0.Length))
                return null;
            StringBuilder sb = new StringBuilder();
            sb.Append("Для решения задачи был выбран критерий ");
            string[] St = Tmpl0_Ans0[_dec];
            sb.Append((St.Length == 1) ? St[0] : (_loss ? St[1] : St[0]));
            sb.Append(" в соответствии ");
            St = Tmpl0_Ans1[_rsn];
            sb.Append((St.Length == 1) ? St[0] : (_loss ? St[1] : St[0]));
            sb.Append(".");
            if (Methods.NeedParam(_dec))
            {
                sb.Append(" Значение коэффициента ");
                switch (_dec)
                {
                    case 5: sb.Append("\"уверенности\" - alpha"); break;
                    case 8: sb.Append("\"степени доверия распределению вероятностей\" - nu"); break;
                    case 9: sb.Append("\"уровня риска\" - epsilon"); break;
                }
                sb.Append(" принято равным ");
                sb.Append(dsf(_val));
                sb.Append(".");
            }
            return sb.ToString();
        }
        //=======================================
        //Часть 1 - Игра без эксперимента

        /// <summary>
        /// Формирование текста с описанием выбранной стратегии для игры без эксперимента
        /// </summary>
        /// <param name="_dec">Выбранная стратегия</param>
        /// <param name="_loss">Если потери</param>
        /// <param name="_wlval">Новое значение выигрыша/потерь</param>
        /// <param name="_apr">Если необходим учёт априорных вероятностей</param>
        /// <returns>Текст с описанием выбора стратегии</returns>
        public static string generateTemplateTextNoExperiment(int _dec, bool _loss, double _wval, bool _apr)
        {
            if (_dec < 0)
                return null;
            StringBuilder sb = new StringBuilder();
            sb.Append("Если в данной игре не предполагается проводить эксперимент, то ");
            if (_apr)
                sb.Append("с учётом известных априорных вероятностей ");
            sb.Append("рекомендуется выбрать ");
            sb.Append(getNum(_dec + 1, Pad.Vn, RdM.j, false, false));
            sb.Append(" стратегию. При этом ");
            sb.Append(_loss ? "ожидаемые потери будут равны " : "ожидаемый выигрыш будет равен ");
            sb.Append(dsf(_loss ? -_wval : _wval));
            sb.Append(".");
            return sb.ToString();
        }
        /// <summary>
        /// Формирование текста с описанием решения о проведении эксперимента
        /// </summary>
        /// <param name="_expNum">Номер эксперимента (с 0-го)</param>
        /// <param name="_dec">0 - проводится, 1 - не требуется из-за вхождения в область останова, 2 - дорог</param>
        /// <returns>Текст описания решения о проведении эксперимента</returns>
        public static string generateTemplateTextExperimentDecision(int _expNum, int _dec)
        {
            if ((_expNum < 0) || (_dec < 0) || (_dec >= 3))
                return null;
            StringBuilder sb = new StringBuilder();
            sb.Append(getNum(_expNum + 1, Pad.Im, RdM.m, false, true));
            sb.Append(" эксперимент ");
            string str1 = " проводить";
            if (_dec == 0)
            {
                sb.Append("следует");
                sb.Append(str1);
            }
            else
            {
                sb.Append("не рекомендуется");
                sb.Append(str1);
                sb.Append(" по причине ");
                if (_dec == 1)
                    sb.Append("вхождения априорных вероятностей в область останова");
                else
                    sb.Append("слишком высокой стоимости его проведения");
            }
            sb.Append(".");
            return sb.ToString();
        }
        

        /// <summary>
        /// Формирование текста о среднем выигрыше/потерях при проведении эксперимента
        /// </summary>
        /// <param name="_expNum">номер эксперимента (с 0-го)</param>
        /// <param name="_loss">Если потери</param>
        /// <param name="_wval">Значение среднего выигрыша с учётом стоимости эксперимента</param>
        /// <returns>Текст о среднем выигрыше/потерях при проведении эксперимента</returns>
        public static string generateTemplateTextExperiment(int _expNum, bool _loss, double _wval)
        {
            if (_expNum < 0)
                return null;

            StringBuilder sb = new StringBuilder();
            sb.Append("Если в данной задаче будет принято решение о проведении ");
            sb.Append(getNum(_expNum + 1, Pad.Rd, RdM.m, false, false));
            sb.Append(" эксперимента, то ");
            sb.Append(_loss ? "средние ожидаемые потери уменьшаются" : "средний ожидаемый выигрыш увеличивается");
            sb.Append(" (по сравнению с ");
            if (_expNum == 0)
                sb.Append("игрой без эксперимента при исходных априорных вероятностях");
            else
                sb.Append("предыдущим экспериментом при соответствующих апостериорных вероятностях");
            sb.Append(") до ");
            sb.Append(dsf(_loss ? -_wval : _wval));
            sb.Append(" (с учётом стоимости эксперимента).");

            return sb.ToString();
        }
        /// <summary>
        /// Формирование текста выбора стратегии при определённом исходе
        /// </summary>
        /// <param name="_expNum">Номер эксперимента</param>
        /// <param name="_res">Исход эксперимента</param>
        /// <param name="_dec">Выбор стратегии</param>
        /// <param name="_loss">Если потери</param>
        /// <param name="_wval">Значение выигрыша с учётом стоимости эксперимента</param>
        /// <returns>Текст выбора стратегии при определённом исходе</returns>
        public static string generateTemplateTextExperimentStrategy(int _expNum, int _res, int _dec, bool _loss, double _wval)
        {
            if ((_expNum < 0) || (_res < 0) || (_dec < 0))
                return null;
            StringBuilder sb = new StringBuilder();
            sb.Append("При ");
            sb.Append(getNum(_res + 1, Pad.Pr, RdM.m, false, false));
            sb.Append(" исходе ");
            sb.Append(getNum(_expNum + 1, Pad.Rd, RdM.m, false, false));
            sb.Append(" эксперимента следует выбрать ");
            sb.Append(getNum(_dec + 1, Pad.Vn, RdM.j, false, false));
            sb.Append(" стратегию. ");
            sb.Append(_loss ? "Ожидаемые потери уменьшаются" : "Ожидаемый выигрыш увеличивается");
            sb.Append(" с учётом стоимости эксперимента до ");
            sb.Append(dsf(_loss ? -_wval : _wval));
            sb.Append(".");
            return sb.ToString();
        }
    }

    /// <summary>
    /// Пример статического класса для формирования ответов в терминах задачи
    /// </summary>
    public static class StringsTemplateGeneratorTaskExample
    {
        //Часть 1 - Игра без эксперимента

        /// <summary>
        /// Формирование текста с описанием выбранной стратегии для игры без эксперимента
        /// </summary>
        /// <param name="_dec">Выбранная стратегия</param>
        /// <param name="_apr">Если необходим учёт априорных вероятностей</param>
        /// <returns>Текст с описанием выбора стратегии</returns>
        public static string generateTemplateTextNoExperiment(int _dec, bool _apr)
        {
            if ((_dec < 0) || (_dec >= 2))
                return null;
            StringBuilder sb = new StringBuilder();
            sb.Append("Если не предполагается проводить эксперимент, то ");
            if (_apr)
                sb.Append("с учётом известных априорных вероятностей ");
            sb.Append("рекомендуется ");
            if (_dec == 1)
                sb.Append("не ");
            sb.Append("брать зонт.");
            return sb.ToString();
        }
        /// <summary>
        /// Фомрирование текста описания решения о проведении эксперимента
        /// </summary>
        /// <param name="_expNum">Номер эксперимента (с 0-го)</param>
        /// <param name="_dec">0 - проводится, 1 - не требуется из-за вхождения в область останова, 2 - дорог</param>
        /// <returns>Текст описания решения о проведении эксперимента</returns>
        public static string generateTemplateTextExperimentDecision(int _expNum, int _dec)
        {
            if ((_expNum < 0) || (_dec < 0) || (_dec >= 3))
                return null;
            StringBuilder sb = new StringBuilder();
            switch (_expNum)
            {
                case 0: sb.Append("Вспоминать погоду в прошлый день "); break;
                case 1: sb.Append("Ознакомиться с прогнозом погоды "); break;
                default: return null;
            }
            if (_dec == 0)
            {
                sb.Append("предпочтительнее");
            }
            else
            {
                sb.Append("не рекомендуется");
                sb.Append(" по причине ");
                if (_dec == 1)
                    sb.Append("достаточной надёжности уже имеющегося решения");
                else
                    sb.Append("слишком высоких затрат по времени");
            }
            sb.Append(".");
            return sb.ToString();
        }
        /// <summary>
        /// Формирование текста выбора стратегии при определённом исходе
        /// </summary>
        /// <param name="_expNum">Номер эксперимента</param>
        /// <param name="_res">Исход эксперимента</param>
        /// <param name="_dec">Выбранная стратегия</param>
        /// <param name="_loss">Если потери</param>
        /// <param name="_wval">Значение выигрыша с учётом стоимости эксперимента</param>
        /// <returns>Текст выбора стратегии при определённом исходе</returns>
        public static string generateTemplateTextExperimentStrategy(int _expNum, int _res, int _dec)
        {
            if ((_expNum < 0) || (_res < 0) || (_dec < 0))
                return null;
            StringBuilder sb = new StringBuilder();
            sb.Append("Если ");
            switch (_expNum)
            {
                case 0: sb.Append("погода в прошлый день"); break;
                case 1: sb.Append("прогноз погоды"); break;
                default: return null;
            }
            switch (_res)
            {
                case 0: sb.Append("предполагает скорее ясную погоду"); break;
                case 1: sb.Append("не может дать точный ответ"); break;
                case 2: sb.Append("предполагает скорее дождь"); break;
            }
            sb.Append(", то рекомендуется ");
            if (_dec == 1)
                sb.Append("не ");
            sb.Append("брать зонт.");
            sb.Append(".");
            return sb.ToString();
        }
    }
    /// <summary>
    /// Класс информации о дереве решений
    /// </summary>
    public class DecisionTree
    {
        /// <summary>
        /// Узел принятия решений для эксперимента
        /// </summary>
        public class ExperimentNode
        {
            /// <summary>
            /// Номера решений от результата эксперимента (z)
            /// </summary>
            public int[] Decisions;//null - текущий эксперимент не проводится по причине высокой стоимости
            /// <summary>
            /// Эксперименты (z)
            /// </summary>
            public ExperimentNode[] Experiments;//[i]==null - следующий эксперимент не проводится по причине вхождения в область останова
            /// <summary>
            /// Конструктор
            /// </summary>
            public ExperimentNode()
            {
                this.Decisions = null;
                this.Experiments = null;
            }
            /// <summary>
            /// Конструктор с клонированием массива решений для исходов эксперимента
            /// </summary>
            /// <param name="_Decisions">Решения от исходов эксперимента</param>
            public ExperimentNode(int[] _Decisions)
            {
                if ((_Decisions == null) || (_Decisions.Length == 0))
                {
                    this.Decisions = null;
                    this.Experiments = null;
                }
                else
                {
                    this.Decisions = (int[])_Decisions.Clone();
                    this.Experiments = new ExperimentNode[this.Decisions.Length];
                    for (int i = 0; i < this.Decisions.Length; i++)
                        this.Experiments[i] = null;
                }
            }
            /// <summary>
            /// Установить новое количество исходов эксперимента
            /// </summary>
            /// <param name="_size">Новое количество исходов эксперимента</param>
            public void resetDCount(int _size)
            {
                if (_size < 0)
                {
                    this.Decisions = null;
                    this.Experiments = null;
                }
                else
                {
                    this.Decisions = new int[_size];
                    this.Experiments = new ExperimentNode[_size];
                    for (int i = 0; i < _size; i++)
                    {
                        this.Decisions[i] = 0;
                        this.Experiments[i] = null;
                    }
                }
            }
            /// <summary>
            /// Отобразить содержимое в консоли
            /// </summary>
            /// <param name="_s">Заголовок</param>
            public void showIt(string _s)
            {
                if (_s != null)
                    Console.Out.WriteLine(_s + ": ");
                for (int i = 0; i < this.Decisions.Length; i++)
                {
                    Console.Out.WriteLine("+ если " + i + ", то " + this.Decisions[i]);
                    if (this.Experiments[i] != null)
                        this.Experiments[i].showIt("или эксперимент");
                    Console.Out.WriteLine("____конец эксперимента;");
                }
            }
        }
        /// <summary>
        /// Номер используемого критерия принятия решений
        /// </summary>
        public int criterion;
        /// <summary>
        /// Номер обоснования критерия
        /// </summary>
        public int criterion_reason;
        /// <summary>
        /// Номер решения без эксперимента
        /// </summary>
        public int decision;
        /// <summary>
        /// Начальный эксперимент
        /// </summary>
        public ExperimentNode FirstExperiment;//null - следующий эксперимент не проводится по причине вхождения в область останова
        /// <summary>
        /// Констркутор по умолчанию
        /// </summary>
        public DecisionTree()
        {
            this.criterion = 0;
            this.criterion_reason = 0;
            this.decision = 0;
            this.FirstExperiment = null;
        }
        /// <summary>
        /// Конструктор с указанием количества решений
        /// </summary>
        /// <param name="_criterion">Номер критерия/param>
        /// <param name="_criterion_reason">Номер обоснования критерия</param>
        /// <param name="_decision">Номер решения для игры без эксперимента</param>
        public DecisionTree(int _criterion, int _criterion_reason, int _decision)
        {
            this.criterion = _criterion;
            this.criterion_reason = _criterion_reason;
            this.decision = _decision;
            this.FirstExperiment = null;
        }
    }
    /// <summary>
    /// Результат для игры без эксперимента
    /// </summary>
    public class NoExpResultData
    {
        /// <summary>
        /// Признаки вхождения в область останова
        /// </summary>
        public bool[] ti;//(gs)
        /// <summary>
        /// Ожидаемые выигрыши для каждого решения
        /// </summary>
        public double[] ML;//(gd)
        /// <summary>
        /// Множество значений эффективности
        /// </summary>
        public double[] K;//(gd)
        /// <summary>
        /// Множество вычисляемых значений для определения допустимых стратегий
        /// </summary>
        public double[,] VM;//(gd,x)
        /// <summary>
        /// Множество обозначений выполняемых условий для определения допустимых стратегий
        /// </summary>
        public bool[,] uxM;//(gd,x)
        /// <summary>
        /// Множество указаний на допустимые стратегии
        /// </summary>
        public bool[] uM;//(gd)
        /// <summary>
        /// Результат по критерию
        /// </summary>
        public int crResult;
        /// <summary>
        /// Конструктор с указанием множества решений
        /// </summary>
        /// <param name="_ti">Достаточные вероятности</param>
        /// <param name="_ML">Ожидаемые выигрыши</param>
        /// <param name="_K">Множество распределений значений эффективности</param>
        /// <param name="_VM">Множество вычисляемых значений для определения допустимых стратегий</param>
        /// <param name="_uxM">Множество обозначений выполняемых условий для определения допустимых стратегий</param>
        /// <param name="_uM">Множество указаний на допустимые стратегии</param>
        /// <param name="_crResult">Результ по критерию</param>
        public NoExpResultData(
            bool[] _ti,
            double[] _ML,
            double[] _K,
            double[,] _VM,
            bool[,] _uxM,
            bool[] _uM,
            int _crResult
            )
        {
            ti = _ti;
            ML = _ML;
            K = _K;
            VM = _VM;
            uxM = _uxM;
            uM = _uM;
            crResult = _crResult;
        }
        /// <summary>
        /// Вывод в консоль
        /// </summary>
        /// <param name="_s">Заголовок</param>
        public void showIt(string _s)
        {//Игра без эксперимента
            if (_s != null)
                Console.Out.WriteLine(_s + ": ");
            Methods.showIt("Достаточные вероятности", ti);
            Methods.showIt("Ожидаемые потери", ML);
            Methods.showIt("Множество распределений значений эффективности", K);
            Methods.showIt("Множество вычисляемых значений для определения допустимых стратегий", VM);
            Methods.showIt("Множество обозначений выполняемых условий для определения допустимых стратегий", uxM);
            Methods.showIt("Множество указаний на опустимые стратегии", uM);
            Methods.showIt("Решение по критерию", crResult);
        }
    }
    /// <summary>
    /// Результат для игры без эксперимента
    /// </summary>
    public class ExpResultData
    {
        /// <summary>
        /// Полные вероятности исходов эксперимента
        /// </summary>
        public double[] Pz;//(gz)
        /// <summary>
        /// Апостериорные вероятности
        /// </summary>
        public double[,] Psz;//(gz, gs)
        /// <summary>
        /// Обозначение вхождения в область останова
        /// </summary>
        public bool[,] tis;//(gz, gs)
        /// <summary>
        /// Множество ожидаемых выигрышей для каждой стратегии каждого исхода эксперимента
        /// </summary>
        public double[,] MLs;//(gz, gd)
        /// <summary>
        /// Множество распределений значений эффективности каждого исхода эксперимента
        /// </summary>
        public double[,] Ks;//(gz, gd)
        /// <summary>
        /// Множество вычисляемых значений для определения допустимых стратегий каждого исхода эксперимента
        /// </summary>
        public double[,,] VMs;//(gz,gd,x)
        /// <summary>
        /// Множество обозначений выполняемых условий для определения допустимых стратегий каждого исхода эксперимента
        /// </summary>
        public bool[,,] uxMs;//(gz,gd,x)
        /// <summary>
        /// Множество указаний на допустимые стратегии каждого исхода эксперимента
        /// </summary>
        public bool[,] uMs;//(gz,gd)
        /// <summary>
        /// Решающая функция
        /// </summary>
        public int[] FR;//(gz)
        /// <summary>
        /// Ожидаемые выигрыши для каждого исхода эксперимента
        /// </summary>
        public double[] op;//(gz)
        /// <summary>
        /// Общий ожидаемый выигрыш
        /// </summary>
        public double Oop;
        /// <summary>
        /// Максимальная стоимость целесообразного эксперимента
        /// </summary>
        public double mexp;
        /// <summary>
        /// Выигрыш от проведения эксперимента
        /// </summary>
        public double expv;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="_Pz">Полные вероятности исходов эксперимента</param>
        /// <param name="_Psz">Апостериорные вероятности</param>
        /// <param name="_tis">Обозначение вхождения в область останова</param>
        /// <param name="_ML">Множество ожидаемых выигрышей для каждой стратегии каждого исхода эксперимента</param>
        /// <param name="_Ks">Множество распределений значений эффективности (gz, gd)</param>
        /// <param name="_VMs">Множество вычисляемых значений для определения допустимых стратегий (gz,gd,2)</param>
        /// <param name="_uxMs">Множество обозначений выполняемых условий для определения допустимых стратегий каждого исхода эксперимента (gz,gd,2)</param>
        /// <param name="_uMs">Множество указаний на допустимые стратегии каждого исхода эксперимента (gz,gd)</param>
        /// <param name="_FR">Решающая функция</param>
        /// <param name="_op">Ожидаемые выигрыши для каждого исхода эксперимента</param>
        /// <param name="_Oop">Общий ожидаемый выигрыш</param>
        /// <param name="_mexp">Максимальная стоимость целесообразного эксперимента</param>
        /// <param name="_expv">Выигрыш от проведения эксперимента</param>
        public ExpResultData(
        double[] _Pz,
        double[,] _Psz,
        bool[,] _tis,
        double[,] _ML,
        double[,] _Ks,
        double[,,] _VMs,
        bool[,,] _uxMs,
        bool[,] _uMs,
        int[] _FR,
        double[] _op,
        double _Oop,
        double _mexp,
        double _expv
        )
        {
            Pz = _Pz;
            Psz = _Psz;
            tis = _tis;
            MLs = _ML;
            Ks = _Ks;
            VMs = _VMs;
            uxMs = _uxMs;
            uMs = _uMs;
            FR = _FR;
            op = _op;
            Oop = _Oop;
            mexp = _mexp;
            expv = _expv;
        }
        /// <summary>
        /// Вывод в консоль
        /// </summary>
        /// <param name="_s">Заголовок</param>
        public void showIt(string _s)
        {
            if (_s != null)
                Console.Out.WriteLine(_s + ": ");
            Methods.showIt("Полные вероятности исходов эксперимента", Pz);
            Methods.showIt("Апостериорные вероятности", Psz);
            Methods.showWhereStop("При различных результатах эксперимента", tis);
            Methods.showIt("Ожидаемые потери", MLs);
            Methods.showIt("Решающая функция", FR);
            Methods.showIt("(Минимальные или иные) ожидаемые потери", op);
            Methods.showIt("Общие ожидаемые потери", Oop);
            Methods.showIt("Максимальная стоимость целесообразного эксперимента", mexp);
            Methods.showIt("Выигрыш", expv);
        }
    }
    /// <summary>
    /// Класс для хранения статических методов
    /// </summary>
    public static class Methods
    {
        /// <summary>
        /// Проверка массива на отсутствие данных
        /// </summary>
        /// <param name="_M">Массив</param>
        /// <returns>Данные отсутствуют</returns>
        public static bool noData(Array _M)
        {
            if (_M == null)
                return true;
            for (int i = 0; i < _M.Rank; i++)
                if (_M.GetLength(i) == 0)
                    return true;
            return false;
        }
        /// <summary>
        /// Вычисление значений эффективности согласно критерию Лапласа
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <returns>Значения эффективности согласно критерию Лапласа</returns>
        public static double[] K_Laplace(double[,] _L)
        {
            if (noData(_L))
                return null;
            int gd = _L.GetLength(0), gs = _L.GetLength(1);

            double[] R = new double[gd];
            for (int i = 0; i < R.Length; i++)
            {
                R[i] = 0.0;
                for (int j = 0; j < gs; j++)
                    R[i] += _L[i, j];
                R[i] /= gs;
            }

            return R;
        }
        /// <summary>
        /// Вычисление значений эффективности согласно критерию максимакса
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <returns>Значения эффективности согласно критерию максимакса</returns>
        public static double[] K_MaxiMax(double[,] _L)
        {
            if (noData(_L))
                return null;
            int gd = _L.GetLength(0), gs = _L.GetLength(1);

            int[] Kpos = new int[gd];
            for (int i = 0; i < Kpos.Length; i++)
            {
                Kpos[i] = 0;
                for (int j = 1; j < gs; j++)
                    if (_L[i, j] > _L[i, Kpos[i]])
                        Kpos[i] = j;
            }
            double[] R = new double[gd];
            for (int vd = 0; vd < gd; vd++)
                R[vd] = _L[vd, Kpos[vd]];
            return R;
        }
        /// <summary>
        /// Вычисление значений эффективности согласно критерию максимина
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <returns>Значения эффективности согласно критерию максимина</returns>
        public static double[] K_MaxiMin(double[,] _L)
        {
            if (noData(_L))
                return null;
            int gd = _L.GetLength(0), gs = _L.GetLength(1);

            int[] Kpos = new int[gd];
            for (int i = 0; i < Kpos.Length; i++)
            {
                Kpos[i] = 0;
                for (int j = 1; j < gs; j++)
                    if (_L[i, j] < _L[i, Kpos[i]])
                        Kpos[i] = j;
            }
            double[] R = new double[gd];
            for (int vd = 0; vd < gd; vd++)
                R[vd] = _L[vd, Kpos[vd]];
            return R;
        }
        /// <summary>
        /// Вычисление значений эффективности согласно критерию Сэвиджа
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <returns>Значения эффективности согласно критерию Сэвиджа</returns>
        public static double[] K_Savage(double[,] _L)
        {
            if (noData(_L))
                return null;
            int gd = _L.GetLength(0), gs = _L.GetLength(1);

            int[] Mb = new int[gs];
            for (int i = 0; i < gs; i++)
            {
                Mb[i] = 0;
                for (int j = 1; j < gd; j++)
                    if (_L[j, i] > _L[Mb[i], i])
                        Mb[i] = j;
            }
            double[,] r = (double[,])_L.Clone();
            for (int i = 0; i < gs; i++)
                for (int j = 0; j < gd; j++)
                    r[j, i] -= Mb[i];//Значения должны быть не положительными
            return K_MaxiMin(r);
        }
        /// <summary>
        /// Вычисление значений эффективности согласно критерию произведений
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <returns>Значения эффективности согласно критерию произведений</returns>
        public static double[] K_Proizv(double[,] _L)
        {
            if (noData(_L))
                return null;
            int gd = _L.GetLength(0), gs = _L.GetLength(1);

            double[] R = new double[gd];
            double minV = _L[0, 0];
            for (int i = 0; i < gd; i++)
                for (int j = 0; j < gs; j++)
                    if (_L[i, j] < minV)
                        minV = _L[i, j];
            if (minV > 0.0)
                minV = 0.0;
            for (int i = 0; i < gd; i++)
            {
                R[i] = (_L[i, 0] - minV);
                for (int j = 1; j < gs; j++)
                    R[i] *= (_L[i, j] - minV);
            }
            return R;
        }
        /// <summary>
        /// Вычисление значений эффективности согласно критерию Гурвица
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <param name="_alpha">Коэффициент уверенности</param>
        /// <returns>Значения эффективности согласно критерию Гурвица</returns>
        public static double[] K_Hurwitz(double[,] _L, double _alpha)
        {
            if (noData(_L) || (_alpha < 0.0) || (_alpha > 1.0))
                return null;
            int gd = _L.GetLength(0), gs = _L.GetLength(1);


            int[] minM = new int[gd];
            int[] maxM = new int[gd];
            for (int i = 0; i < gd; i++)
            {
                minM[i] = 0;
                maxM[i] = 0;
                for (int j = 1; j < gs; j++)
                {
                    if (_L[i, j] < _L[i, minM[i]])
                        minM[i] = j;
                    if (_L[i, j] > _L[i, maxM[i]])
                        maxM[i] = j;
                }
            }
            double[] R = new double[gd];
            for (int i = 0; i < gd; i++)
                R[i] = _L[i, minM[i]] + _alpha * (_L[i, maxM[i]] - _L[i, minM[i]]);

            return R;
        }
        /// <summary>
        /// Вычисление значений эффективности согласно критерию Байеса-Лапласа
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <param name="_P">Распределение вероятностей</param>
        /// <returns>Значения эффективности согласно критерию Байеса-Лапласа</returns>
        public static double[] K_ExpectedValue(double[,] _L, double[] _P)
        {
            if (noData(_L) || noData(_P) || (_L.GetLength(1) != _P.Length))
                return null;
            int gd = _L.GetLength(0), gs = _L.GetLength(1);

            double[] R = new double[gd];
            for (int i = 0; i < gd; i++)
            {
                R[i] = 0.0;
                for (int j = 0; j < gs; j++)
                    R[i] += _L[i, j] * _P[j];
                R[i] /= gs;
            }
            return R;
        }
        /// <summary>
        /// Вычисление значений эффективности согласно критерию Гермейера
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <param name="_P">Распределение вероятностей</param>
        /// <returns>Значения эффективности согласно критерию Гермейера</returns>
        public static double[] K_Germeyer(double[,] _L, double[] _P)
        {
            if (noData(_L) || noData(_P) || (_L.GetLength(1) != _P.Length))
                return null;
            int gd = _L.GetLength(0), gs = _L.GetLength(1);
            double mp = 0.0;
            for (int i = 0; i < gd; i++)
                for (int j = 0; j < gs; j++)
                    if (_L[i, j] < mp)
                        mp = _L[i, j];
            double[,] _kw2 = (double[,])_L.Clone();
            if (mp != 0.0)
                for (int i = 0; i < gd; i++)
                    for (int j = 0; j < gs; j++)
                        _kw2[i, j] = _L[i, j] - mp;
            int[] Kpos = new int[gd];
            for (int i = 0; i < gd; i++)
            {
                Kpos[i] = 0;
                for (int j = 1; j < gs; j++)
                    if ((_kw2[i, j] * _P[j]) < (_kw2[i, Kpos[i]] * _P[Kpos[i]]))
                        Kpos[i] = j;
            }

            double[] R = new double[gd];
            for (int vd = 0; vd < gd; vd++)
                R[vd] = _kw2[vd, Kpos[vd]] * _P[Kpos[vd]];

            return R;
        }
        /// <summary>
        /// Вычисление значений эффективности согласно критерию Гермейера
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <param name="_P">Распределение вероятностей</param>
        /// <param name="_nu">Коэффициент степени доверения распределению вероятностей</param>
        /// <returns>Значения эффективности согласно критерию Гермейера</returns>
        public static double[] K_HodgeLehmann(double[,] _L, double[] _P, double _nu)
        {
            if (noData(_L) || noData(_P) || (_L.GetLength(1) != _P.Length) || (_nu < 0.0) || (_nu > 1.0))
                return null;
            int gd = _L.GetLength(0), gs = _L.GetLength(1);

            double[] svals = new double[gd];
            int[] Kpos = new int[_L.GetLength(0)];
            for (int i = 0; i < gd; i++)
            {
                svals[i] = 0.0;
                Kpos[i] = 0;
                for (int j = 0; j < gs; j++)
                {
                    svals[i] += _L[i, j] * _P[j];
                    if (_L[i, j] < _L[i, Kpos[i]])
                        Kpos[i] = j;
                }
                svals[i] /= gs;
            }

            double[] R = new double[gd];
            for (int vd = 0; vd < gd; vd++)
                R[vd] = _nu * (svals[0]) + (1 - _nu) * (_L[0, Kpos[0]]);

            return R;
        }
        /// <summary>
        /// Вычисление значений эффективности согласно критерию BL(MM)
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <param name="_P">Распределение вероятностей</param>
        /// <param name="_epsilon">Коэффициент уровня риска</param>
        /// <returns>Значения эффективности согласно критерию BL(MM)</returns>
        public static double[] K_BLMM(double[,] _L, double[] _P, double _epsilon)
        {
            if (noData(_L) || noData(_P) || (_P.Length != _L.GetLength(1)) || (_epsilon <= 0.0))
                return null;
            int gd = _L.GetLength(0), gs = _L.GetLength(1);
            
            return K_ExpectedValue(_L, _P);
        }
        /// <summary>
        /// Вычисление значений Epsilon_i и разностей максимальных выигрышей для критерия BL(MM)
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <param name="_P">Распределение вероятностей</param>
        /// <param name="_epsilon">Коэффициент уровня риска</param>
        /// <param name="_i0">Индекс i опорного элемента</param>
        /// <param name="_j0">Индекс j опорного элемента</param>
        /// <returns>Вспомогательные значения Epsilon0 и разностей максимальных выигрышей для критерия BL(MM)</returns>
        public static double[,] VM_BLMM(double[,] _L, double[] _P, double _epsilon, out int _i0, out int _j0)
        {
            if (noData(_L) || noData(_P) || (_P.Length != _L.GetLength(1)) || (_epsilon <= 0.0))
            {
                _i0 = 0;
                _j0 = 0;
                return null;
            }

            int gl0 = _L.GetLength(0), gl1 = _L.GetLength(1);
            int i0 = 0, j0 = 0;
            double[] minKm = new double[gl0];
            double[] maxKm = new double[gl0];

            for (int i = 0; i < gl0; i++)
            {
                double minKP = _L[i, 0] * _P[0];
                int minKPj = 0;
                minKm[i] = _L[i, 0];
                maxKm[i] = _L[i, 0];
                for (int j = 0; j < gl1; j++)
                {
                    double vl = _L[i, j] * _P[j];
                    if (vl < minKP)
                    {
                        minKP = vl;
                        minKPj = j;
                    }
                    if (_L[i, j] < minKm[i])
                        minKm[i] = _L[i, j];
                    if (_L[i, j] > maxKm[i])
                        maxKm[i] = _L[i, j];
                }
                if ((i == 0) || (minKP > (_L[i0, j0] * _P[j0])))
                {
                    i0 = i;
                    j0 = minKPj;
                }
            }
            double[,] R = new double[gl0, 2];
            for (int i = 0; i < gl0; i++)
            {
                double
                    epsi = _L[i0, j0] - minKm[i],
                    MaxMaxDif = maxKm[i] - maxKm[i0];
                R[i, 0] = epsi;
                R[i, 1] = MaxMaxDif;
            }
            _i0 = i0;
            _j0 = j0;
            return R;
        }
        /// <summary>
        /// Вычисление необходимости рассмотрения отдельных стратегий для критерия BL(MM)
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <param name="_P">Распределение вероятностей</param>
        /// <param name="_epsilon">Коэффициент уровня риска</param>
        /// <returns>Необходимости рассмотрения отдельных стратегий для критерия BL(MM)</returns>
        public static bool[,] uxM_BLMM(double[,] _L, double[] _P, double _epsilon)
        {
            if (noData(_L) || noData(_P) || (_P.Length != _L.GetLength(1)) || (_epsilon <= 0.0))
                return null;
            int gd = _L.GetLength(0), gs = _L.GetLength(1);

            int i0, j0;
            double[,] V = VM_BLMM(_L, _P, _epsilon, out i0, out j0);
            bool[,] R = new bool[gd, 2];
            for (int i = 0; i < gd; i++)
            {
                double
                    epsi = V[i, 0],
                    MaxMaxDif = V[i, 1];
                R[i, 0] = epsi <= _epsilon;
                R[i, 1] = MaxMaxDif <= epsi;
            }
            return R;
        }
        /// <summary>
        /// Вычисление значений эффективности в соответствии с указанным критерием
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <param name="_P">Распределение вероятностей</param>
        /// <param name="_criterion">Номер критерия</param>
        /// <param name="_param">Значение параметра критерия</param>
        /// <returns>Значения эффективности</returns>
        public static double[] KByCriterion(double[,] _L, double[] _P, int _criterion, double _param = 0.0)
        {
            double[] R = null;
            switch (_criterion)
            {
                case 0: R = K_Laplace(_L); break;
                case 1: R = K_MaxiMax(_L); break;
                case 2: R = K_MaxiMin(_L); break;
                case 3: R = K_Savage(_L); break;
                case 4: R = K_Proizv(_L); break;
                case 5: R = K_Hurwitz(_L, _param); break;
                case 6: R = K_ExpectedValue(_L, _P); break;
                case 7: R = K_Germeyer(_L, _P); break;
                case 8: R = K_HodgeLehmann(_L, _P, _param); break;
                case 9: R = K_BLMM(_L, _P, _param); break;
            }
            return R;
        }

        /// <summary>
        /// Проверка порядкового номера критерия на корректность
        /// </summary>
        /// <param name="_criterion">Номер критерия</param>
        /// <returns>Признак корректности номера</returns>
        public static bool checkCriterionNumber(int _criterion)
        {
            return ((0 <= _criterion) && (_criterion < 10));
        }
        /// <summary>
        /// Проверка корректности значения параметра
        /// </summary>
        /// <param name="_criterion">Номер критерия</param>
        /// <param name="_param">Значение параметра</param>
        /// <returns>Корректно ли значение параметра</returns>
        public static bool checkParam(int _criterion, double _param)
        {
            switch (_criterion)
            {
                case 5: return ((_param >= 0.0) && (_param <= 1.0));
                case 8: return ((_param >= 0.0) && (_param <= 1.0));
                case 9: return (_param > 0.0);
            }
            return false;
        }
        /// <summary>
        /// Проверка необходимости распределения вероятностей для критерия
        /// </summary>
        /// <param name="_criterion">Номер критерия</param>
        /// <returns>Необходимо ли распределение вероятностей для критерия</returns>
        public static bool NeedP(int _criterion)
        {
            return (_criterion >= 6);
        }
        /// <summary>
        /// Проверка необходимости параметра
        /// </summary>
        /// <param name="_criterion">Номер критерия</param>
        /// <returns>Необходим ли параметр</returns>
        public static bool NeedParam(int _criterion)
        {
            return ((_criterion == 5) || (_criterion >= 8));
        }
        /// <summary>
        /// Выбор стратегий с наибольшим значением эффективности
        /// </summary>
        /// <param name="_K">Значения эффективности</param>
        /// <param name="_uM">Допустимость использования стратегии</param>
        /// <returns>Номера стратегий с наибольшим значением эффективности</returns>
        public static int[] bestResult(double[] _K, bool[] _uM)
        {
            if (noData(_K) || ((_uM != null) && (_K.Length != _uM.Length)))
                return null;
            int gd = _K.Length;
            int[] R = null;
            bool usingB = (_uM != null), allTrue = true;
            if (usingB)
            {
                for (int i = 0; i < _uM.Length; i++)
                {
                    if (_uM[i])
                    {
                        if (R == null)
                        {
                            R = new int[1];
                            R[0] = i;
                        }
                    }
                    else
                        allTrue = false;
                    if ((R != null) && (!allTrue))
                        break;
                }
                if (R == null)
                {
                    usingB = false;
                    R = new int[1];
                    R[0] = 0;
                }
                if (allTrue)
                    usingB = false;
            }
            else
            {
                R = new int[1];
                R[0] = 0;
            }

            for (int i = R[0] + 1; i < gd; i++)
            {
                if (usingB && (!_uM[i]))
                    continue;
                if (_K[i] > _K[R[0]])
                {
                    if (R.Length != 1)
                        R = new int[1];
                    R[0] = i;
                }
                else
                    if (_K[i] == _K[R[0]])
                {
                    int[] Rold = R;
                    R = new int[Rold.Length + 1];
                    Rold.CopyTo(R, 0);
                    R[Rold.Length] = i;
                }
            }
            return R;
        }
        /// <summary>
        /// Формирование единого массива указаний на допустимые стратегии
        /// </summary>
        /// <param name="_uxM">Указание на допущения использования стратегий по различным условиям</param>
        /// <returns>Единый массив указаний на допустимые стратегии</returns>
        public static bool[] F_UM(bool[,] _uxM)
        {
            if (noData(_uxM))
                return null;
            bool[] R = new bool[_uxM.GetLength(0)];
            int cnt = _uxM.GetLength(1);
            for (int i = 0; i < R.Length; i++)
            {
                R[i] = true;
                for (int j = 0; j < cnt; j++)
                    if (!_uxM[i, j])
                    {
                        R[i] = false;
                        break;
                    }
            }
            return R;
        }
        /// <summary>
        /// Выбор стратегий в соответствии с критерием
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <param name="_P">Распредедение вероятностей</param>
        /// <param name="_criterion">Номер критерия</param>
        /// <param name="_param">Значение параметра</param>
        /// <returns>Номера стратегий в соответствии с критерием</returns>
        public static int[] resultByCriterion(double[,] _L, double[] _P, int _criterion, double _param)
        {
            double[] K = KByCriterion(_L, _P, _criterion, _param);
            if (K == null)
                return null;
            bool[] uM = (_criterion == 9) ? F_UM(uxM_BLMM(_L, _P, _param)) : null;
            return bestResult(K, uM);
        }


        /// <summary>
        /// Выбор наилучших стратегий согласно критерию Лапласа
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <returns>Наилучшие стратегии согласно критерию Лапласа</returns>
        public static int[] Laplace(double[,] _L)
        {
            if (noData(_L))
                return null;

            int gl0 = _L.GetLength(0), gl1 = _L.GetLength(1);
            double[] svals = new double[gl0];//K
            for (int i = 0; i < svals.Length; i++)
            {
                svals[i] = 0.0;
                for (int j = 0; j < gl1; j++)
                    svals[i] += _L[i, j];
                svals[i] /= gl1;
            }
            int[] R = new int[1];
            R[0] = 0;
            for (int i = 1; i < gl0; i++)
            {
                if (svals[i] > svals[R[0]])
                {
                    R = new int[1];
                    R[0] = i;
                }
                else
                if (svals[i] == svals[R[0]])
                {
                    int[] Rold = R;
                    R = new int[Rold.Length + 1];
                    Rold.CopyTo(R, 0);
                    R[Rold.Length] = i;
                }
            }
            return R;
        }
        /// <summary>
        /// Выбор наилучших стратегий согласно критерию максимакса
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <returns>Наилучшие стратегии согласно критерию максимакса</returns>
        public static int[] MaxiMax(double[,] _L)
        {//Optimist
            if (noData(_L))
                return null;

            int gl0 = _L.GetLength(0), gl1 = _L.GetLength(1);
            int[] Kpos = new int[gl0];
            for (int i = 0; i < Kpos.Length; i++)
            {
                Kpos[i] = 0;
                for (int j = 1; j < gl1; j++)
                    if (_L[i, j] > _L[i, Kpos[i]])
                        Kpos[i] = j;
            }

            int[] R = new int[1];
            R[0] = 0;

            for (int i = 1; i < gl0; i++)
            {
                if (_L[i, Kpos[i]] > _L[R[0], Kpos[R[0]]])
                {
                    R = new int[1];
                    R[0] = i;
                }
                else
                if (_L[i, Kpos[i]] == _L[R[0], Kpos[R[0]]])
                {
                    int[] Rold = R;
                    R = new int[Rold.Length + 1];
                    Rold.CopyTo(R, 0);
                    R[Rold.Length] = i;
                }
            }
            return R;
        }
        /// <summary>
        /// Выбор наилучших стратегий согласно критерию максимина
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <returns>Наилучшие стратегии согласно критерию максимина</returns>
        public static int[] MaxiMin(double[,] _L)
        {
            //Pessimist
            if (noData(_L))
                return null;

            int gl0 = _L.GetLength(0), gl1 = _L.GetLength(1);
            int[] Kpos = new int[gl0];
            for (int i = 0; i < Kpos.Length; i++)
            {
                Kpos[i] = 0;
                for (int j = 1; j < gl1; j++)
                    if (_L[i, j] < _L[i, Kpos[i]])
                        Kpos[i] = j;
            }

            int[] R = new int[1];
            R[0] = 0;

            for (int i = 1; i < gl0; i++)
            {
                if (_L[i, Kpos[i]] > _L[R[0], Kpos[R[0]]])
                {
                    R = new int[1];
                    R[0] = i;
                }
                else
                if (_L[i, Kpos[i]] == _L[R[0], Kpos[R[0]]])
                {
                    int[] Rold = R;
                    R = new int[Rold.Length + 1];
                    Rold.CopyTo(R, 0);
                    R[Rold.Length] = i;
                }
            }
            return R;
        }
        /// <summary>
        /// Выбор наилучших стратегий согласно критерию Сэвиджа
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <returns>Наилучшие стратегии согласно критерию Сэвиджа</returns>
        public static int[] Savage(double[,] _L)
        {
            if (noData(_L))
                return null;

            int gl0 = _L.GetLength(0), gl1 = _L.GetLength(1);
            int[] Mb = new int[gl1];
            for (int i = 0; i < gl1; i++)
            {
                Mb[i] = 0;
                for (int j = 1; j < gl0; j++)
                    if (_L[j, i] > _L[Mb[i], i])
                        Mb[i] = j;
            }
            double[,] r = (double[,])_L.Clone();
            for (int i = 0; i < gl1; i++)
                for (int j = 0; j < gl0; j++)
                    r[j, i] -= Mb[i];//Значения должны быть не положительными
            return MaxiMin(r);
        }
        /// <summary>
        /// Выбор наилучших стратегий согласно критерию произведений
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <returns>Наилучшие стратегии согласно критерию произведений</returns>
        public static int[] Proizv(double[,] _L)
        {
            if (noData(_L))
                return null;

            int gl0 = _L.GetLength(0), gl1 = _L.GetLength(1);
            double[] svals = new double[gl0];

            double minV = _L[0, 0];
            for (int i = 0; i < gl0; i++)
                for (int j = 0; j < gl1; j++)
                    if (_L[i, j] < minV)
                        minV = _L[i, j];
            if (minV > 0.0)
                minV = 0.0;
            for (int i = 0; i < gl0; i++)
            {
                svals[i] = (_L[i, 0] - minV);
                for (int j = 1; j < gl1; j++)
                    svals[i] *= (_L[i, j] - minV);
            }

            int[] R = new int[1];
            R[0] = 0;
            for (int i = 1; i < svals.Length; i++)
            {
                if (svals[i] > svals[R[0]])
                {
                    R = new int[1];
                    R[0] = i;
                }
                else
                if (svals[i] == svals[R[0]])
                {
                    int[] Rold = R;
                    R = new int[Rold.Length + 1];
                    Rold.CopyTo(R, 0);
                    R[Rold.Length] = i;
                }
            }
            return R;
        }
        /// <summary>
        /// Выбор наилучших стратегий согласно критерию Гурвица
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <param name="_alpha">Коэффициент уверенности</param>
        /// <returns>Наилучшие стратегии согласно критерию Гурвица</returns>
        public static int[] Hurwitz(double[,] _L, double _alpha)
        {
            if (noData(_L) || (_alpha < 0.0) || (_alpha > 1.0))
                return null;

            int gl0 = _L.GetLength(0), gl1 = _L.GetLength(1);
            int[] minM = new int[gl0];
            int[] maxM = new int[gl0];
            for (int i = 0; i < gl0; i++)
            {
                minM[i] = 0;
                maxM[i] = 0;
                for (int j = 1; j < gl1; j++)
                {
                    if (_L[i, j] < _L[i, minM[i]])
                        minM[i] = j;
                    if (_L[i, j] > _L[i, maxM[i]])
                        maxM[i] = j;
                }
            }
            double[] Kval = new double[gl0];
            for (int i = 0; i < gl0; i++)
                Kval[i] = _L[i, minM[i]] + _alpha * (_L[i, maxM[i]] - _L[i, minM[i]]);

            int[] R = new int[1];
            R[0] = 0;

            for (int i = 1; i < gl0; i++)
            {
                if (Kval[i] > Kval[R[0]])
                {
                    R = new int[1];
                    R[0] = i;
                }
                else
                if (Kval[i] == Kval[R[0]])
                {
                    int[] Rold = R;
                    R = new int[Rold.Length + 1];
                    Rold.CopyTo(R, 0);
                    R[Rold.Length] = i;
                }
            }
            return R;
        }
        /// <summary>
        /// Выбор наилучших стратегий согласно критерию Байеса-Лапласа
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <param name="_P">Распределение вероятностей</param>
        /// <returns>Наилучшие стратегии согласно критерию Байеса-Лапласа</returns>
        public static int[] ExpectedValue(double[,] _L, double[] _P)
        {//Realist, Bayesian(-Laplacian)
            if (noData(_L) || noData(_P) || (_P.Length != _L.GetLength(1)))
                return null;

            int gl0 = _L.GetLength(0), gl1 = _L.GetLength(1);
            double[] svals = new double[gl0];
            for (int i = 0; i < gl0; i++)
            {
                svals[i] = 0.0;
                for (int j = 0; j < gl1; j++)
                    svals[i] += _L[i, j] * _P[j];
                svals[i] /= gl1;//Как среднее значение
            }
            int[] R = new int[1];
            R[0] = 0;
            for (int i = 1; i < gl0; i++)
            {
                if (svals[i] > svals[R[0]])
                {
                    R = new int[1];
                    R[0] = i;
                }
                else
                if (svals[i] == svals[R[0]])
                {
                    int[] Rold = R;
                    R = new int[Rold.Length + 1];
                    Rold.CopyTo(R, 0);
                    R[Rold.Length] = i;
                }
            }
            return R;
        }
        /// <summary>
        /// Выбор наилучших стратегий согласно критерию Гермейера
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <param name="_P">Распределение вероятностей</param>
        /// <returns>Наилучшие стратегии согласно критерию Гермейера</returns>
        public static int[] Germeyer(double[,] _L, double[] _P)
        {
            if (noData(_L) || noData(_P) || (_P.Length != _L.GetLength(1)))
                return null;

            int gl0 = _L.GetLength(0), gl1 = _L.GetLength(1);
            double mp = 0.0;
            for (int i = 0; i < gl0; i++)
                for (int j = 0; j < gl1; j++)
                    if (_L[i, j] < mp)
                        mp = _L[i, j];
            if (mp != 0.0)
                for (int i = 0; i < gl0; i++)
                    for (int j = 0; j < gl1; j++)
                        _L[i, j] -= mp;
            int[] Kpos = new int[gl0];
            for (int i = 0; i < gl0; i++)
            {
                Kpos[i] = 0;
                for (int j = 1; j < gl1; j++)
                    if ((_L[i, j] * _P[j]) < (_L[i, Kpos[i]] * _P[Kpos[i]]))
                        Kpos[i] = j;
            }

            double lastVal = _L[0, Kpos[0]] * _P[Kpos[0]];
            int[] R = new int[1];
            R[0] = 0;

            for (int i = 1; i < gl0; i++)
            {
                double curVal = _L[i, Kpos[i]] * _P[Kpos[i]];
                if (curVal > lastVal)
                {
                    R = new int[1];
                    R[0] = i;
                }
                else
                //if (_kw[i, Kpos[i]] == _kw[R[0], Kpos[R[0]]])
                if (curVal == lastVal)
                {
                    int[] Rold = R;
                    R = new int[Rold.Length + 1];
                    Rold.CopyTo(R, 0);
                    R[Rold.Length] = i;
                }
            }
            return R;
        }
        /// <summary>
        /// Выбор наилучших стратегий согласно критерию Ходжа-Лемана
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <param name="_P">Распределение вероятностей</param>
        /// <param name="_nu">Коэффициент степени доверения распределению вероятностей</param>
        /// <returns>Наилучшие стратегии согласно критерию Ходжа-Лемана</returns>
        public static int[] HodgeLehmann(double[,] _L, double[] _P, double _nu)
        {
            if (noData(_L) || noData(_P) || (_P.Length != _L.GetLength(1)) || (_nu < 0.0) || (_nu > 1.0))
                return null;

            int gl0 = _L.GetLength(0), gl1 = _L.GetLength(1);
            double[] svals = new double[gl0];
            int[] Kpos = new int[_L.GetLength(0)];
            for (int i = 0; i < gl0; i++)
            {
                svals[i] = 0.0;
                Kpos[i] = 0;
                for (int j = 0; j < gl1; j++)
                {
                    svals[i] += _L[i, j] * _P[j];
                    if (_L[i, j] < _L[i, Kpos[i]])
                        Kpos[i] = j;
                }
                svals[i] /= gl1;
            }

            double lastVal = _nu * (svals[0]) + (1 - _nu) * (_L[0, Kpos[0]]);
            int[] R = new int[1];
            R[0] = 0;
            for (int i = 1; i < gl0; i++)
            {
                double curVal = _nu * (svals[i]) + (1 - _nu) * (_L[i, Kpos[i]]);
                if (curVal > lastVal)
                {
                    R = new int[1];
                    R[0] = i;
                }
                else
                if (curVal == lastVal)
                {
                    int[] Rold = R;
                    R = new int[Rold.Length + 1];
                    Rold.CopyTo(R, 0);
                    R[Rold.Length] = i;
                }
            }
            return R;
        }
        /// <summary>
        /// Выбор наилучших стратегий согласно критерию BL(MM)
        /// </summary>
        /// <param name="_L">Значения выигрышей</param>
        /// <param name="_P">Распределение вероятностей</param>
        /// <param name="_nu">Коэффициент уровня риска</param>
        /// <returns>Наилучшие стратегии согласно критерию BL(MM)</returns>
        public static int[] BLMM(double[,] _L, double[] _P, double _epsilon)
        {
            if (noData(_L) || noData(_P) || (_P.Length != _L.GetLength(1)) || (_epsilon <= 0.0))
                return null;

            int gd = _L.GetLength(0), gs = _L.GetLength(1);
            int i0 = 0, j0 = 0;
            double[] minKm = new double[gd];
            double[] maxKm = new double[gd];

            for (int i = 0; i < gd; i++)
            {
                double minKP = _L[i, 0] * _P[0];
                int minKPj = 0;
                minKm[i] = _L[i, 0];
                maxKm[i] = _L[i, 0];
                for (int j = 0; j < gs; j++)
                {
                    double vl = _L[i, j] * _P[j];
                    if (vl < minKP)
                    {
                        minKP = vl;
                        minKPj = j;
                    }
                    if (_L[i, j] < minKm[i])
                        minKm[i] = _L[i, j];
                    if (_L[i, j] > maxKm[i])
                        maxKm[i] = _L[i, j];
                }
                if ((i == 0) || (minKP > (_L[i, minKPj] * _P[j0])))
                {
                    i0 = i;
                    j0 = minKPj;
                }
            }
            int[] rbi = new int[gd];
            int bcount = 0;
            for (int i = 0; i < gd; i++)
            {
                double
                    eps0 = (_L[i0, j0] - minKm[i]),
                    MaxMaxDif = maxKm[i] - maxKm[i0];
                if ((eps0 <= _epsilon) && (MaxMaxDif <= eps0))
                {
                    rbi[bcount] = i;
                    bcount++;
                }
            }
            int[] R;
            if ((bcount == 0) || (bcount == gd))
                R = ExpectedValue(_L, _P);
            else
            {
                double[,] L2 = new double[bcount, gs];
                bcount = 0;
                for (int i = 0; i < bcount; i++)
                    for (int j = 0; j < gs; j++)
                        L2[i, j] = _L[rbi[i], j];
                R = ExpectedValue(L2, _P);
            }
            for (int k = 0; k < R.Length; k++)
                R[k] = rbi[R[k]];
            return R;
        }



        /// <summary>
        /// Вывод целочисленного значения
        /// </summary>
        /// <param name="_s">Заголовок</param>
        /// <param name="_v">Значение</param>
        public static void showIt(string _s, int _v)
        {
            if (_s != null)
                Console.Out.Write(_s + ": ");
            Console.Out.WriteLine(_v);
        }
        /// <summary>
        /// Вывод вещественного значения
        /// </summary>
        /// <param name="_s">Заголовок</param>
        /// <param name="_v">Значение</param>
        public static void showIt(string _s, double _v)
        {
            if (_s != null)
                Console.Out.Write(_s + ": ");
            Console.Out.WriteLine(_v);
        }
        /// <summary>
        /// Вывод массива булевых значений
        /// </summary>
        /// <param name="_s">Заголовок</param>
        /// <param name="_M">Массив булевых значений</param>
        public static void showIt(string _s, bool[] _M)
        {
            if (_s != null)
                Console.Out.WriteLine(_s + ": ");
            for (int i = 0; i < _M.Length; i++)
                Console.Write("" + (_M[i] ? '+' : '.') + "\t");
            Console.Out.WriteLine();
        }
        /// <summary>
        /// Вывод двумерного массива булевых значений
        /// </summary>
        /// <param name="_s">Заголовок</param>
        /// <param name="_M">Двумерный массив булевых значений</param>
        public static void showIt(string _s, bool[,] _M)
        {
            if (_s != null)
                Console.Out.WriteLine(_s);
            int gl0 = _M.GetLength(0), gl1 = _M.GetLength(1);
            for (int i = 0; i < gl0; i++)
            {
                for (int j = 0; j < gl1; j++)
                    Console.Write("" + (_M[i, j] ? '+' : '.') + "\t");
                Console.Out.WriteLine();
            }
        }
        /// <summary>
        /// Вывод массива целочисленных значений
        /// </summary>
        /// <param name="_s">Заголовок</param>
        /// <param name="_M">Массив целочисленных значений</param>
        public static void showIt(string _s, int[] _M)
        {
            if (_s != null)
                Console.Out.WriteLine(_s + ": ");
            for (int i = 0; i < _M.Length; i++)
                Console.Write("" + _M[i] + "\t");
            Console.Out.WriteLine();
        }
        /// <summary>
        /// Вывод двумерного массива целочисленных значений
        /// </summary>
        /// <param name="_s">Заголовок</param>
        /// <param name="_M">Двумерный массив целочисленных значений</param>
        public static void showIt(string _s, int[,] _M)
        {
            if (_s != null)
                Console.Out.WriteLine(_s);
            int gl0 = _M.GetLength(0), gl1 = _M.GetLength(1);
            for (int i = 0; i < gl0; i++)
            {
                for (int j = 0; j < gl1; j++)
                    Console.Write("" + _M[i, j] + "\t");
                Console.Out.WriteLine();
            }
        }
        /// <summary>
        /// Вывод массива вещественных значений
        /// </summary>
        /// <param name="_s">Заголовок</param>
        /// <param name="_M">Массив вещественных значений</param>
        public static void showIt(string _s, double[] _M)
        {
            if (_s != null)
                Console.Out.WriteLine(_s);
            for (int i = 0; i < _M.Length; i++)
                Console.Write("" + _M[i] + "\t");
            Console.Out.WriteLine();
        }
        /// <summary>
        /// Вывод двумерного массива вещественных значений
        /// </summary>
        /// <param name="_s">Заголовок</param>
        /// <param name="_M">Двумерный массив вещественных значений</param>
        public static void showIt(string _s, double[,] _M)
        {
            if (_s != null)
                Console.Out.WriteLine(_s);
            int gl0 = _M.GetLength(0), gl1 = _M.GetLength(1);
            for (int i = 0; i < gl0; i++)
            {
                for (int j = 0; j < gl1; j++)
                    Console.Write("" + _M[i, j] + "\t");
                Console.Out.WriteLine();
            }
        }
        /// <summary>
        /// Вывод обозначений превышения порогового значения вероятности относительно области останова
        /// </summary>
        /// <param name="_s">Заголовок</param>
        /// <param name="_M">Обозначения превышения значения области останова</param>
        public static void showWhereStop(string _s, bool[,] _M)
        {
            if (_M == null)
            {
                Console.Out.WriteLine();
                return;
            }
            if (_s != null)
                Console.Out.WriteLine(_s);
            int gl0 = _M.GetLength(0), gl1 = _M.GetLength(1);
            for (int i = 0; i < gl0; i++)
            {
                for (int j = 0; j < gl1; j++)
                    Console.Out.Write((_M[i, j]) ? '+' : '.');
                Console.Out.WriteLine();
            }

        }
        /// <summary>
        /// Вывод номеров стратегий для соответствующих критериев
        /// </summary>
        /// <param name="_s">Заголовок</param>
        /// <param name="_SM">Название критерия</param>
        /// <param name="_M">Множества стратегий</param>
        public static void showCriteriaResults(string _s, string[] _SM, int[][] _M)
        {
            if (_s != null)
                Console.Out.WriteLine(_s);
            for (int i = 0; i < _M.Length; i++)
            {
                Console.Out.WriteLine(_SM[i] + ":");
                for (int j = 0; j < _M[i].Length; j++)
                    Console.Write("" + _M[i][j] + "\t");
                Console.Out.WriteLine();
            }
        }

        /// <summary>
        /// Вычисление ожидаемых выигрышей для каждой стратегии
        /// </summary>
        /// <param name="_L">Ожидаемые выигрыши</param>
        /// <param name="_P">Распределение вероятностей</param>
        /// <returns>Ожидаемые выигрыши для стратегий</returns>
        public static double[] F_ML(double[,] _L, double[] _P)
        {
            if (noData(_L) || noData(_P) || (_L.GetLength(1) != _P.Length))
                return null;
            int gd = _L.GetLength(0), gs = _L.GetLength(1);
            double[] R = new double[gd];
            for (int vd = 0; vd < gd; vd++)
            {
                R[vd] = 0.0;
                for (int vs = 0; vs < gs; vs++)
                    R[vd] += _L[vd, vs] * _P[vs];
            }
            return R;
        }
        /// <summary>
        /// Вычисление ожидаемых выигрышей для всех стратегий
        /// </summary>
        /// <param name="_L">Ожидаемые выигрыши</param>
        /// <param name="_Ps">Распределения вероятностей</param>
        /// <returns>Ожидаемые выигрыши для стратегии</returns>
        public static double[,] F_MLs(double[,] _L, double[,] _Ps)
        {
            if (noData(_L) || noData(_Ps) || (_L.GetLength(1) != _Ps.GetLength(1)))
                return null;
            int gz = _Ps.GetLength(0), gd = _L.GetLength(0), gs = _L.GetLength(1);
            double[,] R = new double[gz, gd];
            for (int vz = 0; vz < gz; vz++)
                for (int vd = 0; vd < gd; vd++)
                {
                    R[vz, vd] = 0.0;
                    for (int vs = 0; vs < gs; vs++)
                        R[vz, vd] += _L[vd, vs] * _Ps[vz, vs];
                }
            return R;
        }
        /// <summary>
        /// Обозначение превышения или достижения вероятностей пороговых значений области останова
        /// </summary>
        /// <param name="_P">Распределение вероятностей</param>
        /// <param name="_b">Область останова</param>
        /// <returns>Массив превышения значений</returns>
        public static bool[] F_Ti(double[] _P, double[] _b)
        {
            if (noData(_P) || noData(_b) || (_P.Length != _b.Length))
                return null;
            bool[] R = new bool[_P.Length];
            for (int i = 0; i < R.Length; i++)
                R[i] = (_P[i] >= _b[i]);
            return R;
        }
        /// <summary>
        /// Обозначение превышения или достижения вероятностей пороговых значений области останова для множества стратегий
        /// </summary>
        /// <param name="_Psz">Распределения вероятностей для множества стратегий</param>
        /// <param name="_b">Область останова</param>
        /// <returns>Массив превышения значений для множества стратегий</returns>
        public static bool[,] F_Tis(double[,] _Psz, double[] _b)
        {
            if (noData(_Psz) || noData(_b) || (_Psz.GetLength(1) != _b.Length))
                return null;
            int gz = _Psz.GetLength(0), gs = _Psz.GetLength(1);
            bool[,] R = new bool[gz, gs];
            for (int vz = 0; vz < gz; vz++)
                for (int vs = 0; vs < gs; vs++)
                    R[vz, vs] = (_Psz[vz, vs] >= _b[vs]);
            return R;
        }
        /// <summary>
        /// Формирование части дерева решений
        /// </summary>
        /// <param name="_EIM">ИНформация для очередного эксперимента</param>
        /// <param name="_expnum">Номер эксперимента (с 0-го)</param>
        /// <param name="_L">Значения выигрышей</param>
        /// <param name="_P">распределение вероятностей</param>
        /// <param name="_wcprev">Максимальные/минимальные ожидаемые выигрыши/потери предыдущего эксперимента</param>
        /// <param name="_criterion">Номер критерия</param>
        /// <param name="_param">Значение параметра</param>
        /// <param name="_Re">Данные о результатах эксперимента</param>
        /// <param name="_DTEN">Элемент дерева решений</param>
        /// <returns>Признак отсутствия ошибок в ходе выполненияreturns>
        public static bool fDTN(
            InputInfo.ExpInputInfo[] _EIM,
            int _expnum,
            double[,] _L,
            double[] _P,
            double _wcprev,
            int _criterion,
            double _param,
            ref ExpResultData _Re,
            out DecisionTree.ExperimentNode _DTEN)
        {
            _DTEN = null;
            bool bres = Exp(
                _L, _P, _wcprev, _EIM[_expnum].b,
                _EIM[_expnum].Pzs, _EIM[_expnum].c,
                _criterion, _param,
                ref _Re);
            if (!bres)//Построение дерева прервано ошибкой
                return false;
            //_Re.showIt("Результаты эксперимента");
            if (_Re.expv <= 0.0)//Эксперимент не проводится по причине слишком высокой стоимости
                return true;
            //Текущий эксперимент проводится
            _DTEN = new DecisionTree.ExperimentNode(_Re.FR);
            int expnumnext = _expnum + 1;
            if (expnumnext == _EIM.Length)
                return true;

            double[] PszZ = new double[_Re.Psz.GetLength(1)];

            //Для каждого результата эксперимента
            for (int i = 0; i < _DTEN.Decisions.Length; i++)
            {
                bool tix = false;
                for (int j = 0; j < _Re.tis.GetLength(1); j++)
                    if (_Re.tis[i, j])
                    {
                        tix = true;
                        break;
                    }
                //Если есть вхождение в область останова на основе апостериорных вероятностей
                if (tix)
                    _DTEN.Experiments[i] = null;//Следующий эксперимент не проводится
                else
                {
                    for (int j = 0; j < PszZ.Length; j++)
                        PszZ[j] = _Re.Psz[i, j];
                    ExpResultData ReZ = null;
                    fDTN(_EIM, expnumnext, _L, PszZ, _Re.op[i] - _EIM[_expnum].c, _criterion, _param, ref ReZ, out _DTEN.Experiments[i]);
                }
            }
            return true;
        }
        /// <summary>
        /// Автоматическое формирование дерева решений
        /// </summary>
        /// <param name="_II">Исходные данные</param>
        /// <param name="_criterion">Номер критерия</param>
        /// <param name="_criterion_reason">Обоснование критерия</param>
        /// <param name="_param">Значение параметра</param>
        /// <param name="_DT">Формируемое дерево решений</param>
        /// <returns>Удалось ли выполнить построение</returns>
        public static bool formDecisionTree(
            InputInfo _II, 
            int _criterion, 
            int _criterion_reason, 
            double _param, 
            out DecisionTree _DT)
        {
            _DT = null;
            if (
                (_II == null) ||
                noData(_II.NEI.L) ||
                ((NeedParam(_criterion)) && (noData(_II.NEI.P))) ||
                ((_II.NEI.P != null) && (_II.NEI.L.GetLength(1) != _II.NEI.P.Length)) ||
                ((_II.NEI.b != null) && (_II.NEI.L.GetLength(1) != _II.NEI.b.Length)) ||
                (!checkCriterionNumber(_criterion)) ||
                ((NeedParam(_criterion)) && (!checkParam(_criterion, _param))) ||
                (noData(_II.NEI.P) && (!noData(_II.EIM)))
                )
                return false;
            if (_II.EIM != null)
                for (int i = 0; i < _II.EIM.Length; i++)
                {
                    InputInfo.ExpInputInfo EII = _II.EIM[i];
                    if (
                        noData(EII.Pzs) ||
                        (EII.c < 0.0) ||
                        ((NeedParam(_criterion)) && (noData(EII.Pzs))) ||
                        ((EII.Pzs != null) && (_II.NEI.L.GetLength(1) != EII.Pzs.GetLength(1))) ||
                        ((EII.b != null) && (_II.NEI.L.GetLength(1) != EII.b.Length)) ||
                        (!checkCriterionNumber(_criterion)) ||
                        ((NeedParam(_criterion)) && (!checkParam(_criterion, _param)))
                        )
                        return false;
                }
            NoExpResultData Rne = null;
            ExpResultData Re = null;
            NoExp(_II.NEI.L, _II.NEI.P, _II.NEI.b, _criterion, _param, ref Rne);

            //Rn.showIt("Игра без эксперимента");

            _DT = new DecisionTree(_criterion, _criterion_reason, Rne.crResult);

            bool stpExp = false;
            for (int i = 0; i < Rne.ti.Length; i++)
                if (Rne.ti[i])
                {
                    stpExp = true;
                    break;
                }
            if (!stpExp)//Нет входа в область останова
                fDTN(
                    _II.EIM, 0, _II.NEI.L, _II.NEI.P,
                    Rne.ML[Rne.crResult], _criterion, _param,
                    ref Re, out _DT.FirstExperiment);

            return true;
        }
        /// <summary>
        /// Вычисления по игре без эксперимента
        /// </summary>
        /// <param name="_L">Ожидаемые выигрыши</param>
        /// <param name="_P">Распределение вероятностей</param>
        /// <param name="_b">Область останова</param>
        /// <param name="_criterion">Номер критерия</param>
        /// <param name="_param">Значение параметра</param>
        /// <param name="_NERD">Данные об игре бех эксперимента</param>
        /// <returns>Удалось ли выполнить вычисления</returns>
        public static bool NoExp(
            double[,] _L,
            double[] _P,
            double[] _b,
            int _criterion,
            double _param,
            ref NoExpResultData _NERD
            )
        {
            if (
                noData(_L) ||
                (NeedP(_criterion) && noData(_P)) ||
                ((_P != null) &&
                    (
                    ((_L.GetLength(1) != _P.Length)) ||
                    ((_b != null) && (_P.Length != _b.Length))
                    )
                ) ||
                (!checkCriterionNumber(_criterion)) ||
                (NeedParam(_criterion) && (!checkParam(_criterion, _param)))
            )
                return false;

            int gd = _L.GetLength(0), gs = _L.GetLength(1);
            bool[] ti = (_b == null) ? null : F_Ti(_P, _b);
            double[] ML = F_ML(_L, _P);

            double[] K = KByCriterion(_L, _P, _criterion, _param);
            double[,] VM = null;
            bool[,] uxM = null;
            bool[] uM = null;
            if (_criterion == 9)
            {
                int i0, j0;
                VM = VM_BLMM(_L, _P, _param, out i0, out j0);
                uxM = uxM_BLMM(_L, _P, _param);
                uM = F_UM(uxM);
            }

            int crResult = bestResult(K, uM)[0];

            if (_NERD == null)
                _NERD = new NoExpResultData(ti, ML, K, VM, uxM, uM, crResult);
            else
            {
                _NERD.ti = ti;
                _NERD.ML = ML;
                _NERD.K = K;
                _NERD.VM = VM;
                _NERD.uxM = uxM;
                _NERD.uM = uM;
                _NERD.crResult = crResult;
            }
            return true;
        }
        /// <summary>
        /// Вычисление полных вероятностей
        /// </summary>
        /// <param name="_P">Априорные вероятности</param>
        /// <param name="_Pzs">Условные вероятности</param>
        /// <returns>Полные вероятности</returns>
        public static double[] F_Pz(
            double[] _P,
            double[,] _Pzs
            )
        {
            int gz = _Pzs.GetLength(0), gs = _Pzs.GetLength(1);
            double[] R = new double[gz];
            for (int vz = 0; vz < gz; vz++)
            {
                R[vz] = 0.0;
                for (int vs = 0; vs < gs; vs++)
                    R[vz] += (_Pzs[vz, vs] * _P[vs]);
            }
            return R;
        }
        /// <summary>
        /// Формирование апостериорных вероятностей
        /// </summary>
        /// <param name="_P">Априорные вероятности</param>
        /// <param name="_Pzs">Условные вероятности</param>
        /// <param name="_Pz">Полные вероятности</param>
        /// <returns>Апостериорные вероятности</returns>
        public static double[,] F_Psz(
            double[] _P,
            double[,] _Pzs,
            out double[] _Pz
            )
        {
            int gz = _Pzs.GetLength(0), gs = _Pzs.GetLength(1);
            _Pz = F_Pz(_P, _Pzs);
            double[,] R = new double[gz, gs];
            for (int vz = 0; vz < gz; vz++)
                for (int vs = 0; vs < gs; vs++)
                    R[vz, vs] = (_Pzs[vz, vs] * _P[vs]) / _Pz[vz];
            return R;
        }
        /// <summary>
        /// Формирование решающей функции
        /// </summary>
        /// <param name="_L"></param>
        /// <param name="_Psz">Апостреиорные вероятности</param>
        /// <param name="_criterion">Номер критерия</param>
        /// <param name="_param">Значение параметра</param>
        /// <returns>Номер решения для исхода эксперимента</returns>
        public static int[] F_FR
            (
            double[,] _L,
            double[,] _Psz,
            int _criterion,
            double _param
            )
        {
            if (
                noData(_L) ||
                noData(_Psz) ||
                (_L.GetLength(1) != _Psz.GetLength(1)) ||
                (!checkCriterionNumber(_criterion)) ||
                (NeedParam(_criterion) && (!checkParam(_criterion, _param)))
            )
                return null;
            int gd = _L.GetLength(0), gs = _L.GetLength(1), gz = _Psz.GetLength(0);
            int[] R = new int[gz];
            double[] P = new double[gs];
            for (int vz = 0; vz < gz; vz++)
            {
                for (int vs = 0; vs < gs; vs++)
                    P[vs] = _Psz[vz, vs];
                R[vz] = resultByCriterion(_L, P, _criterion, _param)[0];
            } 
            return R;
        }
        /// <summary>
        /// Вычисления по очередному эксперименту
        /// </summary>
        /// <param name="_L">Ожидаемые выигрыша</param>
        /// <param name="_P">Распределение априорных вероятностей</param>
        /// <param name="_wcprev">Соответствующий ожидаемый выигрыш предыдущего эксперимента (с учётом стоимости)</param>
        /// <param name="_b">Область останова</param>
        /// <param name="_Pzs">Матрица условных вероятностей</param>
        /// <param name="_c">Стоимость эксперимента</param>
        /// <param name="_criterion">Номер критерия</param>
        /// <param name="_param">Параметр критерия</param>
        /// <param name="_ERD">Результаты расчётов</param>
        /// <returns>Удалось ли выполнить вычисления</returns>
        public static bool Exp(
            double[,] _L,
            double[] _P,
            double _wcprev,
            double[] _b,
            double[,] _Pzs,
            double _c,
            int _criterion,
            double _param,
            ref ExpResultData _ERD
            )
        {
            if (
                noData(_L) ||
                (NeedP(_criterion) && noData(_P)) ||
                ((_P != null) &&
                    (
                    ((_L.GetLength(1) != _P.Length)) ||
                    ((_b != null) && (_P.Length != _b.Length))
                    )
                ) ||
                noData(_Pzs) ||
                (_L.GetLength(1) != _Pzs.GetLength(1)) ||
                (_c < 0.0) ||
                (!checkCriterionNumber(_criterion)) ||
                (NeedParam(_criterion) && (!checkParam(_criterion, _param)))
            )
                return false;
            int gd = _L.GetLength(0), gs = _L.GetLength(1), gz = _Pzs.GetLength(0);
            double[] Pz;
            double[,] Psz = F_Psz(_P, _Pzs, out Pz);
            bool[,] tis = (_b == null) ? null : F_Tis(Psz, _b);
            double[,] MLs = F_MLs(_L, Psz);

            double[,] Ks = new double[gz, gd];//(gz,gd)
            double[,,] VMs = (_criterion == 9)? new double[gz, gd, 2] : null;//(gz,gd,2)
            bool[,,] uxMs = new bool[gz, gd, 2];//(gz,gd,2)
            bool[,] uMs = (_criterion == 9) ? new bool[gz, gd] : null;//(gz,gd)
            int[] FR = new int[gz];//(gz)
            double[] PMst = new double[gs];//gs
            for (int vz = 0; vz < gz; vz++)
            {
                for (int vs = 0; vs < gs; vs++)
                    PMst[vs] = Psz[vz, vs];
                double[] Kt = KByCriterion(_L, PMst, _criterion, _param);
                for (int vd = 0; vd < gd; vd++)
                    Ks[vz, vd] = Kt[vd];

                if (_criterion == 9)
                {
                    int i0, j0;
                    double[,] VMt = VM_BLMM(_L, PMst, _param, out i0, out j0);//(gd,2)
                    bool[,] uxMt = uxM_BLMM(_L, PMst, _param);//(gd,2)
                    int iCnt = uxMt.GetLength(1);
                    for (int vd = 0; vd < gd; vd++)
                        for (int i = 0; i < iCnt; i++)
                        {
                            VMs[vz, vd, i] = VMt[vd, i];
                            uxMs[vz, vd, i] = uxMt[vd, i];
                        }

                    bool[] uMt = F_UM(uxMt);
                    for (int vd = 0; vd < gd; vd++)
                        uMs[vz, vd] = uMt[vd];

                    FR[vz] = bestResult(Kt, uMt)[0];
                }
            }

            double[] op = new double[gz];
            for (int vz = 0; vz < gz; vz++)
                op[vz] = MLs[vz, FR[vz]];
            double Oop = 0.0;
            for (int vz = 0; vz < gz; vz++)
                Oop += op[vz] * Pz[vz];
            double mexp = _wcprev - Oop;
            double expv = mexp - _c;

            if (_ERD == null)
                _ERD = new ExpResultData(
                    Pz, Psz, tis, MLs,
                    Ks, VMs, uxMs, uMs,
                    FR, op, Oop, mexp,
                    expv);
            else
            {
                _ERD.Pz = Pz;
                _ERD.Psz = Psz;
                _ERD.tis = tis;
                _ERD.MLs = MLs;
                _ERD.Ks = Ks;
                _ERD.VMs = VMs;
                _ERD.uxMs = uxMs;
                _ERD.uMs = uMs;
                _ERD.FR = FR;
                _ERD.op = op;
                _ERD.Oop = Oop;
                _ERD.mexp = mexp;
                _ERD.expv = expv;
            }
            return true;
        }
        /// <summary>
        /// Проверка работоспособности программного кода
        /// </summary>
        /// <returns>Отсутствуют ли ошибки</returns>
        public static bool work3()
        {
            ExpResultData[] Rem = new ExpResultData[4];

            InputInfo II = new InputInfo(2);
            II.NEI = new InputInfo.NoExpInputInfo();
            II.NEI.b = new double[] { 0.8, 0.8 };
            II.NEI.L = new double[,]
                {
                    { -30.0 ,25.0 },
                    { 40.0, -45.0 }
                };
            II.NEI.P = new double[] { 0.4, 0.6 };
            II.EIM = new InputInfo.ExpInputInfo[2];
            II.EIM[0] = new InputInfo.ExpInputInfo();
            II.EIM[0].b = new double[] { 0.8, 0.8 };
            II.EIM[0].Pzs = new double[,]
                {
                    { 0.6, 0.1 },
                    { 0.2, 0.2 },
                    { 0.2, 0.7 }
                };
            II.EIM[0].c = 5.0;
            II.EIM[0] = new InputInfo.ExpInputInfo();
            II.EIM[1].b = new double[] { 0.8, 0.8 };
            II.EIM[1].Pzs = new double[,]
                {
                    { 0.8, 0.1 },
                    { 0.1, 0.3 },
                    { 0.1, 0.6 }
                };
            II.EIM[1].c = 4.0;

            DecisionTree DT;
            formDecisionTree(II, 6, 6, 0, out DT);
            //DT.showIt("Описание принятия решения");
            return true;
        }
    }
    /// <summary>
    /// Информация для составления дерева решений
    /// </summary>
    public class InputInfo
    {
        /// <summary>
        /// Информация при отсутствии эксперимента
        /// </summary>
        public class NoExpInputInfo
        {
            /// <summary>
            /// Область продолжения (s)
            /// </summary>
            public double[] b;
            /// <summary>
            /// Матрица потерь (d,s)
            /// </summary>
            public double[,] L;
            /// <summary>
            /// Априорные вероятности (s)
            /// </summary>
            public double[] P;
            public NoExpInputInfo()
            {
                this.L = null;
                this.P = null;
                this.b = null;
            }
        }
        /// <summary>
        /// Информация при наличии эксперимента
        /// </summary>
        public class ExpInputInfo
        {
            /// <summary>
            /// Область продолжения (s)
            /// </summary>
            public double[] b;
            /// <summary>
            /// Матрица условных вероятностей (z,s)
            /// </summary>
            public double[,] Pzs;
            /// <summary>
            /// Стоимость эксперимента
            /// </summary>
            public double c;
            public ExpInputInfo()
            {
                this.b = null;
                this.Pzs = null;
                this.c = 0.0;
            }
        }
        /// <summary>
        /// Информация для игры без эксперимента
        /// </summary>
        public NoExpInputInfo NEI;
        /// <summary>
        /// Информация для игр с экспериментами
        /// </summary>
        public ExpInputInfo[] EIM;
        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public InputInfo()
        {
            this.NEI = null;
            this.EIM = null;
        }
        /// <summary>
        /// Создание с указанием количества экспериментов
        /// </summary>
        /// <param name="_experiments">Количество экспериментов</param>
        public InputInfo(int _experiments)
        {
            this.NEI = null;
            this.EIM = new ExpInputInfo[_experiments];
            for (int i = 0; i < _experiments; i++)
                this.EIM[i] = null;
        }
    }
    /// <summary>
    /// Класс программы
    /// </summary>
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Console.OutputEncoding = System.Text.Encoding.Unicode;
            //Console.InputEncoding = System.Text.Encoding.Unicode;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}




//public class t1
//{
//    public int v1;
//    public t1()
//    {
//        this.v1 = 0;
//    }
//    public void fx()
//    {
//        this.v1 += 10;
//    }
//}
//public class t2 : t1
//{
//    public int v2;
//    public t2()
//    {
//        this.v2 = 0;
//    }
//    public new void fx()
//    {
//        base.fx();
//        this.v2 += 10;
//    }
//}

/*
public static int[] MiniMax(double[,] _kw)//l
{//Opportunist, Wald
    if (_kw == null)
        return null;
    if ((_kw.GetLength(0) == 0) || (_kw.GetLength(1) == 0))
        return new int[0];

    int[] Kpos = new int[_kw.GetLength(0)];
    for (int i = 0; i < Kpos.Length; i++)
    {
        Kpos[i] = 0;
        for (int j = 1; j < _kw.GetLength(1); j++)
            if (_kw[i, j] < _kw[i, Kpos[i]])
                Kpos[i] = j;
    }

    int[] R = new int[1];
    R[0] = 0;

    for (int i = 1; i < Kpos.Length; i++)
    {
        if (_kw[i, Kpos[i]] > _kw[R[0], Kpos[R[0]]])
        {
            R = new int[1];
            R[0] = i;
        }
        else
        if (_kw[i, Kpos[i]] == _kw[R[0], Kpos[R[0]]])
        {
            int[] Rold = R;
            R = new int[Rold.Length + 1];
            Rold.CopyTo(R, 0);
            R[Rold.Length] = i;
        }
    }
    return R;
}
*/

/*
        public static bool work1()
        {
            Console.Out.WriteLine("Part1");

            double[,] k =
                {
                { 4.0, 5.0, 1.0, 2.0 },
                { 3.0, 2.0, 1.0, 8.0 },
                { 2.0, 1.0, 9.0, 5.0 },
                { 8.0, 6.0, 6.0, 7.0 },
                { 2.0, 3.0, 4.0, 5.0 }
                };
            Console.Out.WriteLine(k.GetLength(0));
            Console.Out.WriteLine(k.GetLength(1));
            double[] P = { 0.1, 0.2, 0.4, 0.3 };
            double aplha = 0.5;
            double nu = 0.5;
            double epsilon = 0.5;
            int[][] Results =
                {
                Methods.ExpectedValue(k, P),
                Methods.MaxiMax(k),
                Methods.MaxiMin(k),
                //Methods.MiniMax(k),
                Methods.Laplace(k),
                Methods.Savage(k),
                Methods.Hurwitz(k, aplha),
                Methods.HodgeLehmann(k,P,nu),
                Methods.Germeyer(k,P),
                Methods.BLMM(k,P,epsilon),
                Methods.Proizv(k)
                };
            showIt("Koefficients", k);
            showIt("Probabilities", P);
            string[] Fnames =
                {
                "ExpectedValue",
                "MaxiMax",
                "MaxiMin",
                //"MiniMax",
                "Laplace",
                "Savage",
                "Hurwitz",
                "HodgeLehmann",
                "Germeyer",
                "BLMM",
                "Proizv"
            };
            showCriteriaResults("Results", Fnames, Results);
            Console.In.ReadLine();
            return true;
        }
        public static bool work2(out NoExpResultData _Rn1, ExpResultData[] _Re1m)
        {
            Console.Out.WriteLine("Part2");

            Console.Out.WriteLine("Исходные данные");
            double[,] L =
                {
                    { -30.0 ,25.0 },
                    { 40.0, -45.0 }
                };
            showIt("Матрица потерь", L);
            double[] P = { 0.4, 0.6 };
            showIt("Априорные вероятности", P);

            Console.Out.WriteLine("Игра без эксперимента:");
            double[] ML = new double[L.GetLength(0)];
            for (int i = 0; i < L.GetLength(0); i++)
            {
                ML[i] = 0.0;
                for (int j = 0; j < L.GetLength(1); j++)
                    ML[i] += L[i, j] * P[j];
            }
            showIt("Ожидаемые потери", ML);
            int mlminind = 0;
            for (int i = 0; i < ML.Length; i++)
                if (ML[i] < ML[mlminind])
                    mlminind = i;
            showIt("Порядковый индекс минимальных ожидаемых потерь", mlminind);
            double mlmin = ML[mlminind];
            showIt("Минимальные ожидемые потери", mlmin);

            _Rn1 = new NoExpResultData(ML, mlminind, mlmin);

            Console.Out.WriteLine("Эксперимент 1");
            double[] da = { 0.2, 0.8 };
            showIt("Область продолжения", da);
            double[,] Pzas =
                {
                    { 0.6, 0.1 },
                    { 0.2, 0.2 },
                    { 0.2, 0.7 }
                };
            showIt("Матрица условных вероятностей", Pzas);
            double ca = 5;
            showIt("Стоимость эксперимента", ca);
            double[] Pza = new double[Pzas.GetLength(0)];
            for (int i = 0; i < Pzas.GetLength(0); i++)
            {
                Pza[i] = 0.0;
                for (int j = 0; j < Pzas.GetLength(1); j++)
                    Pza[i] += Pzas[i, j] * P[j];
            }
            showIt("Полные вероятности исходов эксперимента", Pza);
            double[,] Psza = new double[Pzas.GetLength(0), Pzas.GetLength(1)];
            for (int i = 0; i < Pzas.GetLength(0); i++)
                for (int j = 0; j < Pzas.GetLength(1); j++)
                    Psza[i, j] = (Pzas[i, j] * P[j]) / Pza[i];
            showIt("Апостериорные вероятности", Psza);
            int[,] tia = Ti(Psza, da);
            showWhereStop("При различных результатах эксперимента", tia);
            double[,] MLa = new double[L.GetLength(0), Psza.GetLength(0)];
            for (int i = 0; i < MLa.GetLength(0); i++)
                for (int j = 0; j < MLa.GetLength(1); j++)
                {
                    MLa[i, j] = 0.0;
                    for (int k = 0; k < MLa.GetLength(0); k++)
                        MLa[i, j] += L[i, k] * Psza[j, k];
                }
            showIt("Ожидаемые потери", MLa);
            int[] FRa = Fr(MLa);
            showIt("Решающая функция", FRa);
            double[] Mopa = new double[FRa.Length];
            for (int i = 0; i < Mopa.Length; i++)
                Mopa[i] = MLa[FRa[i], i];
            showIt("Минимальные ожидаемые потери", Mopa);
            double Oopa = 0.0;
            for (int i = 0; i < Mopa.Length; i++)
                Oopa += Mopa[i] * Pza[i];
            showIt("Общие ожидаемые потери", Oopa);
            double mexpa = mlmin - Oopa;
            showIt("Максимальная стоимость целесообразного эксперимента", mexpa);
            double expva = mexpa - ca;
            showIt("Выигрыш", expva);

            _Re1m[0] = new ExpResultData(Pza, Psza, tia, MLa, FRa, Mopa, Oopa, mexpa, expva);

            Console.Out.WriteLine("Эксперимент 2");
            for (int expr = 0; expr != Psza.GetLength(0); expr++)
            {
                Console.Out.WriteLine("Для " + expr + "-го исхода 1-го эксперимента.");
                double[] db = { 0.2, 0.8 };
                showIt("Область продолжения", db);
                double[,] Pzbs =
                {
                    { 0.8, 0.1 },
                    { 0.1, 0.3 },
                    { 0.1, 0.6 }
                };
                showIt("Матрица условных вероятностей", Pzbs);
                double cb = 4;
                showIt("Стоимость эксперимента", cb);
                double[] Pzb = new double[Pzbs.GetLength(0)];
                for (int i = 0; i < Pzbs.GetLength(0); i++)
                {
                    Pzb[i] = 0.0;
                    for (int j = 0; j < Pzbs.GetLength(1); j++)
                        Pzb[i] += Pzbs[i, j] * Psza[expr, j];
                }
                showIt("Полные вероятности исходов " + expr + "-го эксперимента", Pzb);
                double[,] Pszb = new double[Pzbs.GetLength(0), Pzbs.GetLength(1)];
                for (int i = 0; i < Pzbs.GetLength(0); i++)
                    for (int j = 0; j < Pzbs.GetLength(1); j++)
                        Pszb[i, j] = (Pzbs[i, j] * Psza[expr, j]) / Pzb[i];
                showIt("Апостериорные вероятности", Pszb);
                int[,] tib = Ti(Pszb, db);
                showWhereStop("При различных результатах эксперимента", tib);
                double[,] MLb = new double[L.GetLength(0), Pszb.GetLength(0)];
                for (int i = 0; i < MLb.GetLength(0); i++)
                    for (int j = 0; j < MLb.GetLength(1); j++)
                    {
                        MLb[i, j] = 0.0;
                        for (int k = 0; k < MLb.GetLength(0); k++)
                            MLb[i, j] += L[i, k] * Pszb[j, k];
                    }
                showIt("Ожидаемые потери", MLb);
                int[] FRb = Fr(MLb);
                showIt("Решающая функция", FRb);
                double[] Mopb = new double[FRb.Length];
                for (int i = 0; i < Mopb.Length; i++)
                    Mopb[i] = MLb[FRb[i], i];
                showIt("Минимальные ожидаемые потери", Mopb);
                double Oopb = 0.0;
                for (int i = 0; i < Mopb.Length; i++)
                    Oopb += Mopb[i] * Pzb[i];
                showIt("Общие ожидаемые потери", Oopb);
                double mexpb = Oopa - Oopb;
                showIt("Максимальная стоимость целесообразного эксперимента", mexpb);
                double expvb = mexpb - cb;
                showIt("Выигрыш", expvb);

                _Re1m[expr + 1] = new ExpResultData(Pzb, Pszb, tib, MLb, FRb, Mopb, Oopb, mexpb, expvb);
            }
            Console.In.ReadLine();
            return true;
        }
        public static bool work2_2(out NoExpResultData _Rn2, ExpResultData[] _Rem)
        {
            Console.Out.WriteLine("Part2_2");

            Console.Out.WriteLine("Исходные данные");
            double[,] L =
                {
                    { -30.0 ,25.0 },
                    { 40.0, -45.0 }
                };
            showIt("Матрица потерь", L);
            double[] P = { 0.4, 0.6 };
            showIt("Априорные вероятности", P);

            NoExpResultData Rn = null;
            Methods.NoExp(L, P, ref Rn);
            Rn.showIt("Игра без эксперимента");
            _Rn2 = Rn;

            Console.Out.WriteLine("Эксперимент 1");
            double[] da = { 0.2, 0.8 };
            showIt("Область продолжения", da);
            double[,] Pzas =
                {
                    { 0.6, 0.1 },
                    { 0.2, 0.2 },
                    { 0.2, 0.7 }
                };
            showIt("Матрица условных вероятностей", Pzas);
            double ca = 5;
            showIt("Стоимость эксперимента", ca);
            ExpResultData Re1 = null;
            Methods.Exp(L, P, Rn.mlmin, da, Pzas, ca, ref Re1);
            Re1.showIt("Второй эксперимент");
            _Rem[0] = Re1;

            double[] Pszam = new double[Re1.Psz.GetLength(1)];
            //ExpResultData[] Re1m = new ExpResultData[Pzas.GetLength(0)];
            //if (Re1.expv <= 0.0)
            if (Re1.expv <= 0.0)
                Console.Out.WriteLine("Эксперимент нецелесообразен.");
            else
                for (int i = 0; i < Re1.Psz.GetLength(0); i++)
                {
                    int tix = 1;
                    for (int j = 0; j < _Rem[0].ti.GetLength(1); j++)
                        tix *= _Rem[0].ti[i, j];
                    if (tix != 0)
                        _Rem[i + 1] = null;
                    else
                    {
                        double[] db = { 0.2, 0.8 };
                        showIt("Область продолжения", db);
                        double[,] Pzbs =
                        {
                        { 0.8, 0.1 },
                        { 0.1, 0.3 },
                        { 0.1, 0.6 }
                        };
                        showIt("Матрица условных вероятностей", Pzbs);
                        double cb = 4;
                        showIt("Стоимость эксперимента", cb);
                        _Rem[i + 1] = null;
                        for (int j = 0; j < Pszam.Length; j++)
                            Pszam[j] = Re1.Psz[i, j];
                        Methods.Exp(L, Pszam, Re1.Oop, db, Pzbs, cb, ref _Rem[i + 1]);
                        _Rem[i + 1].showIt("Второй эксперимент: №" + i.ToString());
                        //if (_Rem[i + 1].expv <= 0.0)
                    }
                }
            Console.In.ReadLine();
            return true;
        }
*/


    


////Числительные в различных падежах
//private static string[] Nums_Im_m = { "нулевой", "первый", "второй", "третий", "четвёртый", "пятый", "шестой", "седьмой", "восьмой", "девятый", "десятый" };
//private static string[] Nums_Rd_m = { "нулевого", "первого", "второго", "третьего", "четвёртого", "пятого", "шестого", "седьмого", "восьмого", "девятого", "десятого" };
//private static string[] Nums_Dt_m = { "нулевому", "первому", "второму", "третьему", "четвёртому", "пятому", "шестому", "седьмому", "восьмому", "девятому", "десятому" };
//private static string[] Nums_Vn_m = { "нулевого", "первого", "второго", "третьего", "четвёртого", "пятого", "шестого", "седьмого", "восьмого", "девятого", "десятого" };
//private static string[] Nums_Tv_m = { "нулевым", "первым", "вторым", "третьим", "четвёртым", "пятым", "шестым", "седьмым", "восьмым", "девятым", "десятым" };
//private static string[] Nums_Pr_m = { "нулевом", "первом", "втором", "третьем", "четвёртом", "пятом", "шестом", "седьмом", "восьмом", "девятом", "десятом" };

//private static string[] Nums_Im_j = { "нулевая", "первая", "вторая", "третья", "четвёртая", "пятая", "шестая", "седьмая", "восьмая", "девятая", "десятая" };
//private static string[] Nums_Rd_j = { "нулевой", "первой", "второй", "третьей", "четвёртой", "пятой", "шестой", "седьмой", "восьмой", "девятой", "десятой" };
//private static string[] Nums_Dt_j = { "нулевой", "первой", "второй", "третьей", "четвёртой", "пятой", "шестой", "седьмой", "восьмой", "девятой", "десятой" };
//private static string[] Nums_Vn_j = { "нулевую", "первую", "вторую", "третью", "четвёртую", "пятую", "шестую", "седьмую", "восьмую", "девятую", "десятую" };
//private static string[] Nums_Tv_j = { "нулевой", "первой", "второй", "третьей", "четвёртой", "пятой", "шестой", "седьмой", "восьмой", "девятой", "десятой" };
//private static string[] Nums_Pr_j = { "нулевой", "первой", "второй", "третьей", "четвёртой", "пятой", "шестой", "седьмой", "восьмой", "девятой", "десятой" };

//private static string[] Nums_Im_s = { null, "первое", "второе", "третье", "четвёртое", "пятое", "шестое", "седьмое", "восьмое", "девятое", "десятое" };
//private static string[] Nums_Rd_s = { null, "первого", "второго", "третьего", "четвёртого", "пятого", "шестого", "седьмого", "восьмого", "девятого", "десятого" };
//private static string[] Nums_Dt_s = { null, "первому", "второму", "третьему", "четвёртому", "пятому", "шестому", "седьмому", "восьмому", "девятому", "десятому" };
//private static string[] Nums_Vn_s = { null, "первое", "второе", "третье", "четвёртое", "пятое", "шестое", "седьмое", "восьмое", "девятое", "десятое" };
//private static string[] Nums_Tv_s = { null, "первым", "вторым", "третьим", "четвёртым", "пятым", "шестым", "седьмым", "восьмым", "девятым", "десятым" };
//private static string[] Nums_Pr_s = { null, "первом", "втором", "третьем", "четвёртом", "пятом", "шестом", "седьмом", "восьмом", "девятом", "десятом" };

//private static string[] Nums_Im_M = { null, "первые", "вторые", "третьи", "четвёртые", "пятые", "шестые", "седьмые", "восьмые", "девяте", "десятые" };
//private static string[] Nums_Rd_M = { null, "первых", "вторых", "третьих", "четвёртых", "пятых", "шестых", "седьмых", "восьмых", "девятых", "десятых" };
//private static string[] Nums_Dt_M = { null, "первым", "вторым", "третьим", "четвёртым", "пятым", "шестым", "седьмым", "восьмым", "девятым", "десятым" };
//private static string[] Nums_Vn_M = { null, "первых", "вторых", "третьих", "четвёртых", "пятых", "шестых", "седьмых", "восьмых", "девятых", "десятых" };
//private static string[] Nums_Tv_M = { null, "первыми", "вторыми", "третьими", "четвёртыми", "пятыми", "шестыми", "седьмыми", "восьмыми", "девятыми", "десятыми" };
//private static string[] Nums_Pr_M = { null, "первых", "вторых", "третьих", "четвёртых", "пятых", "шестых", "седьмых", "восьмых", "девятых", "десятых" };
//private static string getNum_Im_m(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Im_m.Length)
//        return Nums_Im_m[_num];
//    else
//        return _num.ToString() + "-й";
//}
//private static string getNum_Rd_m(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Rd_m.Length)
//        return Nums_Rd_m[_num];
//    else
//        return _num.ToString() + "-го";
//}
//private static string getNum_Dt_m(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Dt_m.Length)
//        return Nums_Dt_m[_num];
//    else
//        return _num.ToString() + "-му";
//}
//private static string getNum_Vn_m(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Vn_m.Length)
//        return Nums_Vn_m[_num];
//    else
//        return _num.ToString() + "-го";
//}
//private static string getNum_Tv_m(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Tv_m.Length)
//        return Nums_Tv_m[_num];
//    else
//        return _num.ToString() + "-м";
//}
//private static string getNum_Pr_m(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Pr_m.Length)
//        return Nums_Pr_m[_num];
//    else
//        return _num.ToString() + "-м";
//}

//private static string getNum_Im_j(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Im_j.Length)
//        return Nums_Im_j[_num];
//    else
//        return _num.ToString() + "-я";
//}
//private static string getNum_Rd_j(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Rd_j.Length)
//        return Nums_Rd_j[_num];
//    else
//        return _num.ToString() + "-й";
//}
//private static string getNum_Dt_j(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Dt_j.Length)
//        return Nums_Dt_j[_num];
//    else
//        return _num.ToString() + "-й";
//}
//private static string getNum_Vn_j(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Vn_j.Length)
//        return Nums_Vn_j[_num];
//    else
//        return _num.ToString() + "-ю";
//}
//private static string getNum_Tv_j(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Tv_j.Length)
//        return Nums_Tv_j[_num];
//    else
//        return _num.ToString() + "-й";
//}
//private static string getNum_Pr_j(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Pr_j.Length)
//        return Nums_Pr_j[_num];
//    else
//        return _num.ToString() + "-й";
//}

//private static string getNum_Im_s(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Im_s.Length)
//        return Nums_Im_s[_num];
//    else
//        return _num.ToString() + "-е";
//}
//private static string getNum_Rd_s(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Rd_m.Length)
//        return Nums_Rd_m[_num];
//    else
//        return _num.ToString() + "-го";
//}
//private static string getNum_Dt_s(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Dt_m.Length)
//        return Nums_Dt_m[_num];
//    else
//        return _num.ToString() + "-му";
//}
//private static string getNum_Vn_s(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Vn_m.Length)
//        return Nums_Vn_m[_num];
//    else
//        return _num.ToString() + "-е";
//}
//private static string getNum_Tv_s(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Tv_m.Length)
//        return Nums_Tv_m[_num];
//    else
//        return _num.ToString() + "-м";
//}
//private static string getNum_Pr_s(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Pr_m.Length)
//        return Nums_Pr_m[_num];
//    else
//        return _num.ToString() + "-м";
//}

//private static string getNum_Im_M(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Im_M.Length)
//        return Nums_Im_M[_num];
//    else
//        return _num.ToString() + "-е";
//}
//private static string getNum_Rd_M(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Rd_M.Length)
//        return Nums_Rd_M[_num];
//    else
//        return _num.ToString() + "-х";
//}
//private static string getNum_Dt_M(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Dt_M.Length)
//        return Nums_Dt_M[_num];
//    else
//        return _num.ToString() + "-м";
//}
//private static string getNum_Vn_M(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Vn_M.Length)
//        return Nums_Vn_M[_num];
//    else
//        return _num.ToString() + "-х";
//}
//private static string getNum_Tv_M(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Tv_M.Length)
//        return Nums_Tv_M[_num];
//    else
//        return _num.ToString() + "-ми";
//}
//private static string getNum_Pr_M(int _num)
//{
//    if (_num < 0)
//        return null;
//    if (_num < Nums_Pr_M.Length)
//        return Nums_Pr_M[_num];
//    else
//        return _num.ToString() + "-х";
//}

//public static string getNum(int _num, Pad _pad, RdM _rdm, bool _Zagl)
//{
//    string word = null;
//    switch (_pad)
//    {
//        case Pad.Im:
//            switch (_rdm)
//            {
//                case RdM.m: word = getNum_Im_m(_num); break;
//                case RdM.j: word = getNum_Im_j(_num); break;
//                case RdM.s: word = getNum_Im_s(_num); break;
//                case RdM.M: word = getNum_Im_M(_num); break;
//            }
//            break;
//        case Pad.Rd:
//            switch (_rdm)
//            {
//                case RdM.m: word = getNum_Rd_m(_num); break;
//                case RdM.j: word = getNum_Rd_j(_num); break;
//                case RdM.s: word = getNum_Rd_s(_num); break;
//                case RdM.M: word = getNum_Rd_M(_num); break;
//            }
//            break;
//        case Pad.Dt:
//            switch (_rdm)
//            {
//                case RdM.m: word = getNum_Dt_m(_num); break;
//                case RdM.j: word = getNum_Dt_j(_num); break;
//                case RdM.s: word = getNum_Dt_s(_num); break;
//                case RdM.M: word = getNum_Dt_M(_num); break;
//            }
//            break;
//        case Pad.Vn:
//            switch (_rdm)
//            {
//                case RdM.m: word = getNum_Vn_m(_num); break;
//                case RdM.j: word = getNum_Vn_j(_num); break;
//                case RdM.s: word = getNum_Vn_s(_num); break;
//                case RdM.M: word = getNum_Vn_M(_num); break;
//            }
//            break;
//        case Pad.Tv:
//            switch (_rdm)
//            {
//                case RdM.m: word = getNum_Tv_m(_num); break;
//                case RdM.j: word = getNum_Tv_j(_num); break;
//                case RdM.s: word = getNum_Tv_s(_num); break;
//                case RdM.M: word = getNum_Tv_M(_num); break;
//            }
//            break;
//        case Pad.Pr:
//            switch (_rdm)
//            {
//                case RdM.m: word = getNum_Pr_m(_num); break;
//                case RdM.j: word = getNum_Pr_j(_num); break;
//                case RdM.s: word = getNum_Pr_s(_num); break;
//                case RdM.M: word = getNum_Pr_M(_num); break;
//            }
//            break;
//    }
//    if (word != null)
//    {
//        StringBuilder sb = new StringBuilder(word);
//        if (_Zagl)
//            sb[0] = word.Substring(0, 1).ToUpper()[0];
//        word = sb.ToString();
//    }
//    return word;
//}

//double ki0j0me = _kw[i0, j0] - _epsilon;
//double maxki0jpe = _kw[i0, j0] + maxKm[i0];
//double k_p_e = Ki0j0 + _epsilon;
//b[i] = (((maxKm[i] + minKm[i]) <= maxki0jpe) && (minKm[i] >= ki0j0me));