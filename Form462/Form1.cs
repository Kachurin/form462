﻿using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.IO;
//using System.Threading.Tasks;
using System.Windows.Forms;

namespace Form462
{
    /// <summary>
    /// Класс формы
    /// </summary>
    public partial class Form1 : Form
    {
        /// <summary>
        /// Абстрактный класс сценария
        /// </summary>
        public abstract class XScript
        {
            /// <summary>
            /// Абстрактный класс панели настроек
            /// </summary>
            public abstract class SettingsPanel : Panel
            {
                /// <summary>
                /// Панель раздела "Решебник"
                /// </summary>
                public PanelC PnlC;
                /// <summary>
                /// Конструктор панели настроек
                /// </summary>
                /// <param name="_PnlC">Панель раздела "Решебник"</param>
                /// <param name="_fileStream">Файловый поток исходных данных</param>
                /// <param name="_Location">Положение</param>
                /// <param name="_Size">Размер</param>
                public SettingsPanel(PanelC _PnlC, Point _Location, Size _Size) : base()
                {
                    PnlC = _PnlC;
                    Location = _Location;
                    Size = _Size;
                    Visible = true;
                    AutoScroll = true;
                    PnlC.XSSP = this;
                    PnlC.ScriptSP.Controls.Add(this);
                }
                /// <summary>
                /// Создание объекта сценария на основе настроек
                /// </summary>
                /// <returns>Объект сценария</returns>
                public abstract XScript getScript();
                /// <summary>
                /// Сохранение в файл
                /// </summary>
                /// <param name="_fileName">Название файла</param>
                /// <returns>Успешность сохранения</returns>
                public bool saveToFile(string _fileName)
                {
                    FileStream FS = File.Create(_fileName);
                    BinaryWriter BW = new BinaryWriter(FS);
                    BW.Write(XScript.scrNum);
                    formMainFileBytes(BW);
                    BW.Close();
                    return true;
                }
                /// <summary>
                /// Формирование основного содержимого файла (не включает номер типа сценария)
                /// </summary>
                /// <returns>Байтовое представление данных для записи в файл</returns>
                public abstract bool formMainFileBytes(BinaryWriter _BW);
                /// <summary>
                /// Отсоединение панели
                /// </summary>
                /// <returns>Успешность операции</returns>
                public bool unlink()
                {
                    if (PnlC.XSSP != this)
                        return false;
                    PnlC.ScriptSP.Controls.Remove(this);
                    PnlC.XSSP = null;
                    return true;
                }
            }
            /// <summary>
            /// Панель раздела "Решебник"
            /// </summary>
            public PanelC PnlC;
            /// <summary>
            /// Основная панель сценария
            /// </summary>
            public Panel MainPanel;
            //public Control[] SettingsElements;
            //============================
            //without position
            //without visible
            //main panel
            //later elements
            //later data
            //====================
            /// <summary>
            /// Конструктор сценария
            /// </summary>
            /// <param name="_F">Окно программы</param>
            /// <param name="_SPanel">Панель настроек</param>
            /// <param name="_ParentPanel">Родительская панель</param>
            public XScript(PanelC _PnlC)
            {
                PnlC = _PnlC;
                MainPanel = NewPanel(new Point(0, 0), PnlC.Size, false, false, PnlC);
            }
            /// <summary>
            /// Подготовка и запуск сценария
            /// </summary>
            public virtual void beginScript()
            {
                PnlC.ScriptSP.Visible = false;
                MainPanel.Visible = true;
            }
            /// <summary>
            /// Завершение работы сценария
            /// </summary>
            public virtual void endScript()
            {
                MainPanel.Visible = false;
                PnlC.ScriptSP.Visible = true;
            }
            /// <summary>
            /// Условный номер сценария
            /// </summary>
            /// <returns>Условный номер сценария</returns>
            public static int scrNum
            {
                get;
            }
        }
        /// <summary>
        /// Класс сценария "Дерево решений"
        /// </summary>
        public class XScript_DecisionTree : XScript
        {
            /// <summary>
            /// Класс панели настроек сценария "Дерево решений"
            /// </summary>
            public class SettingsPanel_DecisionTree : SettingsPanel
            {
                /// <summary>
                /// Основная информация
                /// </summary>
                public InputInfo II;
                /// <summary>
                /// Текстовый блок с описанием условий задачи
                /// </summary>
                public TextBox DescriptionTB;
                /// <summary>
                /// Количество стратегий
                /// </summary>
                public Label dCountL;
                /// <summary>
                /// Количество состояний природы
                /// </summary>
                public Label sCountL;
                /// <summary>
                /// Количество экспериментов
                /// </summary>
                public Label expCountL;

                /// <summary>
                /// "Тип значений"
                /// </summary>
                public Label WinLossL;
                /// <summary>
                /// [Выигрыш/Потери]
                /// </summary>
                public ComboBox WinLossCB;

                /// <summary>
                /// %Использовать априорные вероятности
                /// </summary>
                public CheckBox UsingAP_CB;
                /// <summary>
                /// %Использовать области продолжения
                /// </summary>
                public CheckBox UsingCA_CB;

                /// <summary>
                /// "Принимаемые пользователем решения"
                /// </summary>
                public Label UserDecisionsL;
                /// <summary>
                /// %Выбор принципа оптимальности
                /// </summary>
                public CheckBox UD_CriteriaCB;
                /// <summary>
                /// %Обоснование принципа оптимальности
                /// </summary>
                public CheckBox UD_CriteriaRationaleCB;
                /// <summary>
                /// %Решение о целесообразности эксперимента
                /// </summary>
                public CheckBox UD_ExpediencyCB;
                /// <summary>
                /// %Выбор предпочтительной стартегии
                /// </summary>
                public CheckBox UD_DecisionCB;
                /// <summary>
                /// %Формирование ответа в терминах модели
                /// </summary>
                public CheckBox UD_AnswerModelCB;
                /// <summary>
                /// %Формирование ответа в терминах задачи
                /// </summary>
                public CheckBox UD_AnswerTaskCB;

                /// <summary>
                /// Действие при решении об использовании априорных вероятностей
                /// </summary>
                /// <param name="sender">Объект пункта</param>
                /// <param name="e">Аргументы события</param>
                private void UsingAP_CB_CheckedChanged(object sender, EventArgs e)
                {
                    //UD_CriteriaCB.Visible = UsingAP_CB.Checked;
                    //UD_CriteriaRationaleCB.Visible = UsingAP_CB.Checked;
                    UD_ExpediencyCB.Visible = UsingAP_CB.Checked;
                }
                /// <summary>
                /// Вывод количества стратегий
                /// </summary>
                /// <param name="_cnt">Количество стратегий</param>
                public void dCountShow(int _cnt)
                {
                    dCountL.Text = "Стратегии: " + _cnt;
                }
                /// <summary>
                /// Вывод количества состояний природы
                /// </summary>
                /// <param name="_cnt">Количество состояний природы</param>
                public void sCountShow(int _cnt)
                {
                    sCountL.Text = "Состояния природы: " + _cnt;
                }
                /// <summary>
                /// Вывод количества доступных экспериментов
                /// </summary>
                /// <param name="_cnt">Количество доступных экспериментов</param>
                public void expCountShow(int _cnt)
                {
                    expCountL.Text = "Эксперименты: " + _cnt;
                }
                /// <summary>
                /// Конструктор
                /// </summary>
                /// <param name="_PnlC">Панель раздела "Решебник"</param>
                /// <param name="_fileStream">Поток файла исходных данных сценария</param>
                /// <param name="_Location">Положение панели</param>
                /// <param name="_Size">Размер панели</param>
                public SettingsPanel_DecisionTree(PanelC _PnlC, FileStream _fileStream, Point _Location, Size _Size) : base(_PnlC, _Location, _Size)
                {
                    II = new InputInfo();
                    //===================
                    int xpos = 0, ypos = 0;
                    dCountL = NewLabel(null, xpos, ypos, true, this);
                    ypos++;
                    sCountL = NewLabel(null, xpos, ypos, true, this);
                    ypos++;
                    expCountL = NewLabel(null, xpos, ypos, true, this);
                    ypos++;
                    WinLossL = NewLabel("Тип значений:", xpos, ypos, true, this);
                    xpos++;
                    WinLossCB = NewComboBox(xpos, ypos, 2, new object[] { "Выигрыши", "Потери" }, 0, null, true, this);
                    xpos += 2; ypos -= 3;
                    DescriptionTB = NewTextBox(null, xpos, ypos, 4, 4, true, true, null, this);
                    DescriptionTB.Font = new Font(DescriptionTB.Font.FontFamily, DescriptionTB.Font.SizeInPoints + 2);
                    xpos = 0; ypos += 4;
                    UsingAP_CB = NewCheckBox("Использовать априорные вероятности", false, xpos, ypos, UsingAP_CB_CheckedChanged, true, this);
                    ypos++;
                    UsingCA_CB = NewCheckBox("Использовать области останова", false, xpos, ypos, null, true, this);
                    ypos++;
                    UserDecisionsL = NewLabel("Принимаемые пользователем решения:", xpos, ypos, true, this);
                    ypos++;
                    UD_CriteriaCB = NewCheckBox("Выбор принципа оптимальности", false, xpos, ypos, null, true, this);
                    ypos++;
                    UD_CriteriaRationaleCB = NewCheckBox("Обоснование принципа оптимальности", false, xpos, ypos, null, true, this);
                    ypos++;
                    UD_ExpediencyCB = NewCheckBox("Решение о целесообразности эксперимента", false, xpos, ypos, null, false, this);
                    xpos += 3; ypos -= 2;
                    UD_DecisionCB = NewCheckBox("Выбор предпочтительной стартегии", false, xpos, ypos, null, true, this);
                    ypos++;
                    UD_AnswerModelCB = NewCheckBox("Формирование ответа в терминах модели", false, xpos, ypos, null, true, this);
                    ypos++;
                    UD_AnswerTaskCB = NewCheckBox("Формирование ответа в терминах задачи (не активно)", false, xpos, ypos, null, true, this);

                    if (_fileStream == null)
                    {
                        // - Количество стратегий
                        int d_count = 2;
                        dCountShow(d_count);
                        // - Количество состояний природы
                        int s_count = 2;
                        sCountShow(s_count);
                        int ds_count = d_count * s_count;

                        II.NEI = new InputInfo.NoExpInputInfo();
                        II.NEI.L = new double[d_count, s_count];
                        for (int vd = 0; vd < d_count; vd++)
                            for (int vs = 0; vs < s_count; vs++)
                                II.NEI.L[vd, vs] = 0.0;
                        II.NEI.P = null;
                        // Вероятности (P) отсутстсвуют как таковые(можно составить опционально)
                        // - Количество экспериментов
                        int exp_count = 0;
                        II.EIM = new InputInfo.ExpInputInfo[exp_count];
                        expCountShow(exp_count);
                    }
                    else
                    {
                        using (BinaryReader FileBR = new BinaryReader(_fileStream))
                        {
                            DescriptionTB.Text = FileBR.ReadString();
                            WinLossCB.SelectedIndex = FileBR.ReadBoolean() ? 1 : 0;
                            UsingAP_CB.Checked = FileBR.ReadBoolean();
                            UsingCA_CB.Checked = FileBR.ReadBoolean();
                            UD_CriteriaCB.Checked = FileBR.ReadBoolean();
                            UD_CriteriaRationaleCB.Checked = FileBR.ReadBoolean();
                            UD_ExpediencyCB.Visible = UsingAP_CB.Checked;
                            if (UsingAP_CB.Checked)
                                UD_ExpediencyCB.Checked = FileBR.ReadBoolean();
                            else
                                UD_ExpediencyCB.Checked = false;
                            UD_DecisionCB.Checked = FileBR.ReadBoolean();
                            UD_AnswerModelCB.Checked = FileBR.ReadBoolean();
                            UD_AnswerTaskCB.Checked = FileBR.ReadBoolean();
                            // - Количество стратегий
                            int d_count = FileBR.ReadInt32();
                            dCountShow(d_count);
                            // - Количество состояний природы
                            int s_count = FileBR.ReadInt32();
                            sCountShow(s_count);
                            int ds_count = d_count * s_count;
                            II.NEI = new InputInfo.NoExpInputInfo();
                            II.NEI.L = new double[d_count, s_count];
                            for (int vd = 0; vd < d_count; vd++)
                                for (int vs = 0; vs < s_count; vs++)
                                    II.NEI.L[vd, vs] = FileBR.ReadDouble();
                            if (UsingAP_CB.Checked)
                            {
                                II.NEI.P = new double[s_count];
                                for (int vs = 0; vs < s_count; vs++)
                                    II.NEI.P[vs] = FileBR.ReadDouble();
                            }//Иначе вероятности отсутствуют как таковые (можно составить опционально)
                            if (UsingCA_CB.Checked)
                            {
                                II.NEI.b = new double[s_count];
                                for (int vs = 0; vs < s_count; vs++)
                                    II.NEI.b[vs] = FileBR.ReadDouble();
                            }//Иначе области останова отсутствуют как таковые (можно составить опционально)
                             // - Количество экспериментов
                            if (UsingAP_CB.Checked)
                            {
                                int exp_count = FileBR.ReadInt32();
                                expCountShow(exp_count);
                                II.EIM = new InputInfo.ExpInputInfo[exp_count];
                                for (int exp = 0; exp < exp_count; exp++)
                                {
                                    // - Количество исходов эксперимента
                                    int z_count = FileBR.ReadInt32();
                                    II.EIM[exp] = new InputInfo.ExpInputInfo();
                                    II.EIM[exp].Pzs = new double[z_count, s_count];
                                    for (int vz = 0; vz < z_count; vz++)
                                        for (int vs = 0; vs < s_count; vs++)
                                            II.EIM[exp].Pzs[vz, vs] = FileBR.ReadDouble();
                                    if (UsingCA_CB.Checked)
                                    {
                                        II.EIM[exp].b = new double[s_count];
                                        for (int vs = 0; vs < s_count; vs++)
                                            II.EIM[exp].b[vs] = FileBR.ReadDouble();
                                    }
                                    II.EIM[exp].c = FileBR.ReadDouble();
                                }
                            }
                        }
                    }
                }

                /// <summary>
                /// Формирование объекта сценария "Дерево решений" согласно настройкам
                /// </summary>
                /// <returns>Объект сценария "Дерево решений"</returns>
                public override XScript getScript()
                {
                    return new XScript_DecisionTree(PnlC);
                }
                /// <summary>
                /// Формирование основного содержимого файла (не включает номер типа сценария)
                /// </summary>
                /// <returns>Байтовое представление данных для записи в файл</returns>
                public override bool formMainFileBytes(BinaryWriter _BW)
                {
                    if (_BW == null)
                        return false;
                    _BW.Write(DescriptionTB.Text);
                    _BW.Write(WinLossCB.SelectedIndex == 1);
                    _BW.Write(UsingAP_CB.Checked);
                    _BW.Write(UsingCA_CB.Checked);
                    _BW.Write(UD_CriteriaCB.Checked);
                    _BW.Write(UD_CriteriaRationaleCB.Checked);
                    if (UsingAP_CB.Checked)
                        _BW.Write(UD_ExpediencyCB.Checked);
                    _BW.Write(UD_DecisionCB.Checked);
                    _BW.Write(UD_AnswerModelCB.Checked);
                    _BW.Write(UD_AnswerTaskCB.Checked);
                    int d_count = II.NEI.L.GetLength(0), s_count = II.NEI.L.GetLength(1);
                    _BW.Write(d_count);
                    _BW.Write(s_count);
                    double[,] Lt = II.NEI.L;
                    for (int vd = 0; vd < d_count; vd++)
                        for (int vs = 0; vs < s_count; vs++)
                            _BW.Write(Lt[vd, vs]);
                    if (UsingAP_CB.Checked)
                    {
                        double[] Pt = II.NEI.P;
                        for (int vs = 0; vs < s_count; vs++)
                            _BW.Write(Pt[vs]);
                    }
                    if (UsingCA_CB.Checked)
                    {
                        double[] bt = II.NEI.b;
                        for (int vs = 0; vs < s_count; vs++)
                            _BW.Write(bt[vs]);
                    }
                    if (UsingAP_CB.Checked)
                    {
                        int exp_count = II.EIM.Length;
                        _BW.Write(exp_count);
                        for (int exp = 0; exp < exp_count; exp++)
                        {
                            // - Количество исходов эксперимента
                            int z_count = II.EIM[exp].Pzs.GetLength(0);
                            _BW.Write(z_count);
                            double[,] Pzst = II.EIM[exp].Pzs;
                            for (int vz = 0; vz < z_count; vz++)
                                for (int vs = 0; vs < s_count; vs++)
                                    _BW.Write(Pzst[vz, vs]);
                            if (UsingCA_CB.Checked)
                            {
                                double[] bt = II.EIM[exp].b;
                                for (int vs = 0; vs < s_count; vs++)
                                    _BW.Write(bt[vs]);
                            }
                            _BW.Write(II.EIM[exp].c);
                        }
                    }

                    return true;
                }
            }
            /// <summary>
            /// Условный номер сценария
            /// </summary>
            /// <returns>Условный номер сценария</returns>
            public static new int scrNum
            {
                get { return 0; }
            }
            /// <summary>
            /// Класс панели ввода данных
            /// </summary>
            public class DataInPanel : Panel
            {
                /// <summary>
                /// Вкладка для игры без эксперимента
                /// </summary>
                public class NoExpTabPage : TabPage
                {
                    /// <summary>
                    /// Панель ввода данных
                    /// </summary>
                    DataInPanel DIP;
                    //----
                    /// <summary>
                    /// Обозначение матрицы выигрышей/потерь
                    /// </summary>
                    public Label LLabel;
                    /// <summary>
                    /// Кнопка формирования случайных значений выигрышей/потерь
                    /// </summary>
                    public Button LRandomButton;
                    /// <summary>
                    /// Таблица выигрышей/потерь
                    /// </summary>
                    public DataGridView LDGV;
                    /// <summary>
                    /// Обозначение распределения априорных вероятностей
                    /// </summary>
                    public Label PLabel;
                    /// <summary>
                    /// Кнопка формирования случайных значений априорных вероятностей
                    /// </summary>
                    public Button PRandomButton;
                    /// <summary>
                    /// Априорные вероятности
                    /// </summary>
                    public DataGridView PDGV;
                    /// <summary>
                    /// Обозначение области останова
                    /// </summary>
                    public Label bLabel;
                    /// <summary>
                    /// Область останова
                    /// </summary>
                    public DataGridView bDGV;
                    //----
                    /// <summary>
                    /// Наибольший номер ширины таблицы выигрышей/потерь
                    /// </summary>
                    public const int LDGVmaxBWidth = 8;
                    /// <summary>
                    /// Наибольший номер высоты таблицы выигрышей/потерь
                    /// </summary>
                    public const int LDGVmaxBHeight = 6;
                    /// <summary>
                    /// Наибольший номер ширины таблицы распределения априорных вероятностей
                    /// </summary>
                    public const int PDGVmaxBWidth = 8;
                    /// <summary>
                    /// Наибольший номер высоты таблицы распределения априорных вероятностей
                    /// </summary>
                    public const int PDGVmaxBHeight = 2;
                    /// <summary>
                    /// Наибольший номер ширины таблицы области останова
                    /// </summary>
                    public const int bDGVmaxBWidth = 8;
                    /// <summary>
                    /// Наибольший номер высоты таблицы области останова
                    /// </summary>
                    public const int bDGVmaxBHeight = 2;
                    //----
                    /// <summary>
                    /// Формирование размера таблицы выигрышей/потерь
                    /// </summary>
                    /// <returns>Успешность формрования размера</returns>
                    public bool formClientSizeOfLDGV()
                    {
                        return Form1.formClientSizeOfDGV(LDGV, NWidth(LDGVmaxBWidth), NWidth(LDGVmaxBHeight));
                    }
                    /// <summary>
                    /// Формирование размера таблицы распределения априорных вероятностей
                    /// </summary>
                    /// <returns>Успешность формрования размера</returns>
                    public bool formClientSizeOfPDGV()
                    {
                        return Form1.formClientSizeOfDGV(PDGV, NWidth(PDGVmaxBWidth), NWidth(PDGVmaxBHeight));
                    }
                    /// <summary>
                    /// Формирование размера таблицы области останова
                    /// </summary>
                    /// <returns>Успешность формрования размера</returns>
                    public bool formClientSizeOfbDGV()
                    {
                        return Form1.formClientSizeOfDGV(bDGV, NWidth(bDGVmaxBWidth), NWidth(bDGVmaxBHeight));
                    }
                    /// <summary>
                    /// Конструктор вкладки
                    /// </summary>
                    /// <param name="_Text">Текст</param>
                    /// <param name="_DIP">Панель ввода данных</param>
                    public NoExpTabPage(string _Text, DataInPanel _DIP) : base()
                    {
                        Padding = new Padding(3);
                        UseVisualStyleBackColor = true;
                        AutoScroll = true;
                        Text = _Text;
                        DIP = _DIP;
                        //----
                        InputInfo IIx = DIP.Scr.II;
                        Form1 F1 = DIP.Scr.PnlC.F;
                        int xpos = 0, ypos = 0;
                        LLabel = NewLabel(DIP.Scr.loss ? "Матрица потерь:" : "Матрица выигрышей:", xpos, ypos, true, this);
                        xpos += 2;
                        LRandomButton = NewButton("Случайно", xpos, ypos, 1, 1, LRandomButton_Click, true, this);
                        F1.ControlText(LRandomButton, "Генерация случайных значений матрицы потерь.");
                        xpos = 0; ypos++;
                        LDGV = NewDataGridView(IIx.NEI.L.GetLength(0), IIx.NEI.L.GetLength(1), xpos, ypos, 4, 6, LDGV_CellValueChanged, true, this);
                        for (int vd = 0; vd < IIx.NEI.L.GetLength(0); vd++)
                            for (int vs = 0; vs < IIx.NEI.L.GetLength(1); vs++)
                            {
                                LDGV[vs, vd].ValueType = typeof(double);
                                LDGV[vs, vd].Value = DIP.Scr.loss ? -IIx.NEI.L[vd, vs] : IIx.NEI.L[vd, vs];
                            }
                        formClientSizeOfLDGV();
                        ypos += LDGVmaxBHeight;
                        if (DIP.Scr.UsingAP)
                        {
                            PLabel = NewLabel("Априорные вероятности:", xpos, ypos, true, this);
                            xpos += 2;
                            PRandomButton = NewButton("Случайно", xpos, ypos, 1, 1, PRandomButton_Click, true, this);
                            F1.ControlText(PRandomButton, "Генерация случайных значений матрицы априорных вероятностей.");
                            xpos = 0; ypos++;
                            PDGV = NewDataGridView(1, IIx.NEI.P.Length, xpos, ypos, 4, 2, PDGV_CellValueChanged, true, this);
                            for (int vs = 0; vs < IIx.NEI.P.Length; vs++)
                            {
                                PDGV[vs, 0].ValueType = typeof(double);
                                PDGV[vs, 0].Value = IIx.NEI.P[vs];
                            }
                            formClientSizeOfPDGV();
                            ypos += PDGVmaxBHeight;
                        }
                        else
                        {
                            PLabel = null;
                            PRandomButton = null;
                            PDGV = null;
                        }
                        if (DIP.Scr.UsingB)
                        {
                            bLabel = NewLabel("Области останова:", xpos, ypos, true, this);
                            ypos++;
                            bDGV = NewDataGridView(1, IIx.NEI.b.Length, xpos, ypos, 4, 2, bDGV_CellValueChanged, true, this);
                            for (int vs = 0; vs < IIx.NEI.b.Length; vs++)
                            {
                                bDGV[vs, 0].ValueType = typeof(double);
                                bDGV[vs, 0].Value = IIx.NEI.b[vs];
                            }
                            formClientSizeOfbDGV();
                            ypos += bDGVmaxBHeight;
                        }
                        else
                        {
                            bLabel = null;
                            bDGV = null;
                        }
                    }
                    /// <summary>
                    /// Действие для кнопки задания случайных значений матрицы выигрышей/потерь
                    /// </summary>
                    /// <param name="sender">Объект кнопки</param>
                    /// <param name="e">Аргументы события</param>
                    private void LRandomButton_Click(object sender, EventArgs e)
                    {
                        if (DIP.Scr.isChanging)
                            return;
                        DIP.Scr.isChanging = true;

                        Random rnd = new Random();
                        InputInfo.NoExpInputInfo neIIx = DIP.Scr.II.NEI;
                        int gd = neIIx.L.GetLength(0), gs = neIIx.L.GetLength(1);
                        for (int vd = 0; vd < gd; vd++)
                            for (int vs = 0; vs < gs; vs++)
                            {
                                neIIx.L[vd, vs] = (double)rnd.Next(-100, 101);
                                LDGV[vs, vd].Value = (DIP.Scr.loss) ? -neIIx.L[vd, vs] : neIIx.L[vd, vs];
                            }

                        DIP.Scr.isChanging = false;
                    }
                    /// <summary>
                    /// Действие для кнопки задания случайных значений распределения априорных вероятностей
                    /// </summary>
                    /// <param name="sender">Объект кнопки</param>
                    /// <param name="e">Аргументы события</param>
                    private void PRandomButton_Click(object sender, EventArgs e)
                    {
                        if (DIP.Scr.isChanging)
                            return;
                        DIP.Scr.isChanging = true;

                        InputInfo.NoExpInputInfo neIIx = DIP.Scr.II.NEI;
                        Random rnd = new Random();
                        int gs = neIIx.P.Length;
                        int[] M = new int[gs];
                        for (int vs = 0; vs < gs; vs++)
                            M[vs] = 0;
                        for (int i = 0; i < 100; i++)
                            M[rnd.Next(0, gs)] += 1;
                        for (int vs = 0; vs < gs; vs++)
                            neIIx.P[vs] = ((double)M[vs]) / 100;
                        for (int vs = 0; vs < gs; vs++)
                            PDGV[vs, 0].Value = neIIx.P[vs];

                        DIP.Scr.isChanging = false;
                    }
                    /// <summary>
                    /// Действие при изменении значения ячейки матрицы выигрышей/потерь
                    /// </summary>
                    /// <param name="sender">Объект таблицы</param>
                    /// <param name="e">Аргументы события</param>
                    private void LDGV_CellValueChanged(object sender, DataGridViewCellEventArgs e)
                    {
                        if (DIP.Scr.isChanging)
                            return;
                        DIP.Scr.isChanging = true;

                        Type tp = LDGV[e.ColumnIndex, e.RowIndex].ValueType;
                        double newV = (double)LDGV[e.ColumnIndex, e.RowIndex].Value;
                        DIP.Scr.II.NEI.L[e.RowIndex, e.ColumnIndex] = (DIP.Scr.loss) ? -newV : newV;

                        DIP.Scr.isChanging = false;
                    }
                    /// <summary>
                    /// Действие при изменении значения ячейки распределения априорных вероятностей
                    /// </summary>
                    /// <param name="sender">Объект таблицы</param>
                    /// <param name="e">Аргументы события</param>
                    private void PDGV_CellValueChanged(object sender, DataGridViewCellEventArgs e)
                    {
                        if (DIP.Scr.isChanging)
                            return;
                        DIP.Scr.isChanging = true;

                        double newV = (double)PDGV[e.ColumnIndex, e.RowIndex].Value;
                        if ((newV < 0.0) || (newV > 1.0))
                            PDGV[e.ColumnIndex, e.RowIndex].Value = DIP.Scr.II.NEI.P[e.ColumnIndex];
                        else
                        {
                            if (newV == 1.0)
                            {
                                int gs = DIP.Scr.II.NEI.P.Length;
                                for (int vs = 0; vs < gs; vs++)
                                    DIP.Scr.II.NEI.P[vs] = 0.0;
                                DIP.Scr.II.NEI.P[e.ColumnIndex] = 1.0;
                                for (int vs = 0; vs < gs; vs++)
                                    PDGV[vs, e.RowIndex].Value = DIP.Scr.II.NEI.P[vs];
                            }
                            else
                            {
                                double oldV = DIP.Scr.II.NEI.P[e.ColumnIndex];
                                DIP.Scr.II.NEI.P[e.ColumnIndex] = newV;
                                if (newV != oldV)
                                {
                                    double dV = newV - oldV;
                                    int curInd = e.ColumnIndex;
                                    if (dV < 0.0)
                                    {
                                        int nSt = (curInd + 1) % DIP.Scr.II.NEI.P.Length;
                                        DIP.Scr.II.NEI.P[nSt] -= dV;
                                        PDGV[nSt, e.RowIndex].Value = DIP.Scr.II.NEI.P[nSt];
                                    }
                                    else
                                        while (dV != 0.0)
                                        {
                                            curInd = ((curInd + 1) % DIP.Scr.II.NEI.P.Length);
                                            if (DIP.Scr.II.NEI.P[curInd] < dV)
                                            {
                                                dV -= DIP.Scr.II.NEI.P[curInd];
                                                DIP.Scr.II.NEI.P[curInd] = 0.0;
                                                PDGV[curInd, e.RowIndex].Value = 0.0;
                                            }
                                            else
                                            {
                                                DIP.Scr.II.NEI.P[curInd] -= dV;
                                                PDGV[curInd, e.RowIndex].Value = DIP.Scr.II.NEI.P[curInd];
                                                dV = 0.0;
                                            }
                                        }
                                }
                            }
                        }



                        DIP.Scr.isChanging = false;
                    }
                    /// <summary>
                    /// Действие при изменении значения ячейки области останова
                    /// </summary>
                    /// <param name="sender">Объект таблицы</param>
                    /// <param name="e">Аргументы события</param>
                    private void bDGV_CellValueChanged(object sender, DataGridViewCellEventArgs e)
                    {
                        if (DIP.Scr.isChanging)
                            return;
                        DIP.Scr.isChanging = true;

                        double newV = (double)bDGV[e.ColumnIndex, e.RowIndex].Value;
                        if ((newV >= 0.0) && (newV <= 1.0))
                            DIP.Scr.II.NEI.b[e.ColumnIndex] = newV;
                        else
                            bDGV[e.ColumnIndex, e.RowIndex].Value = DIP.Scr.II.NEI.b[e.ColumnIndex];

                        DIP.Scr.isChanging = false;
                    }
                }
                /// <summary>
                /// Вкладка для эксперимента
                /// </summary>
                public class ExpTabPage : TabPage
                {
                    /// <summary>
                    /// Панель ввода данных
                    /// </summary>
                    DataInPanel DIP;
                    /// <summary>
                    /// Номер эксперимента (с 0-го)
                    /// </summary>
                    int expNum;
                    //----
                    /// <summary>
                    /// Обозначение количества исходов экспреимента
                    /// </summary>
                    public Label zCountLabel;
                    /// <summary>
                    /// Выбор количества исходов эксперимента
                    /// </summary>
                    public ComboBox zCountComboBox;
                    /// <summary>
                    /// Обозначение матрицы условных вероятностей
                    /// </summary>
                    public Label PzsLabel;
                    /// <summary>
                    /// Кнопка формирования случайных значений условных вероятностей
                    /// </summary>
                    public Button PzsRandomButton;
                    /// <summary>
                    /// Матрица условных вероятностей
                    /// </summary>
                    public DataGridView PzsDGV;
                    /// <summary>
                    /// Обозначение области останова
                    /// </summary>
                    public Label bLabel;
                    /// <summary>
                    /// Область останова
                    /// </summary>
                    public DataGridView bDGV;
                    /// <summary>
                    /// Обозначение стоимости эксперимента
                    /// </summary>
                    public Label cLabel;
                    /// <summary>
                    /// Стоимость эксперимента
                    /// </summary>
                    public TextBox cTextBox;
                    //----
                    /// <summary>
                    /// Наибольший номер ширины таблицы условных вероятностей
                    /// </summary>
                    public const int PzsDGVmaxBWidth = 8;
                    /// <summary>
                    /// Наибольший номер высоты таблицы условных вероятностей
                    /// </summary>
                    public const int PzsDGVmaxBHeight = 6;
                    /// <summary>
                    /// Наибольший номер ширины таблицы области останова
                    /// </summary>
                    public const int bDGVmaxBWidth = 8;
                    /// <summary>
                    /// Наибольший номер высоты таблицы области останова
                    /// </summary>
                    public const int bDGVmaxBHeight = 2;
                    //----
                    /// <summary>
                    /// Формирование размера таблицы условных вероятностей
                    /// </summary>
                    /// <returns>Успешность формрования размера</returns>
                    public bool formClientSizeOfPzsDGV()
                    {
                        return Form1.formClientSizeOfDGV(PzsDGV, NWidth(PzsDGVmaxBWidth), NWidth(PzsDGVmaxBHeight));
                    }
                    /// <summary>
                    /// Формирование размера таблицы условных вероятностей
                    /// </summary>
                    /// <returns>Успешность формрования размера</returns>
                    public bool formClientSizeOfbDGV()
                    {
                        return Form1.formClientSizeOfDGV(bDGV, NWidth(bDGVmaxBWidth), NWidth(bDGVmaxBHeight));
                    }
                    /// <summary>
                    /// Конструктор вкладки
                    /// </summary>
                    /// <param name="_Text">Текст</param>
                    /// <param name="_DIP">Панель ввода данных</param>
                    /// <param name="_expNum">Номер эксперимента (с 0-го)</param>
                    public ExpTabPage(string _Text, DataInPanel _DIP, int _expNum) : base()
                    {
                        Padding = new Padding(3);
                        UseVisualStyleBackColor = true;
                        AutoScroll = true;
                        Text = _Text;
                        DIP = _DIP;
                        expNum = _expNum;
                        //----
                        InputInfo.ExpInputInfo EIIx = DIP.Scr.II.EIM[expNum];
                        Form1 F1 = DIP.Scr.PnlC.F;
                        int xpos = 0, ypos = 0;
                        zCountLabel = NewLabel("Количество исходов:", xpos, ypos, true, this);
                        xpos += 2;
                        zCountComboBox = NewComboBox(xpos, ypos, 1, new object[] { 2, 3, 4 }, DIP.Scr.II.EIM[expNum].Pzs.GetLength(0) - 2, zCountComboBox_SelectedIndexChanged, true, this);
                        xpos = 0; ypos++;
                        PzsLabel = NewLabel("Матрица условных вероятностей:", xpos, ypos, true, this);
                        xpos += 2;
                        PzsRandomButton = NewButton("Случайно", xpos, ypos, 1, 1, PzsRandomButton_Click, true, this);
                        F1.ControlText(PzsRandomButton, "Генерация случайных значений матрицы условных вероятностей.");
                        xpos = 0; ypos++;
                        PzsDGV = NewDataGridView(DIP.Scr.II.EIM[expNum].Pzs.GetLength(0), DIP.Scr.II.EIM[expNum].Pzs.GetLength(1), xpos, ypos, 4, 4, PzsDGV_CellValueChanged, true, this);
                        for (int vz = 0; vz < PzsDGV.RowCount; vz++)
                            for (int vs = 0; vs < PzsDGV.ColumnCount; vs++)
                            {
                                PzsDGV[vs, vz].ValueType = typeof(double);
                                PzsDGV[vs, vz].Value = EIIx.Pzs[vz, vs];
                            }
                        formClientSizeOfPzsDGV();
                        ypos += PzsDGVmaxBHeight;
                        bLabel = NewLabel("Области останова:", xpos, ypos, true, this);
                        ypos++;
                        bDGV = NewDataGridView(1, DIP.Scr.II.NEI.b.Length, xpos, ypos, 4, 2, bDGV_CellValueChanged, true, this);
                        for (int vs = 0; vs < bDGV.ColumnCount; vs++)
                        {
                            bDGV[vs, 0].ValueType = typeof(double);
                            bDGV[vs, 0].Value = EIIx.b[vs];
                        }
                        formClientSizeOfbDGV();
                        ypos += bDGVmaxBHeight;
                        cLabel = NewLabel("Стоимость эксперимента:", xpos, ypos, true, this);
                        xpos += 2;
                        cTextBox = NewTextBox(EIIx.c.ToString(), xpos, ypos, 1, 1, true, false, cTextBox_TextChanged, this);
                    }
                    /// <summary>
                    /// Действие при изменении количества исходов эксперимента
                    /// </summary>
                    /// <param name="sender">Объект выпадающего списка</param>
                    /// <param name="e">Аргументы события</param>
                    public void zCountComboBox_SelectedIndexChanged(object sender, System.EventArgs e)
                    {
                        if (DIP.Scr.isChanging)
                            return;
                        DIP.Scr.isChanging = true;

                        int newZCnt = (int)zCountComboBox.SelectedItem;
                        InputInfo.ExpInputInfo eIIx = DIP.Scr.II.EIM[expNum];
                        int gz = eIIx.Pzs.GetLength(0), gs = eIIx.Pzs.GetLength(1);
                        int oldZCnt = eIIx.Pzs.GetLength(0);
                        double[,] Pzs2 = new double[newZCnt, eIIx.Pzs.GetLength(1)];
                        PzsDGV.RowCount = newZCnt;
                        if (oldZCnt < newZCnt)
                        {
                            for (int vz = 0; vz < gz; vz++)
                                for (int vs = 0; vs < gs; vs++)
                                    Pzs2[vz, vs] = eIIx.Pzs[vz, vs];
                            for (int vz = gz; vz < newZCnt; vz++)
                                for (int vs = 0; vs < gs; vs++)
                                    Pzs2[vz, vs] = 0.0;
                            eIIx.Pzs = Pzs2;
                            for (int vz = 0; vz < newZCnt; vz++)
                                for (int vs = 0; vs < gs; vs++)
                                {
                                    PzsDGV[vs, vz].ValueType = typeof(double);
                                    PzsDGV[vs, vz].Value = eIIx.Pzs[vz, vs];
                                }
                        }
                        else
                        {
                            for (int vs = 0; vs < gs; vs++)
                            {
                                double vSum = 0.0;
                                for (int vz = newZCnt; vz < gz; vz++)
                                    vSum += eIIx.Pzs[vz, vs];
                                vSum /= newZCnt;
                                for (int vz = 0; vz < newZCnt; vz++)
                                    Pzs2[vz, vs] = eIIx.Pzs[vz, vs] + vSum;
                            }
                            eIIx.Pzs = Pzs2;
                            PzsDGV.RowCount = newZCnt;
                            for (int vz = 0; vz < newZCnt; vz++)
                                for (int vs = 0; vs < gs; vs++)
                                    PzsDGV[vs, vz].Value = eIIx.Pzs[vz, vs];
                        }
                        formClientSizeOfPzsDGV();

                        DIP.Scr.isChanging = false;
                    }
                    /// <summary>
                    /// Действие для кнопки задания случайных значений условных веротятностей
                    /// </summary>
                    /// <param name="sender">Объект кнопки</param>
                    /// <param name="e">Аргументы события</param>
                    private void PzsRandomButton_Click(object sender, EventArgs e)
                    {
                        if (DIP.Scr.isChanging)
                            return;
                        DIP.Scr.isChanging = true;
                        InputInfo.ExpInputInfo eIIx = DIP.Scr.II.EIM[expNum];
                        Random rnd = new Random();
                        int gz = eIIx.Pzs.GetLength(0), gs = eIIx.Pzs.GetLength(1);
                        int[,] M = new int[gz, gs];

                        for (int vz = 0; vz < gz; vz++)
                            for (int vs = 0; vs < gs; vs++)
                                M[vz, vs] = 0;
                        for (int vs = 0; vs < gs; vs++)
                            for (int i = 0; i < 100; i++)
                                M[rnd.Next(0, gz), vs] += 1;
                        for (int vz = 0; vz < gz; vz++)
                            for (int vs = 0; vs < gs; vs++)
                                eIIx.Pzs[vz, vs] = ((double)M[vz, vs]) / 100;
                        for (int vz = 0; vz < gz; vz++)
                            for (int vs = 0; vs < gs; vs++)
                                PzsDGV[vs, vz].Value = eIIx.Pzs[vz, vs];

                        //for (int vz = 0; vz < gz; vz++)
                        //    for (int vs = 0; vs < gs; vs++)
                        //    {
                        //        DIP.Scr.II.NEI.L[vz, vs] = (double)rnd.Next(-100, 101);
                        //        PzsDGV[vs, vz].Value = eIIx.Pzs[vz, vs];
                        //    }

                        DIP.Scr.isChanging = false;
                    }
                    /// <summary>
                    /// Действие при изменении значения ячейки условных вероятностей
                    /// </summary>
                    /// <param name="sender">Объект таблицы</param>
                    /// <param name="e">Аргументы события</param>
                    private void PzsDGV_CellValueChanged(object sender, DataGridViewCellEventArgs e)
                    {
                        if (DIP.Scr.isChanging)
                            return;
                        DIP.Scr.isChanging = true;

                        InputInfo.ExpInputInfo eIIx = DIP.Scr.II.EIM[expNum];
                        double newV = (double)PzsDGV[e.ColumnIndex, e.RowIndex].Value;
                        int gz = eIIx.Pzs.GetLength(0), gs = eIIx.Pzs.GetLength(1);

                        if ((newV < 0.0) || (newV >= 1.0))
                            PzsDGV[e.ColumnIndex, e.RowIndex].Value = eIIx.Pzs[e.RowIndex, e.ColumnIndex];
                        else
                        if (newV == 1.0)
                        {
                            for (int vz = 0; vz < gz; vz++)
                                eIIx.Pzs[vz, gs] = 0.0;
                            eIIx.Pzs[e.RowIndex, gs] = 1.0;
                            for (int vz = 0; vz < gz; vz++)
                                PzsDGV[e.ColumnIndex, vz].Value = eIIx.Pzs[vz, e.ColumnIndex];
                        }
                        else
                        {
                            double oldV = eIIx.Pzs[e.RowIndex, e.ColumnIndex];
                            eIIx.Pzs[e.RowIndex, e.ColumnIndex] = newV;
                            if (newV != oldV)
                            {
                                double dV = newV - oldV;
                                int curInd = e.RowIndex;
                                if (dV < 0.0)
                                {
                                    int nSt = (curInd + 1) % eIIx.Pzs.GetLength(0);
                                    eIIx.Pzs[nSt, e.ColumnIndex] -= dV;
                                    PzsDGV[e.ColumnIndex, nSt].Value = eIIx.Pzs[nSt, e.ColumnIndex];
                                }
                                else
                                    while (dV != 0.0)
                                    {
                                        curInd = (curInd + 1) % eIIx.Pzs.GetLength(0);
                                        if (eIIx.Pzs[curInd, e.ColumnIndex] < dV)
                                        {
                                            dV -= eIIx.Pzs[curInd, e.ColumnIndex];
                                            eIIx.Pzs[curInd, e.ColumnIndex] = 0.0;
                                            PzsDGV[e.ColumnIndex, curInd].Value = 0.0;
                                        }
                                        else
                                        {
                                            eIIx.Pzs[curInd, e.ColumnIndex] -= dV;
                                            PzsDGV[e.ColumnIndex, curInd].Value = eIIx.Pzs[curInd, e.ColumnIndex];
                                            dV = 0.0;
                                        }
                                    }
                            }
                        }

                        DIP.Scr.isChanging = false;
                    }
                    /// <summary>
                    /// Действие при изменении значения ячейки области останова
                    /// </summary>
                    /// <param name="sender">Объект таблицы</param>
                    /// <param name="e">Аргументы события</param>
                    private void bDGV_CellValueChanged(object sender, DataGridViewCellEventArgs e)
                    {
                        if (DIP.Scr.isChanging)
                            return;
                        DIP.Scr.isChanging = true;

                        InputInfo.ExpInputInfo eIIx = DIP.Scr.II.EIM[expNum];
                        double newV = (double)bDGV[e.ColumnIndex, e.RowIndex].Value;
                        if ((newV >= 0.0) && (newV <= 1.0))
                            eIIx.b[e.ColumnIndex] = newV;
                        else
                            bDGV[e.ColumnIndex, e.RowIndex].Value = eIIx.b[e.ColumnIndex];

                        DIP.Scr.isChanging = false;
                    }
                    /// <summary>
                    /// Действие при изменении текста с указанием стоимости эксперимента
                    /// </summary>
                    /// <param name="sender">Объект текстовой области</param>
                    /// <param name="e">Аргументы события</param>
                    private void cTextBox_TextChanged(object sender, EventArgs e)
                    {
                        if (DIP.Scr.isChanging)
                            return;
                        DIP.Scr.isChanging = true;
                        InputInfo.ExpInputInfo eIIx = DIP.Scr.II.EIM[expNum];
                        double newV = -1.0;
                        if (!Double.TryParse(cTextBox.Text, out newV))
                            cTextBox.Text = eIIx.c.ToString();
                        else
                            eIIx.c = newV;
                        DIP.Scr.isChanging = false;
                    }
                }
                //----
                /// <summary>
                /// Объект сценария
                /// </summary>
                public XScript_DecisionTree Scr;

                /// <summary>
                /// Обозначение шага 2
                /// </summary>
                public Label Step2Label;
                /// <summary>
                /// Обозначение количества стратегий ЛПР
                /// </summary>
                public Label dCountLabel;
                /// <summary>
                /// Количество стратегий ЛПР
                /// </summary>
                public ComboBox dCountComboBox;
                /// <summary>
                /// Обозначение количества состояний природы
                /// </summary>
                public Label sCountLabel;
                /// <summary>
                /// Количество состояний природы
                /// </summary>
                public ComboBox sCountComboBox;
                /// <summary>
                /// Обозначение количества экспериментов
                /// </summary>
                public Label expCountLabel;
                /// <summary>
                /// Количество экспериментов
                /// </summary>
                public ComboBox expCountComboBox;
                /// <summary>
                /// Текстовая область описания
                /// </summary>
                public TextBox DescriptionTB;
                /// <summary>
                /// Кнопка выхода из сценария
                /// </summary>
                public Button outButton;
                /// <summary>
                /// Кнопка начала работы сценария
                /// </summary>
                public Button beginWorkButton;
                //---
                /// <summary>
                /// Рабочая область
                /// </summary>
                public TabControl dataTabControl;
                /// <summary>
                /// Вкладка формирования данных для игры без эксперимента
                /// </summary>
                public NoExpTabPage NoExpTP;
                /// <summary>
                /// Множество вкладок формирования данных для экспериментов
                /// </summary>
                public ExpTabPage[] ExpTPM;
                /// <summary>
                /// Конструктор
                /// </summary>
                /// <param name="_Scr">Объект сценария</param>
                public DataInPanel(XScript_DecisionTree _Scr)
                {
                    Scr = _Scr;
                    Location = new Point(0, 0);
                    Size = Scr.MainPanel.Size;
                    AutoScroll = true;
                    Form1 F1 = Scr.PnlC.F;
                    int xpos = 0, ypos = 0;
                    Step2Label = NewLabel("Шаг 2. Ввод исходных данных", xpos, ypos, true, this);
                    ypos++;
                    dCountLabel = NewLabel("Количество стратегий ЛПР:", xpos, ypos, true, this);
                    xpos += 2;
                    dCountComboBox = NewComboBox(xpos, ypos, 1, new object[] { 2, 3, 4 }, Scr.II.NEI.L.GetLength(0) - 2, dCountComboBox_SelectedIndexChanged, true, this);
                    xpos = 0; ypos++;
                    sCountLabel = NewLabel("Количество состояний природы:", xpos, ypos, true, this);
                    xpos += 2;
                    sCountComboBox = NewComboBox(xpos, ypos, 1, new object[] { 2, 3, 4 }, Scr.II.NEI.L.GetLength(1) - 2, sCountComboBox_SelectedIndexChanged, true, this);
                    xpos = 0; ypos++;
                    expCountLabel = NewLabel("Количество экспериментов:", xpos, ypos, true, this);
                    xpos += 2;
                    expCountComboBox = NewComboBox(xpos, ypos, 1, new object[] { 0, 1, 2 }, Scr.II.EIM.Length, expCountComboBox_SelectedIndexChanged, true, this);

                    xpos++; ypos -= 2;
                    DescriptionTB = NewTextBox(((SettingsPanel_DecisionTree)Scr.PnlC.XSSP).DescriptionTB.Text, xpos, ypos, 4, 4, true, true, null, this);
                    DescriptionTB.Font = new Font(DescriptionTB.Font.FontFamily, DescriptionTB.Font.SizeInPoints + 2);
                    xpos = 0; ypos += 3;

                    outButton = NewButton("Выйти из сценария", xpos, ypos, 2, 1, outButton_Click, true, this);
                    F1.ControlText(outButton, "Выход в меню выбора сценария. Введённые данные будут сброшены.");
                    xpos += 2;
                    beginWorkButton = NewButton("Начать", xpos, ypos, 1, 1, beginWorkButton_Click, true, this);
                    F1.ControlText(beginWorkButton, "Переход к формированию дерева принятия решений.");
                    xpos = 0; ypos++;

                    NoExpTP = new NoExpTabPage("Игра без эксперимента", this);
                    ExpTPM = new ExpTabPage[Scr.II.EIM.Length];
                    for (int i = 0; i < _Scr.II.EIM.Length; i++)
                        ExpTPM[i] = new ExpTabPage("Эксперимент " + i, this, i);
                    TabPage[] TPm = new TabPage[1 + _Scr.II.EIM.Length];
                    TPm[0] = NoExpTP;
                    for (int i = 0; i < _Scr.II.EIM.Length; i++)
                        TPm[i + 1] = ExpTPM[i];
                    dataTabControl = NewTabControl(TPm, xpos, ypos, 7, 12, true, this);
                    Scr.DIPanel = this;
                    Scr.MainPanel.Controls.Add(this);
                }
                /// <summary>
                /// Удаление панели ввода
                /// </summary>
                /// <returns>Если успешно</returns>
                public bool unlink()
                {
                    if (Scr.DIPanel != this)
                        return false;
                    Scr.DIPanel = null;
                    Scr.MainPanel.Controls.Remove(this);
                    return true;
                }
                /// <summary>
                /// Действие при изменении количества стратегий ЛПР
                /// </summary>
                /// <param name="sender">Объект выпадающего списка</param>
                /// <param name="e">Аргументы события</param>
                private void dCountComboBox_SelectedIndexChanged(object sender, System.EventArgs e)
                {
                    if (Scr.isChanging)
                        return;
                    Scr.isChanging = true;

                    int newDCnt = (int)dCountComboBox.SelectedItem, gd = Scr.II.NEI.L.GetLength(0), gs = Scr.II.NEI.L.GetLength(1);
                    double[,] newL = new double[newDCnt, gs];
                    NoExpTP.LDGV.RowCount = newDCnt;
                    if (newDCnt < gd)
                    {
                        for (int vd = 0; vd < newDCnt; vd++)
                            for (int vs = 0; vs < gs; vs++)
                                newL[vd, vs] = Scr.II.NEI.L[vd, vs];
                    }
                    else
                    {
                        for (int vd = 0; vd < gd; vd++)
                            for (int vs = 0; vs < gs; vs++)
                                newL[vd, vs] = Scr.II.NEI.L[vd, vs];
                        for (int vd = gd; vd < newDCnt; vd++)
                            for (int vs = 0; vs < gs; vs++)
                            {
                                newL[vd, vs] = 0.0;

                                NoExpTP.LDGV[vs, vd].ValueType = typeof(double);
                                NoExpTP.LDGV[vs, vd].Value = 0.0;
                            }
                    }
                    Scr.II.NEI.L = newL;
                    NoExpTP.formClientSizeOfLDGV();
                    //Scr.II.NEI.L

                    Scr.isChanging = false;
                }
                /// <summary>
                /// Действие при изменении количества состояний природы
                /// </summary>
                /// <param name="sender">Объект выпадающего списка</param>
                /// <param name="e">Аргументы события</param>
                private void sCountComboBox_SelectedIndexChanged(object sender, System.EventArgs e)
                {
                    if (Scr.isChanging)
                        return;
                    Scr.isChanging = true;

                    int newSCnt = (int)sCountComboBox.SelectedItem, gd = Scr.II.NEI.L.GetLength(0), gs = Scr.II.NEI.L.GetLength(1);

                    double[,] newL = new double[gd, newSCnt];
                    NoExpTP.LDGV.ColumnCount = newSCnt;
                    if (newSCnt < gs)
                    {
                        for (int vd = 0; vd < gd; vd++)
                            for (int vs = 0; vs < newSCnt; vs++)
                                newL[vd, vs] = Scr.II.NEI.L[vd, vs];
                    }
                    else
                    {
                        for (int vd = 0; vd < gd; vd++)
                            for (int vs = 0; vs < gs; vs++)
                                newL[vd, vs] = Scr.II.NEI.L[vd, vs];
                        for (int vd = 0; vd < gd; vd++)
                            for (int vs = gs; vs < newSCnt; vs++)
                            {
                                newL[vd, vs] = 0.0;
                                NoExpTP.LDGV[vs, vd].ValueType = typeof(double);
                                NoExpTP.LDGV[vs, vd].Value = Scr.loss ? -newL[vd, vs] : newL[vd, vs];
                            }
                    }
                    NoExpTP.formClientSizeOfLDGV();
                    Scr.II.NEI.L = newL;

                    if (Scr.UsingAP)
                    {
                        double[] newP = new double[newSCnt];
                        NoExpTP.PDGV.ColumnCount = newSCnt;
                        if (newSCnt < gs)
                        {
                            double pX = 0.0;
                            for (int vs = newSCnt; vs < gs; vs++)
                                pX += Scr.II.NEI.P[vs];
                            pX /= newSCnt;
                            for (int vs = 0; vs < newSCnt; vs++)
                            {
                                newP[vs] = Scr.II.NEI.P[vs] + pX;
                                NoExpTP.PDGV[vs, 0].Value = newP[vs];
                            }
                        }
                        else
                        {
                            for (int vs = 0; vs < gs; vs++)
                                newP[vs] = Scr.II.NEI.P[vs];
                            for (int vs = gs; vs < newSCnt; vs++)
                            {
                                newP[vs] = 0.0;
                                NoExpTP.PDGV[vs, 0].ValueType = typeof(double);
                                NoExpTP.PDGV[vs, 0].Value = newP[vs];
                            }
                        }
                        NoExpTP.formClientSizeOfPDGV();
                        Scr.II.NEI.P = newP;

                        for (int exp = 0; exp < ExpTPM.Length; exp++)
                        {
                            int gz = Scr.II.EIM[exp].Pzs.GetLength(0);
                            double[,] newPzs = new double[gz, newSCnt];
                            ExpTPM[exp].PzsDGV.ColumnCount = newSCnt;
                            if (newSCnt < gs)
                            {
                                for (int vz = 0; vz < gz; vz++)
                                    for (int vs = 0; vs < newSCnt; vs++)
                                        newPzs[vz, vs] = Scr.II.EIM[exp].Pzs[vz, vs];
                            }
                            else
                            {
                                double odpz = 1.0 / gz;
                                for (int vz = 0; vz < gz; vz++)
                                    for (int vs = 0; vs < gs; vs++)
                                        newPzs[vz, vs] = Scr.II.EIM[exp].Pzs[vz, vs];
                                for (int vz = 0; vz < gz; vz++)
                                    for (int vs = gs; vs < newSCnt; vs++)
                                    {
                                        newPzs[vz, vs] = odpz;
                                        ExpTPM[exp].PzsDGV[vs, vz].ValueType = typeof(double);
                                        ExpTPM[exp].PzsDGV[vs, vz].Value = newPzs[vz, vs];
                                    }
                            }
                            ExpTPM[exp].formClientSizeOfPzsDGV();
                            Scr.II.EIM[exp].Pzs = newPzs;

                            if (Scr.UsingB)
                            {
                                double[] newb = new double[newSCnt];
                                ExpTPM[exp].bDGV.ColumnCount = newSCnt;
                                if (newSCnt < gs)
                                {
                                    for (int vs = 0; vs < newSCnt; vs++)
                                        newb[vs] = Scr.II.EIM[exp].b[vs];
                                }
                                else
                                {
                                    for (int vs = 0; vs < gs; vs++)
                                        newb[vs] = Scr.II.EIM[exp].b[vs];
                                    for (int vs = gs; vs < newSCnt; vs++)
                                    {
                                        newb[vs] = 0.0;
                                        ExpTPM[exp].bDGV[vs, 0].ValueType = typeof(double);
                                        ExpTPM[exp].bDGV[vs, 0].Value = newb[vs];
                                    }
                                }
                                ExpTPM[exp].formClientSizeOfbDGV();
                                Scr.II.EIM[exp].b = newb;
                            }
                        }
                    }

                    if (Scr.UsingB)
                    {
                        double[] newb = new double[newSCnt];
                        NoExpTP.bDGV.ColumnCount = newSCnt;
                        if (newSCnt < gs)
                        {
                            for (int vs = 0; vs < newSCnt; vs++)
                                newb[vs] = Scr.II.NEI.b[vs];
                        }
                        else
                        {
                            for (int vs = 0; vs < gs; vs++)
                                newb[vs] = Scr.II.NEI.b[vs];
                            for (int vs = gs; vs < newSCnt; vs++)
                            {
                                newb[vs] = 0.0;
                                NoExpTP.bDGV[vs, 0].ValueType = typeof(double);
                                NoExpTP.bDGV[vs, 0].Value = newb[vs];
                            }
                        }
                        NoExpTP.formClientSizeOfbDGV();
                        Scr.II.NEI.b = newb;
                    }

                    Scr.isChanging = false;
                }
                /// <summary>
                /// Действие при изменении количества экспериментов
                /// </summary>
                /// <param name="sender">Объект выпадающего списка</param>
                /// <param name="e">Аргументы события</param>
                private void expCountComboBox_SelectedIndexChanged(object sender, EventArgs e)
                {
                    if (Scr.isChanging)
                        return;
                    Scr.isChanging = true;

                    int newExpCnt = (int)expCountComboBox.SelectedItem;
                    InputInfo.ExpInputInfo[] eIIxM = new InputInfo.ExpInputInfo[newExpCnt];
                    ExpTabPage[] TpMx = new ExpTabPage[newExpCnt];

                    if (newExpCnt < Scr.II.EIM.Length)
                    {
                        for (int exp = 0; exp < newExpCnt; exp++)
                        {
                            eIIxM[exp] = Scr.II.EIM[exp];
                            TpMx[exp] = ExpTPM[exp];
                        }
                        for (int exp = newExpCnt; exp < Scr.II.EIM.Length; exp++)
                            dataTabControl.Controls.Remove(ExpTPM[exp]);
                        Scr.II.EIM = eIIxM;
                    }
                    else
                    {
                        for (int exp = 0; exp < Scr.II.EIM.Length; exp++)
                        {
                            eIIxM[exp] = Scr.II.EIM[exp];
                            TpMx[exp] = ExpTPM[exp];
                        }
                        for (int exp = Scr.II.EIM.Length; exp < newExpCnt; exp++)
                        {
                            int zCntStd = 2, gs = Scr.II.NEI.L.GetLength(1);
                            double odpz = 1.0 / zCntStd, odps = 1.0 / gs;
                            eIIxM[exp] = new InputInfo.ExpInputInfo();
                            eIIxM[exp].Pzs = new double[zCntStd, gs];
                            for (int vz = 0; vz < zCntStd; vz++)
                                for (int vs = 0; vs < gs; vs++)
                                    eIIxM[exp].Pzs[vz, vs] = odpz;
                            eIIxM[exp].b = new double[gs];
                            for (int vs = 0; vs < gs; vs++)
                                eIIxM[exp].b[vs] = odps;
                            eIIxM[exp].c = 0.0;
                        }
                        Scr.II.EIM = eIIxM;
                        for (int exp = ExpTPM.Length; exp < newExpCnt; exp++)
                        {
                            TpMx[exp] = new ExpTabPage("Эксперимент " + exp, this, exp);
                            dataTabControl.Controls.Add(TpMx[exp]);
                        }
                    }
                    ExpTPM = TpMx;

                    Scr.isChanging = false;
                }
                /// <summary>кнопки задания случайных значений условных веротятностей
                /// Действие для кнопки выхода из сценария
                /// </summary>
                /// <param name="sender">Объект кнопки</param>
                /// <param name="e">Аргументы события</param>
                private void outButton_Click(object sender, EventArgs e)
                {
                    DialogResult res = MessageBox.Show("Введённые данные будут сброшены.", "Выйти?", MessageBoxButtons.YesNo);
                    if (res == DialogResult.Yes)
                    {
                        Scr.endScript();
                        unlink();
                    }
                }
                /// <summary>кнопки задания случайных значений условных веротятностей
                /// Действие для кнопки начала работы сценария
                /// </summary>
                /// <param name="sender">Объект кнопки</param>
                /// <param name="e">Аргументы события</param>
                private void beginWorkButton_Click(object sender, EventArgs e)
                {
                    Scr.WPanel = new WorkPanel(Scr);
                    Visible = false;
                    Scr.WPanel.Visible = true;
                }
            }
            /// <summary>
            /// Класс рабочей панели
            /// </summary>
            public class WorkPanel : Panel
            {
                /// <summary>
                /// Названия критериев при выигрышах (позиция согласно номеру)
                /// </summary>
                public static object[] CriteriaWNames = new object[]
                {
                    "Лапласа",
                    "Максимакса",
                    "Максимина",
                    "Сэвиджа",
                    "Произведений",
                    "Гурвица",
                    "Байеса-Лапласа",
                    "Гермейера",
                    "Ходжа-Леманна",
                    "БЛ-ММ"
                };
                /// <summary>
                /// Названия критериев при потерях (позиция согласно номеру)
                /// </summary>
                public static object[] CriteriaLNames = new object[]
                {
                    "Лапласа",
                    "Минимина",
                    "Минимакса",
                    "Сэвиджа",
                    "Произведений",
                    "Гурвица",
                    "Байеса-Лапласа",
                    "Гермейера",
                    "Ходжа-Леманна",
                    "БЛ-ММ"
                };
                /// <summary>
                /// Обоснования критериев при выигрышах (позиция согласно номеру)
                /// </summary>
                public static object[] criteriaRationalWNames = new object[]
                {
                    "Равенство вероятностей",
                    "Максимизация минимального выигрыша",
                    "Ожидание максимального выигрыша",
                    "Минимизация недополученного выигрыша",
                    "Максимизация произведений выигрышей",
                    "Степень преобладания лучшей ситуации",
                    "Распределение вероятностей",
                    "Максимизация произведения выигрыша на вероятность",
                    "Степень доверия распределению",
                    "В соответствии с BLMM"
                };
                /// <summary>
                /// Обоснования критериев при потерях (позиция согласно номеру)
                /// </summary>
                public static object[] criteriaRationalLNames = new object[]
                {
                    "Равенство вероятностей",
                    "Минимизация максимальных потерь",
                    "Ожидание минимальных потерь",
                    "Отдаление от возможных потерь",
                    "Минимизация произведений проигрышей",
                    "Степень преобладания лучшей ситуации",
                    "Распределение вероятностей",
                    "Минимзация произведения проигрыша на вероятность",
                    "Степень доверия распределению",
                    "В соответствии с BLMM"
                };
                /// <summary>
                /// Абстрактный класс узла дерева для панели
                /// </summary>
                public abstract class PnlTreeNode : TreeNode
                {
                    /// <summary>
                    /// Рабочая панель
                    /// </summary>
                    public WorkPanel WP;
                    /// <summary>
                    /// Панель, соответствующая текущему узлу дерева
                    /// </summary>
                    public TNPanel TNP;
                    /// <summary>
                    /// Конструктор
                    /// </summary>
                    /// <param name="_WP">Рабочая панель</param>
                    public PnlTreeNode(WorkPanel _WP)
                    {
                        WP = _WP;
                        TNP = null;
                    }
                    /// <summary>
                    /// Абстрактный метод формирования развёрнутого ответа
                    /// </summary>
                    public abstract void formTResultModel();
                    /// <summary>
                    /// Удаление элемента из дерева
                    /// </summary>
                    /// <returns>Успешность операции</returns>
                    public abstract bool unlink();
                }
                /// <summary>
                /// Класс узла дерева выбора критерия
                /// </summary>
                public class CrtTreeNode : PnlTreeNode
                {
                    /// <summary>
                    /// Конструктор
                    /// </summary>
                    /// <param name="_WP">Рабочая панель</param>
                    public CrtTreeNode(WorkPanel _WP) : base(_WP)
                    {
                        Text = "Выбор критерия.";
                        new CrtPanel(this);
                        WP.CrTN = this;
                        WP.DecisionTV.Nodes.Add(this);
                    }
                    /// <summary>
                    /// Метод формирования развёрнутого ответа
                    /// </summary>
                    public override void formTResultModel()
                    {
                        CrtPanel CrPnl = ((CrtPanel)this.TNP);
                        WP.TPageTB.Text = StringsTemplateGenerator.generateTemplateTextCriterion(CrPnl.CriteriaCB.SelectedIndex, CrPnl.CriteriaRationalCB.SelectedIndex, WP.Scr.loss, CrPnl.ParamValue);
                    }
                    /// <summary>
                    /// Удаление элемента из дерева
                    /// </summary>
                    /// <returns>Успешность операции</returns>
                    public override bool unlink()
                    {
                        if (WP.CrTN != this)
                            return false;
                        WP.CrTN = null;
                        WP.DecisionTV.Nodes.Remove(this);
                        return true;
                    }
                }
                /// <summary>
                /// Класс узла дерева для игры без эксперимента
                /// </summary>
                public class NoExpTreeNode : PnlTreeNode
                {
                    /// <summary>
                    /// Конструктор
                    /// </summary>
                    /// <param name="_WP">Рабочая панель</param>
                    public NoExpTreeNode(WorkPanel _WP) : base(_WP)
                    {
                        Text = "Игра без эксперимента.";
                        WP.NETN = this;
                        new NoExpPanel(this);
                        WP.DecisionTV.Nodes.Add(this);
                    }
                    /// <summary>
                    /// Метод формирования развёрнутого ответа
                    /// </summary>
                    public override void formTResultModel()
                    {
                        NoExpPanel NEPnl = ((NoExpPanel)this.TNP);//Текущая панель элементов
                        StringBuilder sb = new StringBuilder();
                        double wval = NEPnl.NERD.ML[NEPnl.StrategyCB.SelectedIndex];//Значение ожидаемого выигрыша для игры без эксперимента
                        sb.Append(StringsTemplateGenerator.generateTemplateTextNoExperiment(NEPnl.StrategyCB.SelectedIndex, WP.Scr.loss, wval, Methods.NeedP(((CrtPanel)WP.CrTN.TNP).CurrentCriteria)));

                        //Далее - если имеются эксперименты
                        if (WP.E0TN != null)
                        {
                            sb.Append(StringsTemplateGenerator.newLine);
                            ExpPanel E0Pnl = (ExpPanel)WP.E0TN.TNP;//Панель начального эксперимента
                            int dcn;
                            if (!NEPnl.bNeedNextExperimentCB.Checked)
                                dcn = 1;
                            else
                            {
                                if (!E0Pnl.CostDoCurExperimentCB.Checked)
                                    dcn = 2;
                                else
                                    dcn = 0;
                            }
                            sb.Append(StringsTemplateGenerator.generateTemplateTextExperimentDecision(0, dcn));
                        }
                        WP.TPageTB.Text = sb.ToString();
                    }
                    /// <summary>
                    /// Удаление элемента из дерева
                    /// </summary>
                    /// <returns>Успешность операции</returns>
                    public override bool unlink()
                    {
                        if (WP.NETN != this)
                            return false;
                        WP.NETN = null;
                        WP.DecisionTV.Nodes.Remove(this);
                        return true;
                    }
                }
                /// <summary>
                /// Класс узла дерева для эксперимента
                /// </summary>
                public class ExpTreeNode : PnlTreeNode
                {
                    /// <summary>
                    /// Номер эксперимента
                    /// </summary>
                    public int expNum;
                    /// <summary>
                    /// Предыдущий исход
                    /// </summary>
                    public int prevZNum;
                    /// <summary>
                    /// Конструктор
                    /// </summary>
                    /// <param name="_WP">Рабочая панель</param>
                    /// <param name="_Prnt">Элемент предыдущего эксперимента</param>
                    /// <param name="_prevZNum">Номер исхода предыдущего эксперимента (для начального - 0)</param>
                    public ExpTreeNode(WorkPanel _WP, ExpTreeNode _Prnt, int _prevZNum = 0) : base(_WP)
                    {
                        prevZNum = _prevZNum;
                        if (_Prnt == null)
                        {
                            expNum = 0;
                            WP.E0TN = this;
                            WP.DecisionTV.Nodes.Add(this);
                            NoExpPanel NEp = (NoExpPanel)_WP.NETN.TNP;
                            if (NEp.bNeedNextExperimentCB.Checked)
                                setActive(true);
                        }
                        else
                        {
                            expNum = _Prnt.expNum + 1;
                            _Prnt.Nodes.Add(this);
                            ExpPanel Ep = (ExpPanel)_Prnt.TNP;
                            if (Ep.bPszNeedNextExperiments[prevZNum])
                                setActive(true);
                        }
                        formText();
                    }
                    /// <summary>
                    /// Формирование заголовка элемента дерева решений
                    /// </summary>
                    public virtual void formText()
                    {
                        bool active = (this.TNP != null);
                        StringBuilder sb = new StringBuilder();
                        string eNumStr = StringsTemplateGenerator.getNum(
                            expNum + 1,
                            StringsTemplateGenerator.Pad.Im,
                            StringsTemplateGenerator.RdM.m,
                            false,
                            true
                            );
                        sb.Append(active ? eNumStr + " эксперимент" : "*");
                        sb.Append(". ");
                        if (Parent != null)
                        {
                            ExpTreeNode Ep = (ExpTreeNode)Parent;
                            int[] EpM = new int[expNum - 1];
                            int EpMInd = EpM.Length;
                            while (Ep.Parent != null)
                            {
                                EpMInd--;
                                EpM[EpMInd] = Ep.prevZNum;
                                Ep = (ExpTreeNode)Ep.Parent;
                            }
                            while (EpMInd != EpM.Length)
                            {
                                sb.Append(EpM[EpMInd]);
                                sb.Append("->");
                                EpMInd++;
                            }
                            sb.Append(prevZNum);
                            sb.Append(".");
                        }
                        Text = sb.ToString();
                    }
                    /// <summary>
                    /// Метод формирования развёрнутого ответа
                    /// </summary>
                    public override void formTResultModel()
                    {
                        StringBuilder sb = new StringBuilder();
                        ExpPanel EPnl = ((ExpPanel)this.TNP);
                        if (EPnl == null)
                        {//Эксперимент не проводится по причине вхождения в предыдущую область останова
                            sb.Append(StringsTemplateGenerator.generateTemplateTextExperimentDecision(expNum, 1));
                        }
                        else
                        {
                            if (((ExpPanel)TNP).CostDoCurExperiment == false)
                            {//Эксперимент не проводится по причине высокой стоимости
                                sb.Append(StringsTemplateGenerator.generateTemplateTextExperimentDecision(expNum, 2));
                            }
                            else
                            {//Эксприемент проводится
                                int 
                                    zcnt = EPnl.ERD.MLs.GetLength(0),
                                    scnt = EPnl.ERD.MLs.GetLength(1);
                                
                                sb.Append(StringsTemplateGenerator.generateTemplateTextExperiment(
                                    expNum,
                                    WP.Scr.loss,
                                    EPnl.ERD.Oop - WP.Scr.II.EIM[expNum].c
                                    ));

                                for (int z = 0; z < zcnt; z++)
                                {//Для каждого исхода эксперимента
                                    sb.Append(StringsTemplateGenerator.newLine);
                                    sb.Append(StringsTemplateGenerator.generateTemplateTextExperimentStrategy(
                                        expNum,
                                        z,
                                        EPnl.StrategyM[z],
                                        WP.Scr.loss,
                                        EPnl.ERD.op[z] - WP.Scr.II.EIM[expNum].c
                                        ));
                                    if (expNum < (WP.Scr.II.EIM.Length - 1))
                                    {//Если текущий экспреимент не является последним
                                        ExpPanel EPz = ((ExpPanel)((ExpTreeNode)Nodes[z]).TNP);
                                        int dcn;
                                        if (!EPnl.bPszNeedNextExperiments[z])
                                            dcn = 1;
                                        else
                                            if (!EPz.CostDoCurExperimentCB.Checked)
                                                dcn = 2;
                                            else
                                                dcn = 0;
                                        sb.Append(StringsTemplateGenerator.newLine);
                                        sb.Append(StringsTemplateGenerator.generateTemplateTextExperimentDecision(expNum + 1, dcn));
                                    }
                                }
                            }
                        }
                        WP.TPageTB.Text = sb.ToString();
                    }
                    /// <summary>
                    /// Удаление элемента из дерева
                    /// </summary>
                    /// <returns>Успешность операции</returns>
                    public override bool unlink()
                    {
                        if (Parent == null)
                            if (WP.E0TN != this)
                                return false;
                        setActive(false);
                        WP.E0TN = null;
                        WP.DecisionTV.Nodes.Remove(this);
                        return true;
                    }
                    /// <summary>
                    /// Устанока доступности настройки эксперимента
                    /// </summary>
                    /// <param name="_bPsz">Возможность проведения эксперимента на основе вхождения в предыдущую область останова</param>
                    public void setActive(bool _bPsz)
                    {
                        if (_bPsz)
                            new ExpPanel(this);
                        else
                            ((ExpPanel)TNP).notUsing();
                    }
                }
                /// <summary>
                /// Абстрактный класс панели для узла дерева
                /// </summary>
                public abstract class TNPanel : Panel
                {
                    /// <summary>
                    /// Узел дерева, соответствующий текущей панели
                    /// </summary>
                    public PnlTreeNode PTNode;
                    /// <summary>
                    /// Конструктор
                    /// </summary>
                    /// <param name="_PTNode">Узел дерева, соответствующий текущей панели</param>
                    public TNPanel(PnlTreeNode _PTNode)
                    {
                        PTNode = _PTNode;
                        if (PTNode != null)
                        {
                            PTNode.TNP = this;
                            PTNode.WP.DPage.Controls.Add(this);
                        }
                    }
                    /// <summary>
                    /// Перевод панели в неактивное состояние
                    /// </summary>
                    public virtual void notUsing()
                    {
                        //Visible = false;
                        PTNode.WP.DPage.Controls.Remove(this);
                        PTNode.TNP = null;
                    }
                }
                /// <summary>
                /// Панель выбора критерия
                /// </summary>
                public class CrtPanel : TNPanel
                {
                    /// <summary>
                    /// Указание на выбор критерия
                    /// </summary>
                    public Label CriteriaLabel;
                    /// <summary>
                    /// Номер выбранного критерия
                    /// </summary>
                    public int CurrentCriteria;
                    /// <summary>
                    /// Выбор критерия
                    /// </summary>
                    public ComboBox CriteriaCB;

                    /// <summary>
                    /// Указание на выбор обоснования критерия
                    /// </summary>
                    public Label CriteriaRationalLabel;
                    /// <summary>
                    /// Обоснование критерия
                    /// </summary>
                    public ComboBox CriteriaRationalCB;

                    /// <summary>
                    /// Указание на параметр
                    /// </summary>
                    public Label ParamLabel;
                    /// <summary>
                    /// Значение параметра
                    /// </summary>
                    public double ParamValue;
                    /// <summary>
                    /// Поле ввода вещественного значения параметра
                    /// </summary>
                    public TextBox ParamTB;

                    /// <summary>
                    /// Конструктор
                    /// </summary>
                    /// <param name="_CrTN">Узел дерева, соответствующий текущей панели</param>
                    public CrtPanel(CrtTreeNode _CrTN) : base(_CrTN)
                    {
                        AutoSize = true;
                        Location = new Point(0, 0);
                        Visible = false;

                        CurrentCriteria = 0;
                        int xpos = 0, ypos = 0;
                        bool loss = PTNode.WP.Scr.loss;
                        CriteriaLabel = NewLabel("Принцип оптимальности:", xpos, ypos, true, this);
                        xpos += 2;
                        CriteriaCB = NewComboBox(xpos, ypos, 3, loss ? CriteriaLNames : CriteriaWNames, CurrentCriteria, CriterionCB_SelectedIndexChanged, true, this);
                        xpos = 0; ypos++;

                        CriteriaRationalLabel = NewLabel("Обоснование принципа:", xpos, ypos, true, this);
                        xpos += 2;
                        CriteriaRationalCB = NewComboBox(xpos, ypos, 3, loss ? criteriaRationalLNames : criteriaRationalWNames, 0, CriteriaRationalCB_SelectedIndexChanged, true, this);
                        xpos = 0; ypos++;

                        ParamLabel = NewLabel("alpha:", xpos, ypos, false, this);
                        xpos += 1;
                        ParamValue = 0.5;
                        ParamTB = NewTextBox(ParamValue.ToString(), xpos, ypos, 1, 1, false, false, ParamTB_TextChanged, this);
                    }
                    /// <summary>
                    /// Действие при изменении критерия
                    /// </summary>
                    /// <param name="sender">Объект выпадающего списка</param>
                    /// <param name="e">Аргументы события</param>
                    private void CriterionCB_SelectedIndexChanged(object sender, EventArgs e)
                    {
                        if (PTNode.WP.Scr.isChanging)
                            return;
                        PTNode.WP.Scr.isChanging = true;
                        
                        InputInfo.NoExpInputInfo neII = PTNode.WP.Scr.II.NEI;
                        if ((Methods.NeedP(CriteriaCB.SelectedIndex)) && (neII.P == null))
                        {
                            MessageBox.Show("Использование критерия невозможно. Отсутствуют априорные вероятности.");
                            CriteriaCB.SelectedIndex = CurrentCriteria;
                            PTNode.WP.Scr.isChanging = false;
                            return;
                        }
                        CurrentCriteria = CriteriaCB.SelectedIndex;

                        if (Methods.NeedParam(CurrentCriteria))
                        {
                            switch (CurrentCriteria)
                            {
                                case 5: ParamLabel.Text = "alpha:"; break;
                                case 8: ParamLabel.Text = "nu:"; break;
                                case 9: ParamLabel.Text = "epsilon:"; break;
                            }
                            ParamValue = 0.5;
                            ParamTB.Text = ParamValue.ToString();

                            ParamLabel.Visible = true;
                            ParamTB.Visible = true;
                        }
                        else
                        {
                            ParamLabel.Visible = false;
                            ParamTB.Visible = false;
                        }

                        PTNode.WP.removeAllExperiments();
                        ((NoExpPanel)PTNode.WP.NETN.TNP).reset();

                        PTNode.formTResultModel();

                        PTNode.WP.Scr.isChanging = false;
                    }
                    /// <summary>
                    /// Действие при изменении обоснования критерия
                    /// </summary>
                    /// <param name="sender">Объект выпадающего списка</param>
                    /// <param name="e">Аргументы события</param>
                    private void CriteriaRationalCB_SelectedIndexChanged(object sender, EventArgs e)
                    {
                        if (PTNode.WP.Scr.isChanging)
                            return;
                        PTNode.WP.Scr.isChanging = true;

                        PTNode.WP.removeAllExperiments();
                        ((NoExpPanel)PTNode.WP.NETN.TNP).reset();

                        PTNode.formTResultModel();

                        PTNode.WP.Scr.isChanging = false;
                    }
                    /// <summary>
                    /// Действие при изменении значения параметра критерия
                    /// </summary>
                    /// <param name="sender">Объект текстовой области</param>
                    /// <param name="e">Аргументы события</param>
                    private void ParamTB_TextChanged(object sender, EventArgs e)
                    {
                        if (PTNode.WP.Scr.isChanging == true)
                            return;
                        PTNode.WP.Scr.isChanging = true;

                        double newVal;
                        WorkPanel WPt = PTNode.WP;
                        if ((Double.TryParse(ParamTB.Text, out newVal)) && (Methods.checkParam(CriteriaCB.SelectedIndex, newVal)))
                        {
                            ParamValue = newVal;

                            WPt.removeAllExperiments();
                            ((NoExpPanel)WPt.NETN.TNP).reset();

                            PTNode.formTResultModel();
                        }
                        else
                        {
                            MessageBox.Show("Некорректное значение параметра критерия.");
                            ParamTB.Text = ParamValue.ToString();
                        }

                        WPt.Scr.isChanging = false;
                    }
                }
                /// <summary>
                /// Панель для игры без эксперимента
                /// </summary>
                public class NoExpPanel : TNPanel
                {
                    /// <summary>
                    /// Данные для игры без экспериента
                    /// </summary>
                    public NoExpResultData NERD;

                    /// <summary>
                    /// Обозначение выбора стратегии
                    /// </summary>
                    public Label StrategyLabel;
                    /// <summary>
                    /// Решение по выбору стратегии
                    /// </summary>
                    public ComboBox StrategyCB;

                    /// <summary>
                    /// Следующий эксперимент необходимо провести (нет входа в область останова)
                    /// </summary>
                    public CheckBox bNeedNextExperimentCB;

                    /// <summary>
                    /// Обозначение матрицы выигрышей/потерь
                    /// </summary>
                    public Label LLabel;
                    /// <summary>
                    /// Матрица выигрышей/потерь
                    /// </summary>
                    public DataGridView LDGV;

                    /// <summary>
                    /// Обозначения матрицы априорных вероятностей
                    /// </summary>
                    public Label PLabel;
                    /// <summary>
                    /// Матрица априорных вероятностей
                    /// </summary>
                    public DataGridView PDGV;

                    /// <summary>
                    /// Обозначение области останова
                    /// </summary>
                    public Label bLabel;
                    /// <summary>
                    /// Область останова
                    /// </summary>
                    public DataGridView bDGV;

                    /// <summary>
                    /// Обозначение значений эффективности
                    /// </summary>
                    public Label KLabel;
                    /// <summary>
                    /// Значения эффективности
                    /// </summary>
                    public DataGridView KDGV;

                    /// <summary>
                    /// Обозначение ожидаемых выигрышей для каждого решения
                    /// </summary>
                    public Label MLLabel;
                    /// <summary>
                    /// Матрица ожидаемых выигрышей/потерь для каждого решения
                    /// </summary>
                    public DataGridView MLDGV;
                    /// <summary>
                    /// Обозначение множества вычисляемых значений для определения допустимых стратегий
                    /// </summary>
                    public Label VMLabel;
                    /// <summary>
                    /// Матрица множества вычисляемых значений для определения допустимых стратегий
                    /// </summary>
                    public DataGridView VMDGV;
                    //------------------------------------
                    /// <summary>
                    /// Конструктор
                    /// </summary>
                    /// <param name="_WP">Рабочая панель</param>
                    public NoExpPanel(PnlTreeNode _PnlTN) : base(_PnlTN)
                    {
                        AutoSize = true;
                        Location = new Point(0, 0);
                        Visible = false;

                        InputInfo IIt = PTNode.WP.Scr.II;
                        InputInfo.NoExpInputInfo neII = IIt.NEI;
                        CrtPanel CrPnl = (CrtPanel)PTNode.WP.CrTN.TNP;

                        bool loss = PTNode.WP.Scr.loss;
                        int gd = neII.L.GetLength(0), gs = neII.L.GetLength(1);
                        int crtNum = CrPnl.CriteriaCB.SelectedIndex;

                        NERD = null;
                        
                        int xpos = 0, ypos = 0;

                        StrategyLabel = NewLabel("Стратегия:", xpos, ypos, true, this);
                        xpos++;
                        object[] StrategyCBItems = new object[gd];
                        for (int vd = 0; vd < gd; vd++)
                            StrategyCBItems[vd] = StringsTemplateGenerator.getNum
                                (vd + 1,
                                StringsTemplateGenerator.Pad.Im,
                                StringsTemplateGenerator.RdM.j,
                                false,
                                true
                                );
                        StrategyCB = NewComboBox(xpos, ypos, 1, StrategyCBItems, 0, StrategyCB_SelectedIndexChanged, true, this);
                        xpos++;


                        bool
                            uAP = PTNode.WP.Scr.UsingAP,
                            uB = PTNode.WP.Scr.UsingB;
                        if (uAP)
                            bNeedNextExperimentCB = NewCheckBox("Возможен следующий эксперимент", false, xpos, ypos, bNeedNextExperimentsCB_CheckedChanged, true, this);
                        else
                            bNeedNextExperimentCB = null;
                        xpos = 0; ypos++;
                        LLabel = NewLabel("Матрица " + (loss ? "потерь" : "выигрышей") + ":", xpos, ypos, true, this);
                        ypos++;
                        LDGV = NewDataGridView(gd, gs, xpos, ypos, 5, 4, null, true, this);
                        for (int vd = 0; vd < gd; vd++)
                            for (int vs = 0; vs < gs; vs++)
                            {
                                LDGV[vs, vd].ValueType = typeof(double);
                                LDGV[vs, vd].Value = loss ? -neII.L[vd, vs] : neII.L[vd, vs];
                            }
                        ypos += 4;
                        
                        if (uAP)
                        {
                            PLabel = NewLabel("Априорные вероятности:", xpos, ypos, true, this);
                            ypos++;
                            PDGV = NewDataGridView(1, gs, xpos, ypos, 5, 2, null, true, this);
                            for (int vs = 0; vs < gs; vs++)
                            {
                                PDGV[vs, 0].ValueType = typeof(double);
                                PDGV[vs, 0].Value = neII.P[vs];
                            }
                            ypos += 2;
                        }
                        else
                        {
                            PLabel = null;
                            PDGV = null;
                        }
                        if (uB)
                        {
                            bLabel = NewLabel("Области останова:", xpos, ypos, true, this);
                            ypos++;
                            bDGV = NewDataGridView(1, gs, xpos, ypos, 5, 2, null, true, this);
                            for (int vs = 0; vs < gs; vs++)
                            {
                                bDGV[vs, 0].ValueType = typeof(double);
                                bDGV[vs, 0].Value = neII.b[vs];
                            }
                            ypos += 2;
                        }
                        else
                        {
                            bLabel = null;
                            bDGV = null;
                        }

                        KLabel = NewLabel("Значения эффективности:", xpos, ypos, true, this);
                        ypos++;
                        KDGV = NewDataGridView(gd, 1, xpos, ypos, 2, 4, null, true, this);
                        for (int vd = 0; vd < gd; vd++)
                            KDGV[0, vd].ValueType = typeof(double);
                        ypos += 4;

                        if (uAP)
                        {
                            MLLabel = NewLabel("Ожидаемые выигрыши для каждого решения", xpos, ypos, true, this);
                            ypos++;
                            MLDGV = NewDataGridView(gd, 1, xpos, ypos, 2, 4, null, true, this);
                            for (int vd = 0; vd < gd; vd++)
                                MLDGV[0, vd].ValueType = typeof(double);
                            ypos += 4;
                            if (crtNum == 9)
                            {
                                VMLabel = NewLabel("Вычисляемые значения", xpos, ypos, true, this);
                                ypos++;
                                VMDGV = NewDataGridView(gd, 2, xpos, ypos, 2, 4, null, true, this);
                                for (int vd = 0; vd < gd; vd++)
                                    for (int vx = 0; vx < 2; vx++)
                                        VMDGV[vx, vd].ValueType = typeof(double);
                                ypos += 4;
                            }
                            else
                            {
                                VMLabel = null;
                                VMDGV = null;
                            }
                        }
                        else
                        {
                            MLLabel = null;
                            MLDGV = null;
                            VMLabel = null;
                            VMDGV = null;
                        }

                        reCalculateValues();
                    }
                    public void reset()
                    {
                        StrategyCB.SelectedIndex = 0;
                        if (bNeedNextExperimentCB != null)
                            bNeedNextExperimentCB.Checked = false;
                        reCalculateValues();
                    }
                    /// <summary>
                    /// Перерасчёт значений
                    /// </summary>
                    public void reCalculateValues()
                    {
                        InputInfo.NoExpInputInfo neII = PTNode.WP.Scr.II.NEI;
                        CrtPanel CrPnl = (CrtPanel)PTNode.WP.CrTN.TNP;
                        Methods.NoExp(
                            neII.L,
                            neII.P,
                            neII.b,
                            CrPnl.CriteriaCB.SelectedIndex,
                            CrPnl.ParamValue,
                            ref NERD
                            );
                        int gd = neII.L.GetLength(0);
                        bool loss = PTNode.WP.Scr.loss;
                        int crtNum = CrPnl.CriteriaCB.SelectedIndex;
                        for (int vd = 0; vd < gd; vd++)
                        {
                            KDGV[0, vd].Value = NERD.K[vd];
                            if (MLDGV != null)
                                MLDGV[0, vd].Value = loss ? -NERD.ML[vd] : NERD.ML[vd];
                            if ((crtNum == 9) && (VMDGV != null))
                                for (int vx = 0; vx < 2; vx++)
                                    VMDGV[vx, vd].Value = NERD.VM[vd, vx];
                        }
                    }
                    /// <summary>
                    /// Действие при изменении стратегии
                    /// </summary>
                    /// <param name="sender">Объект выпадающего списка</param>
                    /// <param name="e">Аргументы события</param>
                    private void StrategyCB_SelectedIndexChanged(object sender, EventArgs e)
                    {
                        WorkPanel WPt = PTNode.WP;
                        if (WPt.Scr.isChanging == true)
                            return;
                        WPt.Scr.isChanging = true;
                        
                        if (bNeedNextExperimentCB.Checked)
                        {
                            bNeedNextExperimentCB.Checked = false;
                            WPt.removeAllExperiments();
                        }
                        PTNode.formTResultModel();

                        WPt.Scr.isChanging = false;
                    }
                    /// <summary>
                    /// Указание на возможность следующего эксперимента из-за отсутствия вхождения вероятностей область останова
                    /// </summary>
                    /// <param name="sender">Объект пункта</param>
                    /// <param name="e">Аргументы события</param>
                    private void bNeedNextExperimentsCB_CheckedChanged(object sender, EventArgs e)
                    {
                        if (PTNode.WP.Scr.isChanging == true)
                            return;
                        PTNode.WP.Scr.isChanging = true;

                        WorkPanel WPt = PTNode.WP;
                        InputInfo IIt = WPt.Scr.II;

                        if (bNeedNextExperimentCB.Checked)
                        {
                            if (!Methods.NeedP(((CrtPanel)WPt.CrTN.TNP).CurrentCriteria))
                            {
                                MessageBox.Show("Критерий не предполагает проведение экспериментов.");
                                bNeedNextExperimentCB.Checked = false;

                                PTNode.WP.Scr.isChanging = false;
                                return;
                            }
                            if ((IIt.NEI.P == null) || (IIt.NEI.P.Length == 0))
                            {
                                MessageBox.Show("Эксперимент невозможен без априорных вероятностей.");
                                bNeedNextExperimentCB.Checked = false;

                                WPt.Scr.isChanging = false;
                                return;
                            }
                            new ExpPanel(PTNode.WP.E0TN);

                        }
                        else
                            WPt.removeAllExperiments();
                        PTNode.formTResultModel();

                        WPt.Scr.isChanging = false;
                    }
                }
                /// <summary>
                /// Панель для эксперимента
                /// </summary>
                public class ExpPanel : TNPanel
                {
                    /// <summary>
                    /// Данные для экспериента
                    /// </summary>
                    public ExpResultData ERD;
                    //----------------------------------------

                    /// <summary>
                    /// Целесообразно ли проведение эксперимента на основе стоимости (запомненное значение)
                    /// </summary>
                    public bool CostDoCurExperiment;
                    /// <summary>
                    /// Целесообразно ли проведение эксперимента на основе стоимости
                    /// </summary>
                    public CheckBox CostDoCurExperimentCB; 

                    /// <summary>
                    /// Выбор исхода эксперимента
                    /// </summary>
                    public ComboBox ZCB;

                    /// <summary>
                    /// Обозначение стратегии
                    /// </summary>
                    public Label StrategyLabel;
                    /// <summary>
                    /// Множество стратегий пользователя для каждого исхода эксперимента
                    /// </summary>
                    public int[] StrategyM;
                    /// <summary>
                    /// Стратегия для текущего исхода экспреимента
                    /// </summary>
                    public ComboBox StrategyCB;

                    /// <summary>
                    /// Множество решений пользователя о возможности проведения следующего эксперимента при отсутствии вхождения в область останова
                    /// </summary>
                    public bool[] bPszNeedNextExperiments;
                    /// <summary>
                    /// Решение о возможности проведения следующего эксперимента при отсутствии вхождения в область останова для текущего исхода
                    /// </summary>
                    public CheckBox bPszNeedNextExperimentsCB;
                    //----------------------------------------

                    /// <summary>
                    /// Укзание на априорные вероятности
                    /// </summary>
                    public Label PLabel;
                    /// <summary>
                    /// Априорные вероятности
                    /// </summary>
                    public DataGridView PDGV;

                    /// <summary>
                    /// Указание на матрицу выигрышей
                    /// </summary>
                    public Label LLabel;
                    /// <summary>
                    /// Матрица выигрышей
                    /// </summary>
                    public DataGridView LDGV;

                    /// <summary>
                    /// Указание на условные вероятности
                    /// </summary>
                    public Label PzsLabel;
                    /// <summary>
                    /// Условные вероятности
                    /// </summary>
                    public DataGridView PzsDGV;

                    /// <summary>
                    /// Указание на область останова
                    /// </summary>
                    public Label bLabel;
                    /// <summary>
                    /// Область останова
                    /// </summary>
                    public DataGridView bDGV;

                    /// <summary>
                    /// Указание на стоимость эксперимента
                    /// </summary>
                    public Label cLabel;
                    /// <summary>
                    /// Стоимость эксперимента
                    /// </summary>
                    public TextBox cTB;
                    //------------------

                    /// <summary>
                    /// Обозначение апостериорных вероятностей
                    /// </summary>
                    public Label PszZLabel;
                    /// <summary>
                    /// Апостериорные вероятности для выбранного исхода эксперимента
                    /// </summary>
                    public DataGridView PszZDGV;

                    /// <summary>
                    /// Обозначение значений эффективности для выбранного исхода эксперимента
                    /// </summary>
                    public Label KLabel;
                    /// <summary>
                    /// Одномерная матрица значений эффективности для текущего исхода эксперимента
                    /// </summary>
                    public DataGridView KDGV;

                    /// <summary>
                    /// Обозначение ожидаемых выигрышей для каждой стратегии каждого исхода эксперимента
                    /// </summary>
                    public Label MLLabel;
                    /// <summary>
                    /// Ожидаемые выигрыши для каждой стратегии выбранного исхода эксперимента
                    /// </summary>
                    public DataGridView MLDGV;

                    /// <summary>
                    /// Обозначение вычисляемых значений
                    /// </summary>
                    public Label VMLabel;
                    /// <summary>
                    /// Вычисляемые значения выбранного исхода эксперимента
                    /// </summary>
                    public DataGridView VMDGV;
                    //--------------------------------
                    /// <summary>
                    /// Конструктор
                    /// </summary>
                    /// <param name="_ETN">Узел дерева для эксперимента</param>
                    public ExpPanel(ExpTreeNode _ETN) : base(_ETN)
                    {
                        Visible = PTNode.IsSelected;
                        AutoSize = true;
                        Location = PTNode.WP.CrTN.TNP.Location;

                        ExpTreeNode ETN = (ExpTreeNode)PTNode;
                        ETN.formText();

                        WorkPanel WPt = ETN.WP;
                        InputInfo curII = WPt.Scr.II;
                        InputInfo.NoExpInputInfo neI = curII.NEI;
                        InputInfo.ExpInputInfo eIcur = curII.EIM[ETN.expNum];
                        int gd = neI.L.GetLength(0), gs = neI.L.GetLength(1), gz = eIcur.Pzs.GetLength(0);

                        ERD = null;

                        CrtPanel CrPnl = (CrtPanel)WPt.CrTN.TNP;
                        ExpTreeNode PrntTN = (ExpTreeNode)ETN.Parent;
                        ExpPanel PrntEP = (PrntTN == null) ? null : (ExpPanel)PrntTN.TNP;

                        int zPrev = ETN.prevZNum;
                        int eNum = ETN.expNum;
                        int crtNum = CrPnl.CriteriaCB.SelectedIndex;

                        bool lastExp = (eNum == (curII.EIM.Length - 1));
                        //---------------------------------------------------
                        int xpos = 0, ypos = 0;

                        CostDoCurExperiment = true;//
                        CostDoCurExperimentCB = NewCheckBox("Стоимость приемлемая.", CostDoCurExperiment, xpos, ypos, CostDoCurExperimentCB_CheckedChanged, true, this);
                        ypos++;

                        int curSelZ = 0;
                        string[] ZCBResults = new string[gz];
                        for (int vz = 0; vz < gz; vz++)
                            ZCBResults[vz] = StringsTemplateGenerator.getNum(
                                vz + 1,
                                StringsTemplateGenerator.Pad.Im,
                                StringsTemplateGenerator.RdM.m,
                                false, true)
                                + " исход";
                        ZCB = NewComboBox(xpos, ypos, 2, ZCBResults, curSelZ, ZCB_SelectedIndexChanged, true, this);
                        ypos++;

                        StrategyLabel = NewLabel("Стратегия:", xpos, ypos, true, this);
                        xpos++;
                        object[] StrategyCBItems = new object[gd];
                        for (int vd = 0; vd < gd; vd++)
                            StrategyCBItems[vd] = StringsTemplateGenerator.getNum(
                                vd + 1,
                                StringsTemplateGenerator.Pad.Im,
                                StringsTemplateGenerator.RdM.j,
                                false,
                                true
                                );
                        StrategyM = new int[gz];
                        for (int vz = 0; vz < gz; vz++)
                            StrategyM[vz] = 0;
                        StrategyCB = NewComboBox(xpos, ypos, 1, StrategyCBItems, 0, StrategyCB_SelectedIndexChanged, true, this);
                        xpos = 0; ypos++;

                        if (lastExp)
                        {
                            bPszNeedNextExperiments = null;
                            bPszNeedNextExperimentsCB = null;
                        }
                        else
                        {
                            bPszNeedNextExperiments = new bool[gz];
                            for (int vz = 0; vz < gz; vz++)
                                bPszNeedNextExperiments[vz] = false;
                            bPszNeedNextExperimentsCB = NewCheckBox("Возможен следующий эксперимент.", false, xpos, ypos, bPszNeedNextExperimentsCB_CheckedChanged, true, this);
                            ypos++;
                        }
                        //---------------------------------------------------

                        PLabel = NewLabel("Априорные вероятности:", xpos, ypos, true, this);
                        ypos++;
                        PDGV = NewDataGridView(1, gs, xpos, ypos, 5, 2, null, true, this);
                        for (int vs = 0; vs < gs; vs++)
                            PDGV[vs, 0].ValueType = typeof(double);
                        ypos += 2;

                        LLabel = NewLabel("Матрица выигрышей:", xpos, ypos, true, this);
                        ypos++;
                        LDGV = NewDataGridView(gd, gs, xpos, ypos, 5, 4, null, true, this);
                        for (int vd = 0; vd < gd; vd++)
                            for (int vs = 0; vs < gs; vs++)
                            {
                                LDGV[vs, vd].ValueType = typeof(double);
                                LDGV[vs, vd].Value = WPt.Scr.loss ? -neI.L[vd, vs] : neI.L[vd, vs];
                            }
                        ypos += 4;

                        PzsLabel = NewLabel("Условные вероятности:", xpos, ypos, true, this);
                        ypos++;
                        PzsDGV = NewDataGridView(gz, gs, xpos, ypos, 4, 4, null, true, this);
                        for (int vz = 0; vz < gz; vz++)
                            for (int vs = 0; vs < gs; vs++)
                            {
                                PzsDGV[vs, vz].ValueType = typeof(double);
                                PzsDGV[vs, vz].Value = eIcur.Pzs[vz, vs];
                            }
                        ypos += 4;

                        if (PTNode.WP.Scr.UsingB)
                        {
                            bLabel = NewLabel("Области останова:", xpos, ypos, true, this);
                            ypos++;
                            bDGV = NewDataGridView(1, gs, xpos, ypos, 5, 2, null, true, this);
                            for (int vs = 0; vs < gs; vs++)
                            {
                                bDGV[vs, 0].ValueType = typeof(double);
                                bDGV[vs, 0].Value = eIcur.b[vs];
                            }
                            ypos += 2;
                        }
                        else
                        {
                            bLabel = null;
                            bDGV = null;
                        }
                        cLabel = NewLabel("Стоимость эксперимента:", xpos, ypos, true, this);
                        xpos += 3;
                        cTB = NewTextBox(eIcur.c.ToString(), xpos, ypos, 1, 1, true, false, null, this);
                        xpos = 0; ypos++;

                        //---------------------------------

                        PszZLabel = NewLabel("Апостериорные вероятности результата:", xpos, ypos, true, this);
                        ypos++;
                        PszZDGV = NewDataGridView(1, gs, xpos, ypos, 5, 2, null, true, this);
                        for (int vs = 0; vs < gs; vs++)
                            PszZDGV[vs, 0].ValueType = typeof(double);
                        ypos += 2;

                        int xpost = xpos, ypost = ypos;
                        KLabel = NewLabel("Значения эффективности:", xpos, ypos, true, this);
                        ypos++;
                        KDGV = NewDataGridView(gd, 1, xpos, ypos, 2, 4, null, true, this);
                        for (int vd = 0; vd < gd; vd++)
                            KDGV[0, vd].ValueType = typeof(double);

                        xpos += 2; ypos = ypost;
                        MLLabel = NewLabel("Ожидаемые выигрыши", xpos, ypos, true, this);
                        ypos++;
                        MLDGV = NewDataGridView(gd, 1, xpos, ypos, 2, 4, null, true, this);
                        for (int vd = 0; vd < gd; vd++)
                            MLDGV[0, vd].ValueType = typeof(double);
                        xpos = xpost; ypos += 4;

                        if (crtNum == 9)
                        {
                            VMLabel = NewLabel("Вычислимые значения", xpos, ypos, true, this);
                            ypos++;
                            VMDGV = NewDataGridView(gd, 1, xpos, ypos, 2, 4, null, true, this);
                            for (int vd = 0; vd < gd; vd++)
                                for (int vx = 0; vx < 2; vx++)
                                    VMDGV[vx, vd].ValueType = typeof(double);
                            ypos += 4;
                        }
                        else
                        {
                            VMLabel = null;
                            VMDGV = null;
                        }

                        if (!lastExp)
                            for (int i = 0; i < gz; i++)
                                new ExpTreeNode(WPt, ETN, i);

                        reCalculateValues();
                    }
                    /// <summary>
                    /// Заполнение переменных по результатам рассчётов
                    /// </summary>
                    public bool reWriteValues()
                    {
                        if (ERD == null)
                            return false;
                        ExpTreeNode curETN = (ExpTreeNode)PTNode;
                        CrtPanel CrPnl = (CrtPanel)PTNode.WP.CrTN.TNP;
                        int crtNum = CrPnl.CriteriaCB.SelectedIndex;
                        int eNum = curETN.expNum;
                        int zPrev = curETN.prevZNum;
                        ExpPanel EPt = (ExpPanel)PTNode.TNP;
                        InputInfo IIt = PTNode.WP.Scr.II;
                        InputInfo.NoExpInputInfo neII = IIt.NEI;
                        InputInfo.ExpInputInfo eII = IIt.EIM[eNum];
                        ExpPanel EPprev;
                        int dPrev;
                        double prevWval;
                        
                        if (eNum == 0)
                        {
                            NoExpPanel NP = (NoExpPanel)PTNode.WP.NETN.TNP;
                            EPprev = null;
                            dPrev = NP.StrategyCB.SelectedIndex;
                            prevWval = NP.NERD.ML[dPrev];
                        }
                        else
                        {
                            EPprev = (ExpPanel)(((ExpTreeNode)PTNode.Parent).TNP);
                            dPrev = EPprev.StrategyM[zPrev];
                            prevWval = EPprev.ERD.MLs[zPrev, dPrev] - IIt.EIM[eNum - 1].c;
                        }

                        int zSelected = EPt.ZCB.SelectedIndex;
                        int gd = neII.L.GetLength(0), gs = neII.L.GetLength(1), gz = eII.Pzs.GetLength(0);
                        bool loss = PTNode.WP.Scr.loss;
                        for (int vd = 0; vd < gd; vd++)
                        {
                            KDGV[0, vd].Value = ERD.Ks[zSelected, vd];
                            MLDGV[0, vd].Value = ERD.MLs[zSelected, vd];
                            if (crtNum == 9)
                                for (int vx = 0; vx < 2; vx++)
                                    VMDGV[vx, vd].Value = ERD.VMs[zSelected, vd, vx];
                        }
                        for (int vs = 0; vs < gs; vs++)
                        {
                            PszZDGV[vs, 0].Value = ERD.Psz[zSelected, vs];
                            PDGV[vs, 0].Value = (eNum == 0) ? neII.P[vs] : EPprev.ERD.Psz[zPrev, vs];
                        }
                        return true;
                    }
                    /// <summary>
                    /// Перерасчёт значений
                    /// </summary>
                    public void reCalculateValues()
                    {
                        ExpTreeNode curETN = (ExpTreeNode)PTNode;
                        int eNum = curETN.expNum;

                        InputInfo IIt = PTNode.WP.Scr.II;
                        InputInfo.NoExpInputInfo neII = IIt.NEI;
                        InputInfo.ExpInputInfo eII = IIt.EIM[eNum];

                        CrtPanel CrPnl = (CrtPanel)PTNode.WP.CrTN.TNP;

                        int dPrev;
                        double prevWval;

                        int zPrev = curETN.prevZNum;
                        int crtNum = CrPnl.CriteriaCB.SelectedIndex;
                        double crtParam = CrPnl.ParamValue;
                        if (eNum == 0)
                        {
                            NoExpPanel NP = (NoExpPanel)PTNode.WP.NETN.TNP;
                            dPrev = NP.StrategyCB.SelectedIndex;
                            prevWval = NP.NERD.ML[dPrev];
                        }
                        else
                        {
                            ExpPanel EPprev = (ExpPanel)(((ExpTreeNode)PTNode.Parent).TNP);
                            dPrev = EPprev.StrategyM[zPrev];
                            prevWval = EPprev.ERD.MLs[zPrev, dPrev] - IIt.EIM[eNum - 1].c;
                        }
                        Methods.Exp(
                            neII.L,
                            neII.P,
                            prevWval,
                            eII.b,
                            eII.Pzs,
                            eII.c,
                            crtNum,
                            crtParam,
                            ref ERD
                            );

                        reWriteValues();
                    }
                    /// <summary>
                    /// Перевод панели в неактивное состояние
                    /// </summary>
                    public override void notUsing()
                    {
                        base.notUsing();
                        ((ExpTreeNode)PTNode).formText();
                    }
                    /// <summary>
                    /// Действие при изменении решения о необходимости проведения эксперимента на основе стоимости
                    /// </summary>
                    /// <param name="sender">Объект пункта</param>
                    /// <param name="e">Аргументы события</param>
                    public void CostDoCurExperimentCB_CheckedChanged(object sender, EventArgs e)
                    {
                        if (PTNode.WP.Scr.isChanging == true)
                            return;
                        PTNode.WP.Scr.isChanging = true;

                        CostDoCurExperiment = CostDoCurExperimentCB.Checked;
                        ExpTreeNode ETN = (ExpTreeNode)PTNode;
                        WorkPanel WPt = ETN.WP;
                        InputInfo curII = WPt.Scr.II;
                        int zCount = curII.EIM[ETN.expNum].Pzs.GetLength(0);
                        CrtPanel CrPnl = (CrtPanel)WPt.CrTN.TNP;
                        int eNum = ETN.expNum;
                        int crtNum = CrPnl.CriteriaCB.SelectedIndex;
                        bool lastExp = (eNum == (curII.EIM.Length - 1));
                        StrategyLabel.Visible = CostDoCurExperiment;
                        StrategyCB.Visible = CostDoCurExperiment;
                        if (!lastExp)
                        {
                            if (CostDoCurExperiment)
                            {
                                bPszNeedNextExperimentsCB.Visible = true;
                                for (int i = 0; i < zCount; i++)
                                    new ExpTreeNode(WPt, ETN, i);
                            }
                            else
                            {
                                bPszNeedNextExperimentsCB.Visible = false;
                                for (int i = 0; i < zCount; i++)
                                    WPt.RemPanels((ExpTreeNode)ETN.Nodes[i]);
                                ETN.Nodes.Clear();

                            }
                        }
                        

                        ETN.formText();
                        ETN.formTResultModel();

                        WPt.Scr.isChanging = false;
                    }
                    /// <summary>
                    /// Действие при изменении анализируемого исхода эксперимента
                    /// </summary>
                    /// <param name="sender">Объект пункта</param>
                    /// <param name="e">Аргументы события</param>
                    public void ZCB_SelectedIndexChanged(object sender, EventArgs e)
                    {
                        if (PTNode.WP.Scr.isChanging == true)
                            return;
                        PTNode.WP.Scr.isChanging = true;

                        int zSelected = ZCB.SelectedIndex;
                        StrategyCB.SelectedIndex = StrategyM[zSelected];
                        if (bPszNeedNextExperimentsCB != null)
                            bPszNeedNextExperimentsCB.Checked = bPszNeedNextExperiments[zSelected];
                        reWriteValues();

                        PTNode.WP.Scr.isChanging = false;
                    }
                    /// <summary>
                    /// Действие при изменении стратегии для текущего исхода эксперимента
                    /// </summary>
                    /// <param name="sender">Объект пункта</param>
                    /// <param name="e">Аргументы события</param>
                    public void StrategyCB_SelectedIndexChanged(object sender, EventArgs e)
                    {
                        if (PTNode.WP.Scr.isChanging == true)
                            return;
                        PTNode.WP.Scr.isChanging = true;

                        int curz = ZCB.SelectedIndex;
                        ExpTreeNode ETN = (ExpTreeNode)PTNode;

                        StrategyM[curz] = StrategyCB.SelectedIndex;
                        if (bPszNeedNextExperimentsCB != null)
                        {
                            if (bPszNeedNextExperiments[curz])
                                ((ExpTreeNode)ETN.Nodes[curz]).TNP.notUsing();
                            bPszNeedNextExperiments[curz] = false;
                            bPszNeedNextExperimentsCB.Checked = false;
                        }

                        ETN.formTResultModel();

                        PTNode.WP.Scr.isChanging = false;
                    }
                    /// <summary>
                    /// Действие при изменении решения о возможности проведения следующего эксперимента для текущего исхода эксперимента
                    /// </summary>
                    /// <param name="sender">Объект пункта</param>
                    /// <param name="e">Аргументы события</param>
                    public void bPszNeedNextExperimentsCB_CheckedChanged(object sender, EventArgs e)
                    {
                        if (PTNode.WP.Scr.isChanging == true)
                            return;
                        PTNode.WP.Scr.isChanging = true;

                        int curz = ZCB.SelectedIndex;
                        bPszNeedNextExperiments[curz] = bPszNeedNextExperimentsCB.Checked;
                        ExpTreeNode ETN = (ExpTreeNode)PTNode, ETNNode = (ExpTreeNode)ETN.Nodes[curz];
                        if (bPszNeedNextExperiments[curz])
                            new ExpPanel(ETNNode);
                        else
                            ((ExpPanel)ETNNode.TNP).notUsing();
                        ETN.formTResultModel();

                        PTNode.WP.Scr.isChanging = false;
                    }
                }
                /// <summary>
                /// Объект сценария "Дерево решений"
                /// </summary>
                public XScript_DecisionTree Scr;
                //-------------------------
                /// <summary>
                /// Обозначение шага 3
                /// </summary>
                public Label Step3Label;
                //-------------------------
                //public TNEPcontainer TNEPc;
                /// <summary>
                /// Узел дерева для выбора критерия
                /// </summary>
                public CrtTreeNode CrTN;
                /// <summary>
                /// Узел дерева для игры без эксперимента
                /// </summary>
                public NoExpTreeNode NETN;
                /// <summary>
                /// Узел дерева для начального эксперимента
                /// </summary>
                public ExpTreeNode E0TN;
                //-------------------------
                /// <summary>
                /// Вкладка для рабочей области пользователя
                /// </summary>
                public TabPage DPage;
                /// <summary>
                /// Текстовая область вывода развёрнутого ответа
                /// </summary>
                public TextBox TPageTB;
                /// <summary>
                /// Вкладка для вывода развёрнутого ответа
                /// </summary>
                public TabPage TPage;
                /// <summary>
                /// Кнопка прерывания решения
                /// </summary>
                public Button InterruptButton;
                /// <summary>
                /// Кнопка завершения поиска решения
                /// </summary>
                public Button FinishButton;
                /// <summary>
                /// Область вывода содержимого вкладок
                /// </summary>
                public TabControl MainTC;
                //-------------------------
                /// <summary>
                /// Область навигации по дереву решений
                /// </summary>
                public TreeView DecisionTV;
                //-------------------------
                //public Panel CurPanel;
                /// <summary>
                /// Конструктор
                /// </summary>
                /// <param name="_Scr">Объект сценария "Дерево решений"</param>
                public WorkPanel(XScript_DecisionTree _Scr)
                {//InputInfo.ExpInputInfo
                    Scr = _Scr;
                    AutoScroll = true;
                    Location = new Point(0, 0);
                    Size = Scr.MainPanel.Size;
                    Form1 F1 = Scr.PnlC.F;
                    int xpos = 0, ypos = 0;
                    Step3Label = NewLabel("Шаг 3. Решение задачи", xpos, ypos, true, this);
                    ypos++;
                    InterruptButton = NewButton("Прервать", xpos, ypos, 1, 1, InterruptButton_Click, true, this);
                    F1.ControlText(InterruptButton, "Вернуться к шагу ввода данных.");
                    xpos++;
                    FinishButton = NewButton("Завершить поиск решения", xpos, ypos, 2, 1, FinishButton_Click, true, this);
                    F1.ControlText(FinishButton, "Перейти к шагу сохранения данных.");
                    xpos += 2;
                    DecisionTV = NewTreeView(null, xpos, ypos, 4, 3, true, this);
                    xpos = 0; ypos += 3;
                    
                    MainTC = NewTabControl(null, xpos, ypos, 7, 13, true, this);
                    DPage = NewTabPage("Принятие решений", null, MainTC, true);
                    TPage = NewTabPage("Текстовое описание решения", null, MainTC, true);
                    TPageTB = NewTextBox(null, 0, 0, 6, 10, true, true, null, TPage);
                    TPageTB.Font = new Font(TPageTB.Font.FontFamily, TPageTB.Font.SizeInPoints + 4);
                    
                    new CrtTreeNode(this);
                    new NoExpTreeNode(this);
                    if (Scr.UsingAP)
                        new ExpTreeNode(this, null);
                    else
                        E0TN = null;
                    CrTN.TNP.Visible = true;
                    CrTN.formTResultModel();

                    Scr.WPanel = this;
                    Scr.MainPanel.Controls.Add(this);
                    DecisionTV.SelectedNode = CrTN;
                    DecisionTV.BeforeSelect += DecisionTV_BeforeSelect;
                    DecisionTV.AfterSelect += DecisionTV_AfterSelect;
                }
                /// <summary>
                /// Удаление панели ввода
                /// </summary>
                /// <returns>Если успешно</returns>
                public bool unlink()
                {
                    if (Scr.WPanel != this)
                        return false;
                    Scr.WPanel = null;
                    Scr.MainPanel.Controls.Remove(this);
                    return true;
                }
                /// <summary>
                /// Вычисление результата для эксперимента
                /// </summary>
                /// <param name="zCntM">Массив количеств исходов для каждого эксперимента</param>
                /// <param name="_uDT">Пользовательское дерево решений</param>
                /// <param name="_Prnt">Родительский элемент результата предыдущего эксперимента</param>
                /// <param name="_ETN">Элемент дерева решений</param>
                /// <returns>Резульат эсперимента</returns>
                private userDecisionTree.EResult fUDTN(
                    int[] _zCntM,
                    userDecisionTree _uDT,
                    userDecisionTree.EResult _Prnt,
                    ExpTreeNode _ETN
                    )
                {
                    ExpPanel P = (ExpPanel)_ETN.TNP;
                    int zCnt = _zCntM[_ETN.expNum];
                    bool isNotLastExp = (_ETN.expNum != (_zCntM.Length - 1));
                    if (!P.CostDoCurExperimentCB.Checked)
                        return new userDecisionTree.EResult(
                            _uDT,
                            _Prnt,
                            userDecisionTree.eDecision.noCost,
                            zCnt,
                            isNotLastExp);

                    userDecisionTree.EResult R = new userDecisionTree.EResult(
                        _uDT,
                        _Prnt,
                        userDecisionTree.eDecision.normal,
                        zCnt,
                        isNotLastExp);
                    P.StrategyM.CopyTo(R.E_decisions, 0);

                    if (isNotLastExp)
                    {
                        int zCnt2 = _zCntM[_ETN.expNum + 1];
                        bool nextIsNotLastExp = ((_ETN.expNum + 1) != (_zCntM.Length - 1));
                        TreeNodeCollection TNC = _ETN.Nodes;
                        for (int i = 0; i < zCnt; i++)
                        {
                            if (!P.bPszNeedNextExperiments[i])
                                R.Childs[i] = new userDecisionTree.EResult(
                                    _uDT,
                                    R,
                                    userDecisionTree.eDecision.noB,
                                    zCnt2,
                                    nextIsNotLastExp
                                );
                            else
                                R.Childs[i] = fUDTN(_zCntM, _uDT, R, (ExpTreeNode)(TNC[i]));
                        }
                    }
                    return R;
                }
                /// <summary>
                /// Формировние пользовательского дерева решений
                /// </summary>
                /// <returns>Пользовательское дерево решений</returns>
                public userDecisionTree formUserDecisionTree()
                {
                    userDecisionTree R = new userDecisionTree();

                    CrtPanel CrP = ((CrtPanel)CrTN.TNP);
                    R.C_criterion = CrP.CriteriaCB.SelectedIndex;
                    R.C_interpretation = CrP.CriteriaRationalCB.SelectedIndex;

                    NoExpPanel NEP = ((NoExpPanel)NETN.TNP);
                    R.N_decision = NEP.StrategyCB.SelectedIndex;

                    InputInfo.ExpInputInfo[] EIMt = Scr.II.EIM;
                    int expCnt = (EIMt == null) ? 0 : EIMt.Length;
                    if (expCnt != 0)
                        if (!NEP.bNeedNextExperimentCB.Checked)
                            R.Er = new userDecisionTree.EResult(
                                R,
                                null,
                                userDecisionTree.eDecision.noB,
                                EIMt[0].Pzs.GetLength(0),
                                expCnt != 1
                                );
                        else
                        {
                            int[] zCntM = new int[EIMt.Length];
                            for (int i = 0; i < EIMt.Length; i++)
                                zCntM[i] = EIMt[i].Pzs.GetLength(0);
                            R.Er = fUDTN(zCntM, R, null, E0TN);
                        }
                    return R;
                }
                /// <summary>
                /// Действие при прерывании поиска решения
                /// </summary>
                /// <param name="sender">Объект кнопки</param>
                /// <param name="e">Аргументы события</param>
                private void InterruptButton_Click(object sender, EventArgs e)
                {
                    DialogResult res = MessageBox.Show("Результаты вычислений будут сброшены.", "Прервать?", MessageBoxButtons.YesNo);
                    if (res == DialogResult.Yes)
                    {
                        Visible = false;
                        Scr.DIPanel.Visible = true;
                        unlink();
                    }
                }
                /// <summary>
                /// Действие при заверешении поиска решения
                /// </summary>
                /// <param name="sender">Объект кнопки</param>
                /// <param name="e">Аргументы события</param>
                private void FinishButton_Click(object sender, EventArgs e)
                {
                    Scr.DOPanel = new DataOutPanel(Scr);
                    Visible = false;
                    Scr.DOPanel.Visible = true;
                }
                /// <summary>
                /// Обработка предыдущего элемента дерева
                /// </summary>
                /// <param name="sender">Объект дерева навигации</param>
                /// <param name="e">Аргументы события</param>
                private void DecisionTV_BeforeSelect(object sender, TreeViewCancelEventArgs e)
                {
                    if (Scr.isChanging == true)
                        return;
                    Scr.isChanging = true;

                    PnlTreeNode CurNode = (PnlTreeNode)DecisionTV.SelectedNode;
                    if (CurNode.TNP != null)
                        CurNode.TNP.Visible = false;
                    Scr.isChanging = false;
                }
                /// <summary>
                /// Обработка следующего элемента дерева
                /// </summary>
                /// <param name="sender">Объект дерева навигации</param>
                /// <param name="e">Аргументы события</param>
                private void DecisionTV_AfterSelect(object sender, TreeViewEventArgs e)
                {
                    if (Scr.isChanging == true)
                        return;
                    Scr.isChanging = true;

                    PnlTreeNode CurNode = (PnlTreeNode)DecisionTV.SelectedNode;
                    if (CurNode.TNP != null)
                        CurNode.TNP.Visible = true;
                    CurNode.formTResultModel();

                    Scr.isChanging = false;
                }
                /// <summary>
                /// Удаление решений по экспериментам
                /// </summary>
                public void removeAllExperiments()
                {
                    if (E0TN != null)//Если есть элементы экспериментов (аналогично Scr.UsingAP)
                    {
                        if ((DecisionTV.SelectedNode != CrTN) && (DecisionTV.SelectedNode != NETN))
                            DecisionTV.SelectedNode = CrTN;
                        RemPanels(E0TN);
                        E0TN.Nodes.Clear();
                        E0TN.formText();
                    }
                }
                /// <summary>
                /// Удаление панелей
                /// </summary>
                /// <param name="_N">Элемент дерева, с которого происходит удаление</param>
                public TNPanel RemPanels(ExpTreeNode _N)
                {
                    if (_N == null)
                        return null;
                    TNPanel R = _N.TNP;
                    if (R != null)
                    {
                        DPage.Controls.Remove(R);
                        _N.TNP = null;
                    }
                    for (int i = 0; i < _N.Nodes.Count; i++)
                        RemPanels((ExpTreeNode)_N.Nodes[i]);
                    return R;
                }
                //public void removeExpChildsArray(ExpPanel _EP)
                //{
                //    if (_EP != null)
                //    {
                //        TreeNode TN = TNEPc.Find(_EP);
                //        TN.Nodes.Clear();
                //        //DecisionTV
                //        if (_EP.ChldM != null)
                //            TNEPc.RemoveArray(_EP.ChldM);
                //        remExpTreeArray(_EP.ChldM);
                //        _EP.ChldM = null;
                //    }
                //}
                //public void removeExpChild(ExpPanel _EP, int _p)
                //{
                //    if (_EP != null)
                //    {
                //        ExpPanel EPp = _EP.ChldM[_p];
                //        TreeNode TN = TNEPc.Find(_EP), TNp = TNEPc.Find(EPp);
                //        TN.Nodes.Remove(TNp);
                //        _EP.ChldM[_p] = null;
                //        //DecisionTV
                //        if (_EP.ChldM != null)
                //            TNEPc.RemoveArray(_EP.ChldM);
                //        remExpTreeArray(_EP.ChldM);
                //    }
                //}
                //public void remExpTreeArray(ExpPanel[] _EPM)
                //{
                //    if (_EPM != null)
                //    {
                //        TNEPc.RemoveArray(_EPM);
                //        for (int i = 0; i < _EPM.Length; i++)
                //        {
                //            DPage.Controls.Remove(_EPM[i]);
                //            remExpTreeArray(_EPM[i].ChldM);
                //        }

                //        foreach (ExpPanel EPi in _EP.ChldM)
                //            remExpTreeArray(EPi);
                //    }
                //}
            }
            /// <summary>
            /// Класс панели вывода данных
            /// </summary>
            public class DataOutPanel : Panel
            {
                /// <summary>
                /// Объект сценария "Дерево решений"
                /// </summary>
                public XScript_DecisionTree Scr;

                /// <summary>
                /// Пользоватльское дерево решений
                /// </summary>
                public userDecisionTree uDT;

                /// <summary>
                /// Обозначение шага 4
                /// </summary>
                public Label Step4Label;

                /// <summary>
                /// Кнопка возврата к процессу решения задачи (рабочей панели)
                /// </summary>
                public Button BackButton;
                /// <summary>
                /// Кнопка сохранения результата в терминах модели
                /// </summary>
                public Button SaveButton;
                /// <summary>
                /// Кнопка выхода из сценария
                /// </summary>
                public Button EndButton;

                /// <summary>
                /// Текстовая область описания
                /// </summary>
                public TextBox AnswerTB;

                /// <summary>
                /// Конструктор
                /// </summary>
                /// <param name="_Scr">Объект сценария "Дерево решений"</param>
                public DataOutPanel(XScript_DecisionTree _Scr)
                {
                    Scr = _Scr;
                    uDT = Scr.WPanel.formUserDecisionTree();
                    AutoScroll = true;
                    Location = new Point(0, 0);
                    Size = Scr.MainPanel.Size;
                    Form1 F1 = Scr.PnlC.F;
                    int xpos = 0, ypos = 0, txpos = xpos + 2, typos = ypos + 1;
                    Step4Label = NewLabel("Шаг 4. Сохранение реультатов", xpos, ypos, true, this);
                    ypos++;

                    BackButton = NewButton("Прервать", xpos, ypos, 2, 1, BackButton_Click, true, this);
                    F1.ControlText(BackButton, "Вернуться к шагу построения дерева решений.");
                    ypos++;
                    SaveButton = NewButton("Сохранить результаты", xpos, ypos, 2, 1, SaveButton_AnswerModel_Click, true, this);
                    ypos++;
                    EndButton = NewButton("Выйти из сценария", xpos, ypos, 2, 1, EndButton_Click, true, this);
                    F1.ControlText(EndButton, "Перейти в окно настроек сценария, несохранённые данные будут сброшены.");
                    ypos++;

                    AnswerTB = NewTextBox(null, txpos, typos, 5, 16, true, true, null, this);
                    AnswerTB.Font = new Font(AnswerTB.Font.FontFamily, AnswerTB.Font.SizeInPoints + 4);

                    formAnswer();

                    Scr.DOPanel = this;
                    Scr.MainPanel.Controls.Add(this);
                }
                /// <summary>
                /// Формирование части ответа для эксперимента
                /// </summary>
                /// <param name="_Er">Объект описания решения для эксперимента</param>
                /// <param name="_ETN">Узел дерева экспериментов для эксперимента</param>
                /// <param name="_sb">Объект формирования строки ответа</param>
                /// <returns>Успешность операции</returns>
                public bool formAnswerPart(
                    userDecisionTree.EResult _Er,
                    WorkPanel.ExpTreeNode _ETN,
                    StringBuilder _sb
                    )
                {
                    if ((_Er == null) || (_sb == null))
                        return false;
                    WorkPanel.ExpPanel EPnl = (WorkPanel.ExpPanel)_ETN.TNP;

                    XScript_DecisionTree XScr = _ETN.WP.Scr;
                    int eNum = _ETN.expNum;
                    string
                        strNl = StringsTemplateGenerator.newLine,
                        strNT = StringsTemplateGenerator.tabN(eNum),
                        strNTp1 = StringsTemplateGenerator.tabN(eNum + 1);

                    int dec = (_Er.experimentDecision == userDecisionTree.eDecision.noB) ? 1 :
                        (_Er.experimentDecision == userDecisionTree.eDecision.noCost) ? 2 :
                        0;
                    _sb.Append(strNT);
                    _sb.Append(StringsTemplateGenerator.generateTemplateTextExperimentDecision(eNum, dec));
                    _sb.Append(strNl);

                    if (dec == 0)
                    {
                        double expC = XScr.II.EIM[eNum].c;
                        ExpResultData ERDt = EPnl.ERD;
                        double[,] MLst = ERDt.MLs;
                        int
                            zcnt = MLst.GetLength(0),
                            scnt = MLst.GetLength(1);

                        _sb.Append(strNT);
                        _sb.Append(StringsTemplateGenerator.generateTemplateTextExperiment(_ETN.expNum, XScr.loss, ERDt.Oop - expC));
                        _sb.Append(strNl);

                        for (int i = 0; i < _Er.E_decisions.Length; i++)
                        {
                            _sb.Append(strNTp1);
                            _sb.Append(StringsTemplateGenerator.generateTemplateTextExperimentStrategy(
                                eNum, i, _Er.E_decisions[i], XScr.loss, ERDt.op[i] - expC));
                            _sb.Append(strNl);
                            if (_Er.Childs != null)
                                formAnswerPart(_Er.Childs[i], (WorkPanel.ExpTreeNode)_ETN.Nodes[i], _sb);
                        }
                    }

                    return true;
                }
                /// <summary>
                /// Формирование полного текстового ответа
                /// </summary>
                /// <returns>Признак успешности</returns>
                public bool formAnswer()
                {
                    StringBuilder sb = new StringBuilder();
                    WorkPanel WPt = Scr.WPanel;

                    WorkPanel.CrtPanel CrPnl = ((WorkPanel.CrtPanel)WPt.CrTN.TNP);
                    sb.Append(StringsTemplateGenerator.generateTemplateTextCriterion(
                        uDT.C_criterion,
                        uDT.C_interpretation,
                        Scr.loss,
                        CrPnl.ParamValue
                        ));
                    sb.Append(StringsTemplateGenerator.newLine);

                    WorkPanel.NoExpPanel NEPnl = ((WorkPanel.NoExpPanel)WPt.NETN.TNP);
                    double wval = NEPnl.NERD.ML[NEPnl.StrategyCB.SelectedIndex];
                    sb.Append(StringsTemplateGenerator.generateTemplateTextNoExperiment(
                        uDT.N_decision,
                        Scr.loss,
                        wval,
                        Methods.NeedP(CrPnl.CurrentCriteria))
                        );
                    sb.Append(StringsTemplateGenerator.newLine);
                    //Далее - если имеются эксперименты
                    if ((Scr.II.EIM != null) && (Scr.II.EIM.Length != 0))
                        formAnswerPart(uDT.Er, WPt.E0TN, sb);

                    AnswerTB.Text = sb.ToString();
                    return true;
                }
                /// <summary>
                /// Удаление панели ввода
                /// </summary>
                /// <returns>Если успешно</returns>
                public bool unlink()
                {
                    if (Scr.DOPanel != this)
                        return false;
                    Scr.DOPanel = null;
                    Scr.MainPanel.Controls.Remove(this);
                    return true;
                }
                /// <summary>
                /// Действие при сохранении результатов в терминах модели
                /// </summary>
                /// <param name="sender">Объект кнопки</param>
                /// <param name="e">Аргументы события</param>
                private void SaveButton_AnswerModel_Click(object sender, EventArgs e)
                {
                    System.IO.FileStream FS = null;
                    SaveFileDialog sFD = new SaveFileDialog();
                    sFD.InitialDirectory = Directory.GetCurrentDirectory();
                    sFD.Filter = "Data files (*.dat)|*.dat";
                    sFD.FilterIndex = 1;
                    sFD.RestoreDirectory = true;

                    //Если открыть файл не удалось - ничего не меняется
                    if (sFD.ShowDialog() != DialogResult.OK)
                        return;
                    try
                    {
                        FS = File.OpenWrite(sFD.FileName);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Не удалось создать файл: " + ex.Message);
                    }

                    using (FS)
                    {
                        using (BinaryWriter FileBW = new BinaryWriter(FS))
                        {
                            //Общие настройки
                            FileBW.Write(scrNum);//Сценарий дерева решений
                            FileBW.Write(Scr.DIPanel.DescriptionTB.Text);//Текстовое описание задачи
                            FileBW.Write(Scr.loss);//Используются потери
                            FileBW.Write(Scr.UsingAP);//Использовать априорные вероятности
                            FileBW.Write(Scr.UsingB);//Использовать области продолжения
                            //Решения, принимаемые пользователем
                            FileBW.Write(Scr.UD_Criteria);//Выбор принципа оптимальности
                            FileBW.Write(Scr.UD_CriteriaRationale);//Обоснование принципа оптимальности
                            if (Scr.UsingAP)
                                FileBW.Write(Scr.UD_Expediency);//Решение о целесообразности эксперимента
                            FileBW.Write(Scr.UD_Decision);//Выбор стратегии
                            FileBW.Write(Scr.UD_AnswerModel);//Формирование ответа в терминах модели
                            FileBW.Write(Scr.UD_AnswerTask);//Формирование ответа в терминах задачи
                            
                            //Исходные данные

                            // - Матрица платежей
                            double[,] Lt = Scr.II.NEI.L;
                            FileBW.Write(Lt.GetLength(0));// - Количество стратегий
                            FileBW.Write(Lt.GetLength(1));// - Количество состояний природы
                            for (int i = 0; i < Lt.GetLength(0); i++)
                                for (int j = 0; j < Lt.GetLength(1); j++)
                                    FileBW.Write(Lt[i, j]);
                            // - Распределение вероятностей
                            double[] Pt = Scr.II.NEI.P;
                            for (int i = 0; i < Pt.Length; i++)
                                FileBW.Write(Pt[i]);
                            // - Область останова
                            double[] bt = Scr.II.NEI.b;
                            for (int i = 0; i < Pt.Length; i++)
                                FileBW.Write(bt[i]);

                            //Эксперименты
                            if (Scr.II.EIM == null)
                                FileBW.Write(0);
                            else
                            {
                                FileBW.Write(Scr.II.EIM.Length);
                                for (int k = 0; k < Scr.II.EIM.Length; k++)
                                {
                                    InputInfo.ExpInputInfo eII = Scr.II.EIM[k];
                                    FileBW.Write(eII.Pzs.GetLength(0));// - Количество исходов эксперимента
                                    // - Распределение вероятностей эксперимента
                                    for (int i = 0; i < eII.Pzs.GetLength(0); i++)
                                        for (int j = 0; j < eII.Pzs.GetLength(1); j++)
                                            FileBW.Write(eII.Pzs[i, j]);
                                    // - Область останова
                                    for (int i = 0; i < eII.b.Length; i++)
                                        FileBW.Write(eII.b[i]);
                                    // - Стоимость эксперимента
                                    FileBW.Write(eII.c);
                                }
                            }
                            //=========================================================

                            //Дерево решений
                            FileBW.Write(true);//Дерево решений присутствует

                            WorkPanel.CrtPanel CrPnl = (WorkPanel.CrtPanel)Scr.WPanel.CrTN.TNP;
                            FileBW.Write(CrPnl.CurrentCriteria);
                            FileBW.Write(CrPnl.CriteriaRationalCB.SelectedIndex);

                            WorkPanel.NoExpPanel NEPnl = (WorkPanel.NoExpPanel)Scr.WPanel.NETN.TNP;
                            FileBW.Write(NEPnl.StrategyCB.SelectedIndex);
                            FileBW.Write(NEPnl.bNeedNextExperimentCB.Checked);

                            WorkPanel.ExpTreeNode E0TN = Scr.WPanel.E0TN;
                            if ((Scr.II.EIM != null) || (Scr.II.EIM.Length != 0))
                            {
                                bool ne = NEPnl.bNeedNextExperimentCB.Checked;
                                FileBW.Write(ne);
                                if (ne)
                                    SaveStep(FileBW, E0TN);
                            }

                            //=========================================================
                            FileBW.Write(true);//Ответ в терминах модели присутствует
                            FileBW.Write(AnswerTB.Text);//Ответ в терминах модели
                        }
                    }
                }
                /// <summary>
                /// Шаг сохранения данных эксперимента
                /// </summary>
                /// <param name="_FileBW">Поток записи данных в файл</param>
                /// <param name="_ETN">Узел эксперимента дерева решений</param>
                public void SaveStep(BinaryWriter _FileBW, WorkPanel.ExpTreeNode _ETN)
                {
                    WorkPanel.ExpPanel EP = (WorkPanel.ExpPanel)_ETN.TNP;
                    _FileBW.Write(EP.CostDoCurExperiment);
                    if (EP.CostDoCurExperiment)
                        for (int i = 0; i < EP.StrategyM.Length; i++)
                        {
                            _FileBW.Write(EP.StrategyM[i]);
                            if (EP.bPszNeedNextExperiments != null)
                            {
                                bool nne = EP.bPszNeedNextExperiments[i];
                                _FileBW.Write(nne);
                                if (nne)
                                    SaveStep(_FileBW, (WorkPanel.ExpTreeNode)_ETN.Nodes[i]);
                            }
                        }
                }

                /// <summary>
                /// Действие при возврате к процессу решения задачи
                /// </summary>
                /// <param name="sender">Объект кнопки</param>
                /// <param name="e">Аргументы события</param>
                private void BackButton_Click(object sender, EventArgs e)
                {
                    Scr.WPanel.Visible = true;
                    Visible = false;
                    unlink();
                }
                /// <summary>
                /// Действие при выходе из сценария
                /// </summary>
                /// <param name="sender">Объект кнопки</param>
                /// <param name="e">Аргументы события</param>
                private void EndButton_Click(object sender, EventArgs e)
                {
                    Scr.endScript();
                }
            }
            /// <summary>
            /// Имя файла примера исходных данных сценария "Дерево решений"
            /// </summary>
            public const string exampleFileName = "example.dat";
            /// <summary>
            /// Входные данные
            /// </summary>
            public InputInfo II;
            /// <summary>
            /// Признак использования потерь (иначе - выигрышей)
            /// </summary>
            public bool loss;
            /// <summary>
            /// Признак отсутствия непосредственной инициализации метода пользователем
            /// </summary>
            public bool isChanging;

            /// <summary>
            /// Использовать априорные вероятности
            /// </summary>
            public bool UsingAP;
            /// <summary>
            /// Использовать области продолжения
            /// </summary>
            public bool UsingB;
            /// <summary>
            /// Выбор принципа оптимальности пользователем (иначе - программой)
            /// </summary>
            public bool UD_Criteria;
            /// <summary>
            /// Обоснование принципа оптимальности пользователем (иначе - программой)
            /// </summary>
            public bool UD_CriteriaRationale;
            /// <summary>
            /// Решение о целесообразности эксперимента пользователем (иначе - программой)
            /// </summary>
            public bool UD_Expediency;
            /// <summary>
            /// Выбор стратегии пользователем (иначе - программой)
            /// </summary>
            public bool UD_Decision;
            /// <summary>
            /// Формирование ответа в терминах модели пользователем (иначе - программой)
            /// </summary>
            public bool UD_AnswerModel;
            /// <summary>
            /// Формирование ответа в терминах задачи пользователем (иначе - программой)
            /// </summary>
            public bool UD_AnswerTask;

            /// <summary>
            /// Объект панели ввода данных
            /// </summary>
            public DataInPanel DIPanel;
            /// <summary>
            /// Объект рабочей панели
            /// </summary>
            public WorkPanel WPanel;
            /// <summary>
            /// Объект вывода данных
            /// </summary>
            public DataOutPanel DOPanel;

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="_F">Окно программы</param>
            /// <param name="_SPanel">Панель настроек сценария</param>
            /// <param name="_ParentPanel">Родительская панель</param>
            public XScript_DecisionTree(PanelC _PnlC) : base(_PnlC)
            {
                SettingsPanel_DecisionTree SPDT = (SettingsPanel_DecisionTree)_PnlC.XSSP;
                //Загрузка параметров
                UsingAP = SPDT.UsingAP_CB.Checked;
                UsingB = SPDT.UsingCA_CB.Checked;
                UD_Criteria = SPDT.UD_CriteriaCB.Checked;
                UD_CriteriaRationale = SPDT.UD_CriteriaRationaleCB.Checked;
                UD_Expediency = SPDT.UD_ExpediencyCB.Checked;
                UD_Decision = SPDT.UD_DecisionCB.Checked;
                UD_AnswerModel = SPDT.UD_AnswerModelCB.Checked;
                UD_AnswerTask = SPDT.UD_AnswerTaskCB.Checked;

                //Принятие и предобработка предварительных данных
                II = new InputInfo();
                II.NEI = new InputInfo.NoExpInputInfo();
                int gd = SPDT.II.NEI.L.GetLength(0), gs = SPDT.II.NEI.L.GetLength(1);
                II.NEI.L = new double[gd, gs];
                for (int vd = 0; vd < gs; vd++)
                    for (int vs = 0; vs < gs; vs++)
                        II.NEI.L[vd, vs] = SPDT.II.NEI.L[vd, vs];
                if (UsingAP)
                {
                    II.NEI.P = new double[gs];
                    if (SPDT.II.NEI.P != null)
                        for (int vs = 0; vs < gs; vs++)
                            II.NEI.P[vs] = SPDT.II.NEI.P[vs];
                    else
                    {
                        double eql = 1.0 / II.NEI.P.Length;
                        for (int vs = 0; vs < gs; vs++)
                            II.NEI.P[vs] = eql;
                    }
                }
                else
                    II.NEI.P = null;
                if (UsingB)
                {
                    II.NEI.b = new double[gs];
                    if (SPDT.II.NEI.b != null)
                        for (int vs = 0; vs < gs; vs++)
                            II.NEI.b[vs] = SPDT.II.NEI.b[vs];
                    else
                        for (int vs = 0; vs < gs; vs++)
                            II.NEI.b[vs] = 0.0;
                }
                else
                    II.NEI.b = null;
                II.EIM = new InputInfo.ExpInputInfo[SPDT.II.EIM.Length];
                for (int exp = 0; exp < SPDT.II.EIM.Length; exp++)
                {
                    II.EIM[exp] = new InputInfo.ExpInputInfo();
                    InputInfo.ExpInputInfo EII = II.EIM[exp], EIIx = SPDT.II.EIM[exp];
                    int gz = EIIx.Pzs.GetLength(0);
                    EII.Pzs = new double[gz, gs];
                    for (int vz = 0; vz < gz; vz++)
                        for (int vs = 0; vs < gs; vs++)
                            EII.Pzs[vz, vs] = EIIx.Pzs[vz, vs];
                    if (UsingB)
                    {
                        EII.b = new double[gs];
                        for (int vs = 0; vs < gs; vs++)
                            EII.b[vs] = EIIx.b[vs];
                    }
                    EII.c = EIIx.c;
                }

                isChanging = true;//На время формирования содержимого

                //Выигрыш или потери
                loss = (SPDT.WinLossCB.SelectedIndex == 1);

                //========
                new DataInPanel(this);
                WPanel = null;
                DOPanel = null;
                //----
                //WorkPanel = NewPanel(new Point(0, 0), MainPanel.Size, false, false, MainPanel);
                //----
                //DataOutPanel = NewPanel(new Point(0, 0), MainPanel.Size, false, false, MainPanel);

                isChanging = false;
            }
            /// <summary>
            /// Подготовка и запуск сценария
            /// </summary>
            public override void beginScript()
            {
                base.beginScript();
            }
            /// <summary>
            /// Завершение работы сценария
            /// </summary>
            public override void endScript()
            {
                base.endScript();
            }
        }

        //==========================================================================================
        /// <summary>
        /// Класс панели раздела "Учебные и справочные материалы"
        /// </summary>
        public class PanelA : Panel
        {
            /// <summary>
            /// Окно программы
            /// </summary>
            public Form1 F;
            /// <summary>
            /// Окно для просмотра теоретического материала
            /// </summary>
            public WebBrowser WB;
            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="_F"></param>
            public PanelA(Form1 _F) : base()
            {
                F = _F;
                Size = F.panel2.Size;
                Location = new Point(0, 0);
                WB = new WebBrowser();
                int lb = 10, rb = 10, ub = 10, db = 10;
                WB.Size = new Size(this.Width - (lb + rb), this.Height - (ub + db));
                WB.Visible = true;
                this.Controls.Add(WB);
                _F.PnlA = this;
                _F.panel2.Controls.Add(_F.PnlA);
            }
            /// <summary>
            /// Перевод панели в исходное состояние
            /// </summary>
            //public void reset()
            //{
            //    //Заполнить при необходимости
            //}

            /// <summary>
            /// Если произошёл выход из раздела
            /// </summary>
            public bool unlink()
            {
                if (F.PnlA != this)
                    return false;
                F.panel2.Controls.Remove(F.PnlA);
                F.PnlA = null;
                return true;
            }
        }
        
        /// <summary>
        /// Панель раздела "Решебник"
        /// </summary>
        public class PanelC : Panel
        {
            /// <summary>
            /// Окно программы
            /// </summary>
            public Form1 F;
            /// <summary>
            /// Исходное положение для панели настроек
            /// </summary>
            public Point SPLocation;
            /// <summary>
            /// Исходный размер для панели настроек
            /// </summary>
            public Size SPSize;
            /// <summary>
            /// Панель шага настроек
            /// </summary>
            public Panel ScriptSP;
            /// <summary>
            /// Название файла для сценария
            /// </summary>
            public string ScriptFileName;
            /// <summary>
            /// Панель настроек сценария, добавляемая в панель шага настроек
            /// </summary>
            public XScript.SettingsPanel XSSP;
            /// <summary>
            /// Сценарий
            /// </summary>
            public XScript XScr;


            /// <summary>
            /// Обозначение шага выбора сценария
            /// </summary>
            public Label Step1Label;
            /// <summary>
            /// Обозначение выбора сценария
            /// </summary>
            public Label ScrCBLabel;
            /// <summary>
            /// Выбор сценария
            /// </summary>
            public ComboBox ScrCB;
            /// <summary>
            /// Вывод имени файла или его отсутствия
            /// </summary>
            public Label SourceNameLabel;

            /// <summary>
            /// Выход в меню
            /// </summary>
            public Button OutButton;
            /// <summary>
            /// Выбор файла
            /// </summary>
            public Button FileButton;
            /// <summary>
            /// Запуск сценария
            /// </summary>
            public Button StartButton;
            /// <summary>
            /// Файловый поток для файла сценария
            /// </summary>
            ///FileStream ScriptFileStream;

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="_F">Окно программы</param>
            public PanelC(Form1 _F) : base()
            {
                F = _F;
                SPLocation = NPos(0, 4);
                SPSize = NSize(8, 12);
                Location = new Point(0, 0);
                Size = F.panel2.Size;
                ScriptSP = NewPanel(new Point(0, 0), Size, true, false, this);
                F.PnlC = this;
                F.panel4.Controls.Add(this);

                ScriptFileName = null;

                Step1Label = NewLabel("Шаг 1: выбор сценария", 0, 0, true, ScriptSP);

                OutButton = NewButton("Отменить", 0, 1, 1, 1, OutButton_Click, true, ScriptSP);
                F.ControlText(OutButton, "Вернуться в главное меню.");
                FileButton = NewButton("Загрузить", 1, 1, 1, 1, FileButton_Click, true, ScriptSP);
                F.ControlText(FileButton, "Загрузить сценарий из файла.");
                StartButton = NewButton("Запустить", 2, 1, 1, 1, StartButton_Click, true, ScriptSP);
                F.ControlText(StartButton, "Перейти к шагу формирования исходных данных.");

                ScrCBLabel = NewLabel("Сценарий:", 0, 2, true, ScriptSP);
                object[] SCArray = new object[] { "Дерево решений" };
                ScrCB = NewComboBox(1, 2, 2, SCArray, 0, ScrCB_SelectedIndexChanged, true, ScriptSP);

                SourceNameLabel = NewLabel("Сценарий задан пользователем.", 0, 3, true, ScriptSP);

                createSettingsPanelByIndex(0, null);
                XScr = null;

                //ScriptFileStream = null;
                this.Visible = true;
            }
            /// <summary>
            /// Если произошёл выход из раздела
            /// </summary>
            public bool unlink()
            {
                if (F.PnlC != this)
                    return false;
                ScriptFileName = null;
                F.panel4.Controls.Remove(F.PnlC);
                F.PnlC = null;
                return true;
            }
            /// <summary>
            /// Создание панели настроек согласно выбранному сценарию
            /// </summary>
            /// <param name="_scriptIndex">Номер сценария</param>
            /// <returns>Панель настроек</returns>
            public bool createSettingsPanelByIndex(int _scriptIndex, FileStream _ScriptFileStream)
            {
                switch (_scriptIndex)
                {
                    case 0:
                        {
                            if (XSSP != null)
                                XSSP.unlink();
                            new XScript_DecisionTree.SettingsPanel_DecisionTree(this, _ScriptFileStream, SPLocation, SPSize);
                            return true;
                        }
                    default:
                        return false;
                }
            }
            /// <summary>
            /// Кнопка выхода из раздела
            /// </summary>
            /// <param name="sender">Объект кнопки</param>
            /// <param name="e">Аргументы раздела</param>
            private void OutButton_Click(object sender, EventArgs e)
            {
                DialogResult res = MessageBox.Show("Введённые данные будут сброшены.", "Выйти?", MessageBoxButtons.YesNo);
                if (res == DialogResult.Yes)
                {
                    unlink();
                    F.SLT.toParent();
                }
            }
            /// <summary>
            /// Создание временного файла с примером данных сценария "Дерево решений" ("temp.txt")
            /// </summary>
            /// <returns>Описание ошибки при возникновении (иначе null)</returns>
            private string createExampleFile()
            {
                FileStream FSt;
                try
                {
                    FSt = File.Create(XScript_DecisionTree.exampleFileName);
                }
                catch (Exception e)
                {
                    return e.ToString();
                }
                using (FSt)
                {
                    using (BinaryWriter FileBW = new BinaryWriter(FSt))
                    {
                        FileBW.Write(XScript_DecisionTree.scrNum);//Сценарий дерева решений
                        string textDesc =
                            @"В задаче описаны 2 возможных стратегии d лица, принимающего решение (ЛПР) и 2 состояния природы s. По умолчанию задана матрица выигрышей/потерь L(d,s), определяющая значение выигрышей/потерь для каждой пары «решение-состояние». Априорные вероятности p определяют вероятности возникновения каждой из возможных состояний природы s.
Для первого эксперимента определена матрица условных вероятностей Pa(z / s), стоимость проведения эксперимента ca, область останова (определена значениями вероятностей, превышение которых соответственно означает уверенность в результате, достаточную для прекращения экспериментов) da.
Для второго эксперимента определена матрица условных вероятностей Pb(z / s), стоимость проведения эксперимента cb, область останова db.
Необходимо сформировать дерево принятия решений на основании исходных данных.";
                        FileBW.Write(textDesc);//Текстовое описание задачи
                        FileBW.Write(false);//Используются потери
                        FileBW.Write(true);//Использовать априорные вероятности
                        FileBW.Write(true);//Использовать области продолжения
                        //Решения, принимаемые пользователем
                        FileBW.Write(true);//Выбор принципа оптимальности
                        FileBW.Write(true);//Обоснование принципа оптимальности
                        FileBW.Write(true);//Решение о целесообразности эксперимента
                        FileBW.Write(true);//Выбор стратегии
                        FileBW.Write(true);//Формирование ответа в терминах модели
                        FileBW.Write(true);//Формирование ответа в терминах задачи
                        //Исходные данные
                        FileBW.Write(2);// - Количество стратегий
                        FileBW.Write(2);// - Количество состояний природы
                        // - Матрица платежей
                        FileBW.Write(-30.0);
                        FileBW.Write(25.0);
                        FileBW.Write(40.0);
                        FileBW.Write(-45.0);
                        // - Распределение вероятностей
                        FileBW.Write(0.4);
                        FileBW.Write(0.6);
                        // - Область останова
                        FileBW.Write(0.5);
                        FileBW.Write(0.5);
                        //Эксперименты
                        FileBW.Write(2);// - Количество экспериментов

                        FileBW.Write(3);// - Количество исходов эксперимента
                        // - Распределение вероятностей эксперимента
                        FileBW.Write(0.6);
                        FileBW.Write(0.1);
                        FileBW.Write(0.2);
                        FileBW.Write(0.2);
                        FileBW.Write(0.2);
                        FileBW.Write(0.7);
                        // - Область останова
                        FileBW.Write(0.8);
                        FileBW.Write(0.8);
                        // - Стоимость эксперимента
                        FileBW.Write(5.0);

                        FileBW.Write(3);// - Количество исходов эксперимента
                        // - Распределение вероятностей эксперимента
                        FileBW.Write(0.8);
                        FileBW.Write(0.1);
                        FileBW.Write(0.1);
                        FileBW.Write(0.3);
                        FileBW.Write(0.1);
                        FileBW.Write(0.6);
                        // - Область останова
                        FileBW.Write(0.8);
                        FileBW.Write(0.8);
                        // - Стоимость эксперимента
                        FileBW.Write(4.0);
                    }
                }
                return null;
            }
            /// <summary>
            /// Загрузка данных из файла
            /// </summary>
            /// <param name="sender">Объект кнопки</param>
            /// <param name="e">Аргументы события</param>
            private void FileButton_Click(object sender, EventArgs e)
            {
                //Создание файла примера
                bool reDoExampleFile = !File.Exists(XScript_DecisionTree.exampleFileName);
                reDoExampleFile |= false; // true - если необходимо заново создать в любом случае
                if (reDoExampleFile)
                    createExampleFile();
                //====================================
                FileStream ScriptFileStream = null;
                OpenFileDialog oFD = new OpenFileDialog();
                oFD.InitialDirectory = Directory.GetCurrentDirectory();
                oFD.Filter = "Data files (*.dat)|*.dat";
                oFD.FilterIndex = 1;
                oFD.RestoreDirectory = true;

                //Если открыть файл не удалось - ничего не меняется
                if (oFD.ShowDialog() != DialogResult.OK)
                    return;

                try
                {
                    //newScriptFileStream = oFD.OpenFile();
                    if ((ScriptFileStream = File.OpenRead(oFD.FileName)) == null)
                    {
                        MessageBox.Show("Не удалось открть файл.");
                        return;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Не удалось открть файл: " + ex.Message);
                }

                ScriptFileName = oFD.FileName;
                SourceNameLabel.Text = "Сценарий загружен из файла: " + Path.GetFileName(ScriptFileName);

                BinaryReader FileBR = new BinaryReader(ScriptFileStream);
                int typeOfScript;
                using (FileBR)
                {
                    typeOfScript = FileBR.ReadInt32();
                    createSettingsPanelByIndex(typeOfScript, ScriptFileStream);
                }
                ScriptFileStream.Close();
                ScriptFileStream = null;
            }
            /// <summary>
            /// Кнопка начала работы
            /// </summary>
            /// <param name="sender">Объект кнопки</param>
            /// <param name="e">Аргументы события</param>
            private void StartButton_Click(object sender, EventArgs e)
            {
                XScr = XSSP.getScript();
                XScr.beginScript();
            }
            /// <summary>
            /// Действие при выборе сценария
            /// </summary>
            /// <param name="sender">Объект списка</param>
            /// <param name="e">Аргументы события</param>
            private void ScrCB_SelectedIndexChanged(object sender, System.EventArgs e)
            {
                //ScriptFileStream = null;
                ScriptFileName = null;
                createSettingsPanelByIndex(ScrCB.SelectedIndex, null);
                SourceNameLabel.Text = "Сценарий задан пользователем.";
            }
        }
        //=====================================================
        /// <summary>
        /// Класс с описанием выбора пользователя
        /// </summary>
        public class userDecisionTree
        {
            /// <summary>
            /// Обозначение решения о необходимости проведения эксперимента
            /// </summary>
            public enum eDecision: byte
            {
                /// <summary>
                /// Эксперимент проводится
                /// </summary>
                normal,
                /// <summary>
                /// Эксперимент не проводится из-за вхождения имеющихся вероятностей в область останова
                /// </summary>
                noB,
                /// <summary>
                /// Эксперимент не проводится из-за высокой стоимости эксперимента
                /// </summary>
                noCost
            };
            /// <summary>
            /// Класс с описанием выбора стратегий пользователя при эксперименте
            /// </summary>
            public class EResult
            {
                /// <summary>
                /// Класс решения
                /// </summary>
                public userDecisionTree uDT;
                /// <summary>
                /// Предыдущий эксперимент (или null)
                /// </summary>
                public EResult Parent;
                /// <summary>
                /// Решение о необходимости проведения эксперимента
                /// </summary>
                public eDecision experimentDecision;
                /// <summary>
                /// Стратегии для каждого исхода эксперимента
                /// </summary>
                public int[] E_decisions;
                /// <summary>
                /// Эксперименты для каждого решения
                /// </summary>
                public EResult[] Childs;
                /// <summary>
                /// Конструктор
                /// </summary>
                /// <param name="_uDT">Объект дерева решений пользователя</param>
                /// <param name="_Parent">Родительский элемент</param>
                /// <param name="_zCount">Количество результатов</param>
                public EResult(userDecisionTree _uDT, EResult _Parent, eDecision _eD, int _zCount, bool _nextExps)
                {
                    uDT = _uDT;
                    Parent = _Parent;
                    experimentDecision = _eD;
                    if ((_eD != eDecision.normal) || (_zCount == 0))
                    {
                        E_decisions = null;
                        Childs = null;
                    }
                    else
                    {
                        E_decisions = new int[_zCount];
                        for (int i = 0; i < _zCount; i++)
                            E_decisions[i] = 0;
                        if (_nextExps)
                        {
                            Childs = new EResult[_zCount];
                            for (int i = 0; i < _zCount; i++)
                                Childs[i] = null;
                        }
                        else
                            Childs = null;
                    }
                }
            }
            /// <summary>
            /// Выбранный критерий
            /// </summary>
            public int C_criterion;
            /// <summary>
            /// Интерпретация критерия
            /// </summary>
            public int C_interpretation;
            /// <summary>
            /// Решение для игры без эксперимента
            /// </summary>
            public int N_decision;
            /// <summary>
            /// Результат для начального эксперимента
            /// </summary>
            public EResult Er;
            /// <summary>
            /// Конструктор
            /// </summary>
            public userDecisionTree()
            {
                C_criterion = 0;
                C_interpretation = 0;
                N_decision = 0;
                Er = null;
            }
        }
        /// <summary>
        /// Класс множества панелей, построенных в виде дерева
        /// </summary>
        public class Panels
        {
            /// <summary>
            /// Количество панелей
            /// </summary>
            public int pcnt;
            /// <summary>
            /// Панель-родитель
            /// </summary>
            public Panel parentPanel;
            /// <summary>
            /// Текущая панель
            /// </summary>
            public int currentPanelIndex;
            /// <summary>
            /// Панели-потомки
            /// </summary>
            public Panel[] Pnls;
            /// <summary>
            /// Окно программы
            /// </summary>
            public Form1 F;
            /// <summary>
            /// Конструктор с указанием количества потомков и родителя
            /// </summary>
            /// <param name="_pcnt">Количество потомков</param>
            /// <param name="_parentPanel">Родитель</param>
            public Panels(int _pcnt, Panel _parentPanel, Form1 _F)
            {
                pcnt = _pcnt;
                parentPanel = _parentPanel;
                currentPanelIndex = 0;
                Pnls = null;
                F = _F;
            }
            /// <summary>
            /// Собрать панели в памяти
            /// </summary>
            public void assembleContent()
            {
                Pnls = new Panel[pcnt];
                for (int i = 0; i < Pnls.Length; i++)
                    Pnls[i] = new Panel();
                currentPanelIndex = 0;
                for (int i = 0; i < Pnls.Length; i++)
                {
                    Pnls[i].Location = new Point(0, 0);
                    Pnls[i].Size = parentPanel.Size;
                    Pnls[i].Visible = false;
                    parentPanel.Controls.Add(Pnls[i]);
                    //Pnls[i].TabIndex = i;
                }
                Pnls[currentPanelIndex].Visible = true;
            }
            /// <summary>
            /// Разобрать панели из памяти
            /// </summary>
            public void disassembleContent()
            {
                parentPanel.Controls.Clear();

                Pnls = null;
                currentPanelIndex = 0;
            }
            /// <summary>
            /// Перейти к панели по порядковому номеру
            /// </summary>
            /// <param name="_p">порядковый номер панели</param>
            public void choosePanel(int _p)
            {
                if ((_p < 0) || (_p >= Pnls.Length))
                    return;
                Pnls[currentPanelIndex].Visible = false;
                currentPanelIndex = _p;
                Pnls[currentPanelIndex].Visible = true;
            }
            /// <summary>
            /// Выбор следующей панели
            /// </summary>
            public void chooseNextPanel()
            {
                if (currentPanelIndex >= (Pnls.Length - 1))
                    return;
                Pnls[currentPanelIndex].Visible = false;
                currentPanelIndex++;
                Pnls[currentPanelIndex].Visible = true;
            }
        }
        /// <summary>
        /// Класс дерева разделов программы
        /// </summary>
        public class SectionLineTree
        {
            /// <summary>
            /// Класс секции
            /// </summary>
            public class Sec
            {
                /// <summary>
                /// Дерево разделов программы
                /// </summary>
                SectionLineTree SLT;
                /// <summary>
                /// Родитель
                /// </summary>
                public Sec Parent = null;
                /// <summary>
                /// Потомки
                /// </summary>
                public Sec[] Childs = null;
                /// <summary>
                /// Текст
                /// </summary>
                public string Stext = null;
                /// <summary>
                /// Панель
                /// </summary>
                public Panel XPanel = null;
                /// <summary>
                /// Граница справа
                /// </summary>
                public int border = 0;
                /// <summary>
                /// Констркутор
                /// </summary>
                /// <param name="_stext">Текст</param>
                /// <param name="_childCount">Количество потомков</param>
                /// <param name="_parent">Предок</param>
                /// <param name="_xpanel">Панель</param>
                /// <param name="_f">Окно программы</param>
                public Sec(string _stext, int _childCount, Sec _parent, Panel _xpanel, SectionLineTree _SLT)
                {
                    Parent = _parent;
                    Stext = _stext;
                    if (_childCount > 0)
                        Childs = new Sec[_childCount];
                    XPanel = _xpanel;
                    SLT = _SLT;
                    if (Parent == null)
                        border = 0;
                    else
                        border = Parent.border;
                    if (SLT.F != null)
                        border += ((int)(Graphics.FromImage(SLT.F.pictureBox1.BackgroundImage).MeasureString(_stext, SLT.F.SLFont).Width) + SLT.F.sld);
                }
            }
            /// <summary>
            /// Окно программы
            /// </summary>
            public Form1 F;
            /// <summary>
            /// Первый узел
            /// </summary>
            public Sec FirstNode;
            /// <summary>
            /// Текущий узел
            /// </summary>
            public Sec CurPoint;
            /// <summary>
            /// Констркутор
            /// </summary>
            /// <param name="_F">Окно программы</param>
            public SectionLineTree(Form1 _F)
            {
                F = _F;
                FirstNode = new Sec("Разделы", 4, FirstNode, _F.panel1, this);
                FirstNode.Childs[0] = new Sec("Учебные и справочные материалы", 0, FirstNode, _F.panel2, this);
                FirstNode.Childs[1] = new Sec("Инструктивные материалы и тренировочные примеры", 0, FirstNode, _F.panel3, this);
                FirstNode.Childs[2] = new Sec("\"Решебник\"", 0, FirstNode, _F.panel4, this);
                FirstNode.Childs[3] = new Sec("Исследование", 0, FirstNode, _F.panel5, this);
                CurPoint = FirstNode;
            }
            /// <summary>
            /// Переход к выбранной секции
            /// </summary>
            /// <param name="_S">Секция</param>
            public void toPoint(Sec _S)
            {
                CurPoint.XPanel.Visible = false;
                CurPoint = _S;
                CurPoint.XPanel.Visible = true;
                F.redrawSectionLine();
            }
            /// <summary>
            /// Переход на уровень выше
            /// </summary>
            public void toParent()
            {
                toPoint(CurPoint.Parent);
            }
            /// <summary>
            /// Переход к подуровню
            /// </summary>
            /// <param name="_i">Панель-потомок</param>
            public void toChild(int _i)
            {
                toPoint(CurPoint.Childs[_i]);
            }
        }
        //==========================================================================================

        /// <summary>
        /// Горизонтальный отступ
        /// </summary>
        public const int blockWidthSpace = 25;
        /// <summary>
        /// Вертикальный отступ
        /// </summary>
        public const int blockHeightSpace = 25;
        /// <summary>
        /// Горизонтальная единица размера
        /// </summary>
        public const int blockWidth = 100;
        /// <summary>
        /// Вертикальная единица размера
        /// </summary>
        public const int blockHeight = 25;
        /// <summary>
        /// Вычисление горизонтальной позиции с учётом формата
        /// </summary>
        /// <param name="_bx">Горизонтальный номер позиции</param>
        /// <returns>Горизонтальная позиция</returns>
        public static int NX(int _bx)
        {
            return Form1.blockWidthSpace + (_bx * Form1.blockWidth);
        }
        /// <summary>
        /// Вычисление вертикальной позиции с учётом формата
        /// </summary>
        /// <param name="_by">Вертикальный номер позиции</param>
        /// <returns>Вертикальная позиция</returns>
        public static int NY(int _by)
        {
            return Form1.blockHeightSpace + (_by * Form1.blockHeightSpace);
        }
        /// <summary>
        /// Вычисление позиции с учётом формата
        /// </summary>
        /// <param name="_bx">Горизонтальный номер позиции</param>
        /// <param name="_by">Вертикальный номер позиции</param>
        /// <returns>Позиция</returns>
        public static Point NPos(int _bx, int _by)
        {
            return new Point(NX(_bx), NY(_by));
        }
        /// <summary>
        /// Вычисление ширины элемента с учётом формата
        /// </summary>
        /// <param name="_bwidth">Номер значения ширины</param>
        /// <returns>Ширина</returns>
        public static int NWidth(int _bwidth)
        {
            return _bwidth * Form1.blockWidth;
        }
        /// <summary>
        /// Вычисление высоты элемента с учётом формата
        /// </summary>
        /// <param name="_bwidth">Номер значения ширины</param>
        /// <returns>Высота</returns>
        public static int NHeight(int _bheight)
        {
            return _bheight * Form1.blockHeight;
        }
        /// <summary>
        /// Вычисление размера с учётом формата
        /// </summary>
        /// <param name="_bwidth">Номер значения ширины</param>
        /// <param name="_bheight">Номер значения ширины</param>
        /// <returns>Размер</returns>
        public static Size NSize(int _bwidth, int _bheight)
        {
            return new Size(NWidth(_bwidth), NHeight(_bheight));
        }

        //=====================================================
        /// <summary>
        /// Привязка всплывающего текста к элементу
        /// </summary>
        /// <param name="_Control">Графический элемент</param>
        /// <param name="_text">Текст</param>
        public void ControlText(Control _Control, string _text)
        {
            toolTip1.SetToolTip(_Control, _text);
        }
        /// <summary>
        /// Создание объекта Panel
        /// </summary>
        /// <param name="_Location">Положение</param>
        /// <param name="_Size">Размер</param>
        /// <param name="_Visible">Видимость</param>
        /// <param name="_AutoScroll">Автоматическая прокрутна содержимого</param>
        /// <param name="_ParentPanel">Родительская панель</param>
        /// <returns>Объект Panel</returns>
        public static Panel NewPanel(Point _Location, Size _Size, bool _Visible, bool _AutoScroll, Panel _ParentPanel)
        {
            Panel R = new Panel();
            R.Location = _Location;
            R.Size = _Size;
            R.Visible = _Visible;
            R.AutoScroll = _AutoScroll;
            if (_ParentPanel != null)
                _ParentPanel.Controls.Add(R);
            return R;
        }
        /// <summary>
        /// Создание объекта Label
        /// </summary>
        /// <param name="_Text">Текст</param>
        /// <param name="_bx">Горизонтальный номер позиции</param>
        /// <param name="_by">Вертикальный номер позиции</param>
        /// <param name="_Visible">Видимость</param>
        /// <param name="_ParentPanel">Родительская панель</param>
        /// <returns>Объект Label</returns>
        public static Label NewLabel(string _Text, int _bx, int _by, bool _Visible, Panel _ParentPanel)
        {
            Label R = new Label();
            R.AutoSize = true;
            R.Location = Form1.NPos(_bx, _by);
            R.Size = new Size(35, 13);
            R.Text = _Text;
            //R.Font = new Font(R.Font.FontFamily, 12);
            R.Visible = _Visible;
            if (_ParentPanel != null)
                _ParentPanel.Controls.Add(R);
            //R.Name = "Label0_0";
            //R.TabIndex = 0;
            return R;
        }
        /// <summary>
        /// Создание объекта ComboBox
        /// </summary>
        /// <param name="_bx">Горизонтальный номер позиции</param>
        /// <param name="_by">Вертикальный номер позиции</param>
        /// <param name="_bwidth">Номер значения ширины</param>
        /// <param name="_ItemsRange">Множество объектов списка</param>
        /// <param name="_SelectedIndex">Номер выбранного элемента</param>
        /// <param name="_SelectedIndexChanged">Действие при выборе другого элемента</param>
        /// <param name="_Visible">Видимость</param>
        /// <param name="_ParentPanel">Родительская панель</param>
        /// <returns>Объект ComboBox</returns>
        public static ComboBox NewComboBox(int _bx, int _by, int _bwidth, Object[] _ItemsRange, int _SelectedIndex, EventHandler _SelectedIndexChanged, bool _Visible, Panel _ParentPanel)
        {
            ComboBox R = new ComboBox();
            R.DropDownWidth = 100;
            R.Items.AddRange(_ItemsRange);
            R.DropDownStyle = ComboBoxStyle.DropDownList;
            R.SelectedIndex = _SelectedIndex;
            R.Location = NPos(_bx, _by);
            R.Size = new Size(Form1.NWidth(_bwidth), 21);
            if (_SelectedIndexChanged != null)
                R.SelectedIndexChanged += _SelectedIndexChanged;
            R.Visible = _Visible;
            if (_ParentPanel != null)
                _ParentPanel.Controls.Add(R);
            //R.Name = "ComboBox0_0";
            //R.TabIndex = 4;
            return R;
        }
        /// <summary>
        /// Создание объекта Button
        /// </summary>
        /// <param name="_Text">Текст</param>
        /// <param name="_bx">Горизонтальный номер позиции</param>
        /// <param name="_by">Вертикальный номер позиции</param>
        /// <param name="_bwidth">Номер значения ширины</param>
        /// <param name="_bheight">Номер значения высоты</param>
        /// <param name="_Click">Действие при нажатии</param>
        /// <param name="_Visible">Видимость</param>
        /// <param name="_ParentPanel">Родительская панель</param>
        /// <returns>Объект Button</returns>
        public static Button NewButton(string _Text, int _bx, int _by, int _bwidth, int _bheight, EventHandler _Click, bool _Visible, Panel _ParentPanel)
        {
            Button R = new Button();
            R.Location = Form1.NPos(_bx, _by);
            R.Size = Form1.NSize(_bwidth, _bheight);
            R.UseVisualStyleBackColor = true;
            R.Text = _Text;
            if (_Click != null)
                R.Click += _Click;
            //R.Font = new Font(R.Font.FontFamily, 12);
            R.Visible = _Visible;
            if (_ParentPanel != null)
                _ParentPanel.Controls.Add(R);
            return R;
        }
        /// <summary>
        /// Создание объекта TabControl
        /// </summary>
        /// <param name="_Controls">Множество вкладок</param>
        /// <param name="_bx">Горизонтальный номер позиции</param>
        /// <param name="_by">Вертикальный номер позиции</param>
        /// <param name="_bwidth">Номер значения ширины</param>
        /// <param name="_bheight">Номер значения высоты</param>
        /// <param name="_Visible">Видимость</param>
        /// <param name="_ParentPanel">Родительская панель</param>
        /// <returns>Объект TabControl</returns>
        public static TabControl NewTabControl(TabPage[] _Controls, int _bx, int _by, int _bwidth, int _bheight, bool _Visible, Panel _ParentPanel)
        {
            TabControl R = new TabControl();
            R.Location = Form1.NPos(_bx, _by);
            R.Size = Form1.NSize(_bwidth, _bheight);
            if (_Controls != null)
            {
                R.Controls.AddRange(_Controls);
                R.SelectedIndex = 0;
            }
            R.Visible = _Visible;
            if (_ParentPanel != null)
                _ParentPanel.Controls.Add(R);
            return R;
        }
        /// <summary>
        /// Создание объекта TabControl
        /// </summary>
        /// <param name="_Text">Текст</param>
        /// <param name="_Controls">Содержимое</param>
        /// <param name="_TabControl">Родительский контейнер</param>
        /// <param name="_Visible">Видимость</param>
        /// <returns>Объект TabControl</returns>
        public static TabPage NewTabPage(string _Text, Control[] _Controls, TabControl _TabControl, bool _Visible)
        {
            TabPage R = new TabPage();
            R.Padding = new Padding(3);
            R.UseVisualStyleBackColor = true;
            R.Text = _Text;
            R.AutoScroll = true;

            //int controlsCount = (_Controls == null) ? 0 : _Controls.Length, maxWidth = 0, maxHeight = 0;
            //for (int i = 0; i < controlsCount; i++)
            //{
            //    int curWidth = _Controls[i].Size.Width + _Controls[i].Location.X, curHeight = _Controls[i].Size.Height + _Controls[i].Location.Y;
            //    if (curWidth > maxWidth)
            //        maxWidth = curWidth;
            //    if (curHeight > maxHeight)
            //        maxHeight = curHeight;
            //}
            //Size newSize = new Size(_TabControl.Size.Width - 8, _TabControl.Size.Height - 26);
            //if (maxWidth > newSize.Width)
            //{
            //    newSize.Width = maxWidth;
            //    R.AutoScroll = true;
            //}
            //if (maxHeight > newSize.Height)
            //{
            //    newSize.Height = maxHeight;
            //    R.AutoScroll = true;
            //}
            //R.Size = newSize;
            if (_Controls != null)
                R.Controls.AddRange(_Controls);
            
            R.Visible = _Visible;
            if (_TabControl != null)
                _TabControl.Controls.Add(R);
            //R.Name = "TabPage0";
            return R;
        }
        /// <summary>
        /// Создание объекта CheckBox
        /// </summary>
        /// <param name="_Text">Текст</param>
        /// <param name="_Checked">Если выбран</param>
        /// <param name="_bx">Горизонтальный номер позиции</param>
        /// <param name="_by">Вертикальный номер позиции</param>
        /// <param name="_CheckedChanged">При изменении значения выбранности</param>
        /// <param name="_Visible">Видимость</param>
        /// <param name="_ParentPanel">Родительская панель</param>
        /// <returns>Объект CheckBox</returns>
        public static CheckBox NewCheckBox(string _Text, bool _Checked, int _bx, int _by, EventHandler _CheckedChanged, bool _Visible, Panel _ParentPanel)
        {
            CheckBox R = new CheckBox();
            R.Text = _Text;
            R.Checked = _Checked;
            if (_CheckedChanged != null)
                R.CheckedChanged += _CheckedChanged;
            R.AutoSize = true;
            R.Location = NPos(_bx, _by);
            //R.Font = new Font(R.Font.FontFamily, 12);
            R.UseVisualStyleBackColor = true;
            R.Visible = _Visible;
            if (_ParentPanel != null)
                _ParentPanel.Controls.Add(R);
            return R;
        }
        /// <summary>
        /// Создание объекта TreeView
        /// </summary>
        /// <param name="_Nodes">Начальные элементы дерева</param>
        /// <param name="_bx">Горизонтальный номер позиции</param>
        /// <param name="_by">Вертикальный номер позиции</param>
        /// <param name="_bwidth">Номер значения ширины</param>
        /// <param name="_bheight">Номер значения высоты</param>
        /// <param name="_Visible">Видимость</param>
        /// <param name="_ParentPanel">одительская панель</param>
        /// <returns>Объект TreeView</returns>
        public static TreeView NewTreeView(TreeNode[] _Nodes, int _bx, int _by, int _bwidth, int _bheight, bool _Visible, Panel _ParentPanel)
        {
            TreeView R = new TreeView();
            R.BeginUpdate();
            R.Location = NPos(_bx, _by);
            R.Size = NSize(_bwidth, _bheight);
            R.BeginUpdate();
            if (_Nodes != null)
                R.Nodes.AddRange(_Nodes);
            R.EndUpdate();
            R.Visible = _Visible;
            if (_ParentPanel != null)
                _ParentPanel.Controls.Add(R);
            R.EndUpdate();
            return R;
        }
        /// <summary>
        /// Создание объекта DataGridView
        /// </summary>
        /// <param name="_RowCount">Количество строк</param>
        /// <param name="_ColumnCount">Количество столбцов</param>
        /// <param name="_bx">Горизонтальный номер позиции</param>
        /// <param name="_by">Вертикальный номер позиции</param>
        /// <param name="_bwidth">Номер значения ширины</param>
        /// <param name="_bheight">Номер значения высоты</param>
        /// <param name="_CellValueChanged">Действие при изменении значения ячейки</param>
        /// <param name="_Visible">Видимость</param>
        /// <param name="_ParentPanel">Родительская панель</param>
        /// <returns>Объект DataGridView</returns>
        public static DataGridView NewDataGridView(int _RowCount, int _ColumnCount, int _bx, int _by, int _bwidth, int _bheight, DataGridViewCellEventHandler _CellValueChanged, bool _Visible, Panel _ParentPanel)
        {
            DataGridView R = new DataGridView();
            R.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            R.Location = NPos(_bx, _by);
            R.AllowUserToAddRows = false;
            R.AllowUserToDeleteRows = false;
            R.RowCount = _RowCount;
            R.ColumnCount = _ColumnCount;
            formClientSizeOfDGV(R, NWidth(_bwidth), NHeight(_bheight));
            if (_CellValueChanged != null)
                R.CellValueChanged += _CellValueChanged;
            else
                R.ReadOnly = true;
            R.Visible = _Visible;
            if (_ParentPanel != null)
                _ParentPanel.Controls.Add(R);
            return R;
        }
        /// <summary>
        /// Создание объекта TextBox
        /// </summary>
        /// <param name="_Text">Текст</param>
        /// <param name="_bx">Горизонтальный номер позиции</param>
        /// <param name="_by">Вертикальный номер позиции</param>
        /// <param name="_bwidth">Номер значения ширины</param>
        /// <param name="_bheight">Номер значения высоты</param>
        /// <param name="_Visible">Видимость</param>
        /// <param name="_Multiline">Множественность строк</param>
        /// <param name="_TextChanged">Действие при изменении текста</param>
        /// <param name="_ParentPanel">Родительская панель</param>
        /// <returns>Объект TextBox</returns>
        public static TextBox NewTextBox(string _Text, int _bx, int _by, int _bwidth, int _bheight, bool _Visible, bool _Multiline, EventHandler _TextChanged, Panel _ParentPanel)
        {
            TextBox R = new TextBox();
            R.ScrollBars = ScrollBars.Vertical;
            R.Multiline = _Multiline;
            R.WordWrap = true;
            R.Text = _Text;
            if (_TextChanged != null)
                R.TextChanged += _TextChanged;
            else
                R.ReadOnly = true;
            R.Visible = _Visible;
            R.Location = NPos(_bx, _by);
            R.Size = NSize(_bwidth, _bheight);
            if (_ParentPanel != null)
                _ParentPanel.Controls.Add(R);
            return R;
        }
        //=====================================================

        /// <summary>
        /// Дерево разделов программы
        /// </summary>
        public SectionLineTree SLT;
        /// <summary>
        /// Шрифт текста для разделов
        /// </summary>
        public Font SLFont;
        /// <summary>
        /// Промежуток между текстами разделов
        /// </summary>
        public int sld;

        //----------------------
        /// <summary>
        /// Панель раздела "Учебные и справочные материалы"
        /// </summary>
        public PanelA PnlA;
        /// <summary>
        /// Панель раздела "Решебник"
        /// </summary>
        public PanelC PnlC;
        //==========================================================================================
        /// <summary>
        /// Констркутор
        /// </summary>
        public Form1()
        {
            InitializeComponent();

            //classA AB = new classB();
            //classA AC = new classC();
            //classB BC = new classC();
            //AB.methodVOO();
            //AC.methodVOO();
            //BC.methodVOO();

            SLFont = new Font("Arial", 14);
            sld = 5;

            Image Ix = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            Graphics GR = Graphics.FromImage(Ix);
            pictureBox1.BackgroundImage = Ix;
            pictureBox1.MouseClick += new MouseEventHandler(LineClick);

            SLT = new SectionLineTree(this);
            
            button1.Text = SLT.FirstNode.Childs[0].Stext;
            toolTip1.SetToolTip(button1, button1.Text);
            button2.Text = SLT.FirstNode.Childs[1].Stext;
            toolTip1.SetToolTip(button2, button2.Text);
            button2.Enabled = false;
            button3.Text = SLT.FirstNode.Childs[2].Stext;
            toolTip1.SetToolTip(button3, button3.Text);
            button4.Text = SLT.FirstNode.Childs[3].Stext;
            toolTip1.SetToolTip(button4, button4.Text);
            button4.Enabled = false;

            //PanelsH = new PanelsHelp(panel2, this);
            //PanelsW = new PanelsWorkout(panel4, this);

            //PanelsH.assembleContent();
            //PanelsW.assembleContent();

            redrawSectionLine();
            //XScr = null;
            //XSSP = null;
            //ScriptSP = NewPanel(new Point(0, 0), panel4.Size, true, false, panel4);
        }
        /// <summary>
        /// Формирование размера таблицы
        /// </summary>
        /// <param name="_DGV">Таблица</param>
        /// <param name="_maxWidth">Наибольшая допустимая ширина</param>
        /// <param name="_maxHeight">Наибольшая допустимая высота</param>
        /// <returns>Признак удачного изменения размера таблицы</returns>
        public static bool formClientSizeOfDGV(DataGridView _DGV, int _maxWidth, int _maxHeight)
        {
            if (_DGV == null)
                return false;

            Size Sx = new Size();

            Sx.Width = _DGV.Columns.GetColumnsWidth(DataGridViewElementStates.None) + _DGV.RowHeadersWidth + 2;
            Sx.Height = _DGV.Rows.GetRowsHeight(DataGridViewElementStates.None) + _DGV.ColumnHeadersHeight + 2;
            
            if (Sx.Width > _maxWidth)
                Sx.Width = _maxWidth;
            if (Sx.Height > _maxHeight)
                Sx.Height = _maxHeight;
            
            _DGV.ClientSize = Sx;
            return true;
        }
        /// <summary>
        /// Обновить линию секций
        /// </summary>
        private void redrawSectionLine()
        {
            Graphics GR = Graphics.FromImage(pictureBox1.BackgroundImage);
            Rectangle Rect = new Rectangle();
            Brush
                BrCur = new SolidBrush(Color.White),
                BrPrv = new SolidBrush(Color.Blue),
                BrText = new SolidBrush(Color.Black);
            Pen Pn = new Pen(Color.Black);
            Rect.Y = 0;
            Rect.Height = (pictureBox1.BackgroundImage.Height - 1);

            if (SLT.CurPoint.Parent == null)
                Rect.X = 0;
            else
                Rect.X = SLT.CurPoint.Parent.border;
            Rect.Width = (pictureBox1.BackgroundImage.Width - 1) - Rect.X;
            GR.FillRectangle(BrCur, Rect);
            GR.DrawRectangle(Pn, Rect);
            GR.DrawString(SLT.CurPoint.Stext, SLFont, BrText, Rect.X, 0);

            SectionLineTree.Sec S0 = SLT.CurPoint.Parent;
            while (S0 != null)
            {
                if (S0.Parent == null)
                    Rect.X = 0;
                else
                    Rect.X = S0.Parent.border;
                Rect.Width = S0.border - Rect.X;
                GR.FillRectangle(BrPrv, Rect);
                GR.DrawRectangle(Pn, Rect);
                GR.DrawString(S0.Stext, SLFont, BrText, Rect.X, 0);
                S0 = S0.Parent;
            }
            //GR.Dispose();
            pictureBox1.Invalidate();
        }
        /// <summary>
        /// Нажатие на линию (переход на один из предыдущих уровней)
        /// </summary>
        /// <param name="sender">Объект аресной линии</param>
        /// <param name="e">Данные о месте нажатия</param>
        private void LineClick(object sender, MouseEventArgs e)
        {
            //textBox1.Lines = new String[] { e.X.ToString(), e.Y.ToString() };
            //Родитель текущей секции
            SectionLineTree.Sec S0 = SLT.CurPoint.Parent;
            //Позиция границы
            int bp;
            if (S0 == null)
                bp = 0;
            else
                bp = S0.border;
            if ((e.X < bp) && (S0 != null))
                do
                {
                    if (S0.Parent == null)
                        bp = 0;
                    else
                        bp = S0.Parent.border;
                    if (e.X >= bp)
                    {
                        SLT.toPoint(S0);
                        break;
                    }
                    S0 = S0.Parent;
                }
                while (S0 != null);
        }
        /// <summary>
        /// Переход к разделу "Учебные и справочные материалы"
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Аргументы события</param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (PnlA == null)
                new PanelA(this);
            SLT.toChild(0);
        }

        /// <summary>
        /// Переход к разделу "Инструктивные материалы и тренировочные примеры"
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Аргументы события</param>
        private void button2_Click(object sender, EventArgs e)
        {
            SLT.toChild(1);
        }

        /// <summary>
        /// Переход к разделу "Решебник"
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Аргументы события</param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (PnlC == null)
                new PanelC(this);
            SLT.toChild(2);
        }

        /// <summary>
        /// Переход к разделу "Исследование"
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Аргументы события</param>
        private void button4_Click(object sender, EventArgs e)
        {
            SLT.toChild(3);
        }
    }

    //public abstract class classA
    //{
    //    public classA()
    //    {
    //    }
    //    public virtual int methodVOO()
    //    {
    //        return 1;
    //    }
    //    public int method_OO()
    //    {
    //        return 1;
    //    }
    //}
    //public class classB : classA
    //{
    //    public classB() : base()
    //    {
    //    }
    //    public override int methodVOO()
    //    {
    //        return 10 + base.methodVOO();
    //    }
    //    public new int method_OO()
    //    {
    //        return 10 + base.method_OO();
    //    }
    //}
    //public class classC : classB
    //{
    //    public classC() : base()
    //    {
    //    }
    //    public override int methodVOO()
    //    {
    //        return 100 + base.methodVOO();
    //    }
    //}
}






















//===========================================================================================
/*
            for (SectionLineTree.Sec S0 = this.SecPoint.Parent; S0 != null; S0=S0.Parent)
                wsum += (GR.MeasureString(S0.Stext, this.SLFont).Width + d);
            GR.FillRectangle(new SolidBrush(Color.White), wsum, 0, (this.pictureBox1.Width - 1) - wsum, this.pictureBox1.Height - 1);
            GR.DrawRectangle(new Pen(Color.Black), wsum, 0, (this.pictureBox1.Width - 1) - wsum, this.pictureBox1.Height - 1));
            GR.DrawString("Текст", this.SLFont, BrText, wsum, 0);
            for (SectionLineTree.Sec S0 = this.SecPoint.Parent; S0 != null; S0 = S0.Parent)
                wsum += (GR.MeasureString(S0.Stext, this.SLFont).Width + d);
*/

/*
this.MinimizeBox = false;

XButton button = new XButton();
button.Parent = this;
button.Bounds = new Rectangle(10, 30, 150, 30);
button.ForeColor = Color.White;
button.BackgroundImage = MakeBitmap(Color.Blue, button.Width, button.Height);
button.PressedImage = MakeBitmap(Color.LightBlue, button.Width, button.Height);
button.Text = "click me";
button.Click += new EventHandler(this.XButtonClickClick);
*/

//Bitmap BM = new Bitmap(200, 100);
//Graphics GR = Graphics.FromImage(BM);
//Pen P = new Pen(Color.Gray);
//GR.DrawRectangle(P, 0, 0, button1.Size.Width, button1.Size.Height);

/*
Bitmap MakeBitmap(Color color, int width, int height)
{
    Bitmap bmp = new Bitmap(width, height);
    Graphics g = Graphics.FromImage(bmp);
    g.FillRectangle(new SolidBrush(color), 0, 0, bmp.Width, bmp.Height);
    g.DrawEllipse(new Pen(Color.DarkGray), 3, 3, width - 6, height - 6);
    g.Dispose();

    return bmp;
}
private void XButtonClickClick(object sender, EventArgs e)
{
    this.Text = "Click Count = " + ++this.clickCount;
}
*/

/*
public class XButton : Control
{
    Image backgroundImage, pressedImage;
    bool pressed = false;
    public override Image BackgroundImage
    {
        get
        {
            return this.backgroundImage;
        }
        set
        {
            this.backgroundImage = value;
        }
    }

    // Property for the background image to be drawn behind the button text when
    // the button is pressed.
    public Image PressedImage
    {
        get
        {
            return this.pressedImage;
        }
        set
        {
            this.pressedImage = value;
        }
    }
    protected override void OnMouseDown(MouseEventArgs e)
    {
        this.pressed = true;
        this.Invalidate();
        base.OnMouseDown(e);
    }

    protected override void OnPaint(PaintEventArgs e)
    {
        if (this.pressed && this.pressedImage != null)
            e.Graphics.DrawImage(this.pressedImage, 0, 0);
        else
            e.Graphics.DrawImage(this.backgroundImage, 0, 0);

        if (this.Text.Length > 0)
        {
            SizeF size = e.Graphics.MeasureString(this.Text, this.Font);

            e.Graphics.DrawString(this.Text,
                this.Font,
                new SolidBrush(this.ForeColor),
                (this.ClientSize.Width - size.Width) / 2,
                (this.ClientSize.Height - size.Height) / 2);
        }

        e.Graphics.DrawRectangle(new Pen(Color.Black), 0, 0,
            this.ClientSize.Width - 1, this.ClientSize.Height - 1);

        base.OnPaint(e);
    }
}
*/

//II.EIM[0] = new InputInfo.ExpInputInfo();
//II.EIM[0].c = 0.0;
//II.EIM[0].b = new double[2];
//II.EIM[0].b[0] = 0.0;
//II.EIM[0].b[1] = 1.0;
//II.EIM[0].Pzs = new double[II.NEI.L.GetLength(0) + 1, II.NEI.L.GetLength(1)];
//for (int i = 0; i < II.EIM[0].Pzs.GetLength(0); i++)
//    for (int j = 0; j < II.EIM[0].Pzs.GetLength(1); j++)
//        II.EIM[0].Pzs[i, j] = 1.0 / II.EIM[0].Pzs.GetLength(0);
//II.EIM[1] = new InputInfo.ExpInputInfo();
//II.EIM[1].c = 0.0;
//II.EIM[1].b = new double[2];
//II.EIM[1].b[0] = 0.0;
//II.EIM[1].b[1] = 1.0;
//II.EIM[1].Pzs = new double[II.NEI.L.GetLength(0) + 1, II.NEI.L.GetLength(1)];
//for (int i = 0; i < II.EIM[1].Pzs.GetLength(0); i++)
//    for (int j = 0; j < II.EIM[1].Pzs.GetLength(1); j++)
//        II.EIM[1].Pzs[i, j] = 1.0 / II.EIM[1].Pzs.GetLength(0)

//this.webBrowser2.MinimumSize = new System.Drawing.Size(20, 20);

/*
/// <summary>
/// Класс панелей помощи
/// </summary>
public class PanelsHelp : Panels
{
    /// <summary>
    /// Окно просмотра WEB-страниц
    /// </summary>
    public WebBrowser WB;
    /// <summary>
    /// Конструктор с указанием родителя
    /// </summary>
    /// <param name="_parentPanel"></param>
    public PanelsHelp(Panel _parentPanel, Form1 _F) : base(1, _parentPanel, _F)
    {
        this.WB = null;
    }
    /// <summary>
    /// Собрать панели в памяти
    /// </summary>
    public new void assembleContent()
    {
        base.assembleContent();
        WB = new WebBrowser();

        //for (int i = 0; i < this.Pnls.Length; i++)
        //    this.Pnls[i].SuspendLayout();
        //this.WB.SuspendLayout();


        Pnls[0].Name = "Information";

        WB.Location = new System.Drawing.Point(0, 0);
        WB.Size = Pnls[0].Size;
        WB.Name = "webBrowserForTree";
        WB.TabIndex = 0;

        Pnls[0].Controls.Add(WB);

        //this.WB.ResumeLayout(false);
        //for (int i = 0; i < this.Pnls.Length; i++)
        //    this.Pnls[i].ResumeLayout(false);
    }
    /// <summary>
    /// Разобрать панели из памяти
    /// </summary>
    public new void disassembleContent()
    {
        base.disassembleContent();
        //for (int i = 0; i < this.Pnls.Length; i++)
        //    this.Pnls[i].SuspendLayout();
        //this.WB.SuspendLayout();

        //this.WB.ResumeLayout(false);
        //for (int i = 0; i < this.Pnls.Length; i++)
        //    this.Pnls[i].ResumeLayout(false);

        WB = null;
    }
    /// <summary>
    /// Перейти к панели по порядковому номеру
    /// </summary>
    /// <param name="_p"></param>
    public new void choosePanel(int _p)
    {
        base.choosePanel(_p);
    }
}
/// <summary>
/// Класс панелей тестирования
/// </summary>
public class PanelsWorkout : Panels
{
    public const string txt_r = "Использовать априорные вероятности";
    public const string txt_w = "Использовать априорные вероятности (проверьте критерий)";
    //===============================================
    /// <summary>
    /// Изменение данных вызвано другим методом
    /// </summary>
    public bool isChanging;
    /// <summary>
    /// Данные для обработки
    /// </summary>
    public InputInfo II;
    /// <summary>
    /// Хранение решений пользователя
    /// </summary>
    public userDecisionTree uDT;
    //=================================== Настройка данных
    /// <summary>
    /// Обозначение шага 1
    /// </summary>
    public Label Label0_0;

    /// <summary>
    /// Обозначение количества стратегий ЛПР
    /// </summary>
    public Label Label0_1;
    /// <summary>
    /// Обозначение количества состояний природы
    /// </summary>
    public Label Label0_2;
    /// <summary>
    /// Обозначение количества экспериментов
    /// </summary>
    public Label Label0_3;

    /// <summary>
    /// Выбор количества стратегий ЛПР
    /// </summary>
    public ComboBox ComboBox0_0;
    /// <summary>
    /// Выбор количества состояний природы
    /// </summary>
    public ComboBox ComboBox0_1;
    /// <summary>
    /// Выбор количества экспериментов
    /// </summary>
    public ComboBox ComboBox0_2;

    /// <summary>
    /// Тип значений (выигрыши или потери)
    /// </summary>
    public ComboBox ComboBox0_3;

    /// <summary>
    /// Случайное заполнение
    /// </summary>
    public Button Button0;
    /// <summary>
    /// Загрузить из файла
    /// </summary>
    public Button Button1;
    /// <summary>
    /// Кнопка начала решения
    /// </summary>
    public Button Button2;

    /// <summary>
    /// Множество вкладок для хранения информации о платежах и вероятностях экспериментов
    /// </summary>
    public TabControl TabControl0;

    /// <summary>
    /// Информация для игры без эксперимента
    /// </summary>
    public TabPage TabPage0;

    /// <summary>
    /// Обозначение матрицы платежей
    /// </summary>
    public Label Label0_4;
    /// <summary>
    /// Матрица платежей
    /// </summary>
    public DataGridView DataGridViewL;
    /// <summary>
    /// Выбрано если используются априорные вероятности для игры без эксперимента
    /// </summary>
    public CheckBox CheckBox0_1;
    /// <summary>
    /// Обозначение матрицы априорных вероятностей игры без эксперимента
    /// </summary>
    public Label Label0_5;
    /// <summary>
    /// Матрица априорных вероятностей игры без эксперимента
    /// </summary>
    public DataGridView DataGridViewP;
    /// <summary>
    /// Обозначение выбора критерия принятия решений
    /// </summary>
    public Label Label0_6;
    /// <summary>
    /// Выбор критерия принятия решений
    /// </summary>
    public ComboBox ComboBox0_4;

    /// <summary>
    /// Информация для экспериментов
    /// </summary>
    public TabPage[] TabPageM;
    /// <summary>
    /// Обозначение матриц условных вероятностей
    /// </summary>
    public Label[] Label0_MXM;
    /// <summary>
    /// Матрицы условных вероятностей игр с экспериментом
    /// </summary>
    public DataGridView[] DataGridViewMXM;
    /// <summary>
    /// Обозначение области продолжения (нижняя граница)
    /// </summary>
    public Label[] Label0_Mb0;
    /// <summary>
    /// Обозначение области продолжения (верхняя граница)
    /// </summary>
    public Label[] Label0_Mb1;
    /// <summary>
    /// Область продолжения (нижняя граница)
    /// </summary>
    public TrackBar[] TrackBar0_Mb0;
    /// <summary>
    /// Область продолжения (верхняя граница)
    /// </summary>
    public TrackBar[] TrackBar0_Mb1;
    /// <summary>
    /// Отображение области продолжения (нижняя граница)
    /// </summary>
    public TextBox[] TextBox0_Mb0;
    /// <summary>
    /// Отображение области продолжения (верхняя граница)
    /// </summary>
    public TextBox[] TextBox0_Mb1;
    /// <summary>
    /// Обозначение стоимости эксперимента
    /// </summary>
    public Label[] Label0_Mс;
    /// <summary>
    /// Стоимость эксперимента
    /// </summary>
    public NumericUpDown[] NumericUpDown0_Mс;
    /// <summary>
    /// Обозначение количества исходов эксперимента
    /// </summary>
    public Label[] Label0_Mz;
    /// <summary>
    /// Количество исходов эксперимента
    /// </summary>
    public ComboBox[] ComboBox0_Mz;



    //----------------------------------------------------------------------------------
    /// <summary>
    /// Информация обо всех экспериментах и их разветвлениях
    /// </summary>
    public TreeView TreeView0;
    //public System.Windows.Forms.DataGridView DataGridViewM;
    /// <summary>
    /// Обозначение шага 2
    /// </summary>
    public Label Label1_0;
    /// <summary>
    /// Вернуться к настройке
    /// </summary>
    public Button Button1_0;
    /// <summary>
    /// Завершить
    /// </summary>
    public Button Button1_1;

    /// <summary>
    /// Обозначение матрицы платежей
    /// </summary>
    public Label Label1_1;
    /// <summary>
    /// Матрица платежей
    /// </summary>
    public DataGridView DataGridViewL_1;
    /// <summary>
    /// Обозначение матрицы (одномерной) априорных вероятностей
    /// </summary>
    public Label Label1_2;
    /// <summary>
    /// Матрица (одномерная) априорных вероятностей
    /// </summary>
    public DataGridView DataGridViewP_1;

    public Panel[] Panel1_M;//Всего две панели - для игры без эксперимента и эксперимента

    //==
    /// <summary>
    /// Обозначение матрицы ожидаемых выигрышей
    /// </summary>
    public Label Label1_P0_MLne;
    /// <summary>
    /// Матрица ожидаемых выигрышей
    /// </summary>
    public DataGridView DataGridViewM_P0_MLne;

    /// <summary>
    /// Обозначение решения по критерию
    /// </summary>
    public Label Label1_P0_dnum;

    /// <summary>
    /// Решение о проведении начального эксперимента (0 - проводится, 1 - нет необходимости, 2 - дорог)
    /// </summary>
    public ComboBox ComboBox1_P0_ExpCB;

    //===
    /// <summary>
    /// Обозначение матрицы полных вероятностей
    /// </summary>
    public Label Label1_P1_Pz;
    /// <summary>
    /// Матрица полных вероятностей
    /// </summary>
    public DataGridView DataGridViewM_P1_Pz;

    /// <summary>
    /// Обозначение матрицы апостериорных вероятностей
    /// </summary>
    public Label Label1_P1_Psz;
    /// <summary>
    /// Матрица апостериорных вероятностей
    /// </summary>
    public DataGridView DataGridViewM_P1_Psz;

    /// <summary>
    /// Обозначение матрицы уверенности
    /// </summary>
    public Label Label1_P1_ti;
    /// <summary>
    /// Матрица уверенности
    /// </summary>
    public DataGridView DataGridViewM_P1_ti;

    /// <summary>
    /// Обозначение матрицы ожидаемых выигрышей для каждого результата
    /// </summary>
    public Label Label1_P1_ML;
    /// <summary>
    /// Матрица ожидаемых выигрышей (для каждого результата и стратегии)
    /// </summary>
    public DataGridView DataGridViewM_P1_ML;

    /// <summary>
    /// Обозначение решающей функции
    /// </summary>
    public Label Label1_P1_FR;
    /// <summary>
    /// Решающая функция
    /// </summary>
    public DataGridView DataGridViewM_P1_FR;

    /// <summary>
    /// Обозначение лучших по критерию выигрышей
    /// </summary>
    public Label Label1_P1_op;
    /// <summary>
    /// Лучшие по критерию выигрыши
    /// </summary>
    public DataGridView DataGridViewM_P1_op;

    /// <summary>
    /// Отображение общего ожидаемого выигрыша
    /// </summary>
    public Label Label1_P0_Oop;
    /// <summary>
    /// Отображение максимальной стоимости целесообразного эксперимента
    /// </summary>
    public Label Label1_P0_mexp;
    /// <summary>
    /// Отображение выигрыша от проведения эксперимента
    /// </summary>
    public Label Label1_P0_expv;
    //====

    /// <summary>
    /// Обозначение матрицы ожидаемых выигрышей
    /// </summary>
    //public Label Label1_3;
    /// <summary>
    /// Матрица ожидаемых выигрышей
    /// </summary>
    //public DataGridView DataGridViewM_1;

    /// <summary>
    /// Обозначение Решения
    /// </summary>
    //public Label Label1_4;
    /// <summary>
    /// Выбор лучшего решения
    /// </summary>
    //public ComboBox ComboBox1_0;
    /// <summary>
    /// Обоснование лучшего решения
    /// </summary>
    //public ComboBox ComboBox1_1;

    /// <summary>
    /// Конструктор с указанием родителя
    /// </summary>
    /// <param name="_parentPanel">Родитель</param>
    public PanelsWorkout(Panel _parentPanel, Form1 _F) : base(2, _parentPanel, _F)
    {
        //DecisionTree DT = new DecisionTree();
        //DT.FirstExperiment.

        isChanging = false;
        II = null;
        uDT = null;
        Label0_0 = null;
        Label0_1 = null;
        Label0_2 = null;
        Label0_3 = null;
        ComboBox0_0 = null;
        ComboBox0_1 = null;
        ComboBox0_2 = null;
        ComboBox0_3 = null;
        Button0 = null;
        Button1 = null;
        Button2 = null;
        TabControl0 = null;
        TabPage0 = null;
        Label0_4 = null;
        DataGridViewL = null;
        CheckBox0_1 = null;
        Label0_5 = null;
        DataGridViewP = null;
        Label0_6 = null;
        ComboBox0_4 = null;
        TabPageM = null;
        Label0_MXM = null;
        DataGridViewMXM = null;
        Label0_Mb0 = null;
        Label0_Mb1 = null;
        TrackBar0_Mb0 = null;
        TrackBar0_Mb1 = null;
        TextBox0_Mb0 = null;
        TextBox0_Mb1 = null;
        Label0_Mс = null;
        NumericUpDown0_Mс = null;
        Label0_Mz = null;
        ComboBox0_Mz = null;
        //==============================================
        TreeView0 = null;
        Label1_0 = null;
        Button1_0 = null;
        Button1_1 = null;
        Label1_1 = null;
        DataGridViewL_1 = null;
        Label1_2 = null;
        DataGridViewP_1 = null;
        Panel1_M = null;
        Label1_P0_MLne = null;
        DataGridViewM_P0_MLne = null;
        Label1_P0_dnum = null;
        ComboBox1_P0_ExpCB = null;
        Label1_P1_Pz = null;
        DataGridViewM_P1_Pz = null;
        Label1_P1_Psz = null;
        DataGridViewM_P1_Psz = null;
        Label1_P1_ti = null;
        DataGridViewM_P1_ti = null;
        Label1_P1_ML = null;
        DataGridViewM_P1_ML = null;
        Label1_P1_FR = null;
        DataGridViewM_P1_FR = null;
        Label1_P1_op = null;
        DataGridViewM_P1_op = null;
        Label1_P0_Oop = null;
        Label1_P0_mexp = null;
        Label1_P0_expv = null;
    }
    /// <summary>
    /// Задание случайных начений имеющимся данным (значения полей формы не меняются)
    /// </summary>
    public void randomiseInputInfo()
    {
        Random rndm = new Random();
        for (int i = 0; i < II.NEI.L.GetLength(0); i++)
            for (int j = 0; j < II.NEI.L.GetLength(1); j++)
                II.NEI.L[i, j] = rndm.Next(-100, 100);
        int[] intMasP = new int[II.NEI.P.Length];
        int intMasPSum = 0;
        for (int i = 0; i < II.NEI.P.Length; i++)
        {
            intMasP[i] = rndm.Next(1, 4);
            intMasPSum += intMasP[i];
        }
        for (int i = 0; i < II.NEI.P.Length; i++)
            II.NEI.P[i] = (double)intMasP[i] / intMasPSum;
        for (int k = 0; k < II.EIM.Length; k++)
        {
            int[] intMas = new int[II.EIM[k].Pzs.GetLength(0)];
            int intMasSum;
            for (int j = 0; j < II.EIM[k].Pzs.GetLength(1); j++)
            {
                intMasSum = 0;
                for (int i = 0; i < II.EIM[k].Pzs.GetLength(0); i++)
                {
                    intMas[i] = rndm.Next(1, 4);
                    intMasSum += intMas[i];
                }
                for (int i = 0; i < II.EIM[k].Pzs.GetLength(0); i++)
                    II.EIM[k].Pzs[i, j] = (double)intMas[i] / intMasSum;
            }
            II.EIM[k].b[0] = rndm.Next(1, 4) / 10.0;
            II.EIM[k].b[1] = rndm.Next(6, 9) / 10.0;
            II.EIM[k].c = rndm.Next(1, 4);
        }
    }
    /// <summary>
    /// Координаты для указанных пунктов по горизонтали и вертикали
    /// </summary>
    /// <param name="_bx">Пунк по горизонтали</param>
    /// <param name="_by">Пунк по вертикали</param>
    /// <returns>Координаты</returns>

    public bool isCrAndPCorrect()
    {
        return (!((ComboBox0_4.SelectedIndex >= 6) && (!CheckBox0_1.Checked)));
    }
    /// <summary>
    /// Собрать панели в памяти
    /// </summary>
    public new void assembleContent()
    {
        base.assembleContent();

        isChanging = false;

        II = new InputInfo();
        uDT = new userDecisionTree();
        Label0_0 = new Label();
        Label0_1 = new Label();
        Label0_2 = new Label();
        Label0_3 = new Label();
        ComboBox0_0 = new ComboBox();
        ComboBox0_1 = new ComboBox();
        ComboBox0_2 = new ComboBox();
        Button0 = new Button();
        Button1 = new Button();
        Button2 = new Button();
        ComboBox0_3 = new ComboBox();
        TabControl0 = new TabControl();
        TabPage0 = new TabPage();
        Label0_4 = new Label();
        DataGridViewL = new DataGridView();
        CheckBox0_1 = new CheckBox();
        Label0_5 = new Label();
        DataGridViewP = new DataGridView();
        Label0_6 = new Label();
        ComboBox0_4 = new ComboBox();
        TabPageM = new TabPage[0];
        Label0_MXM = new Label[0];
        DataGridViewMXM = new DataGridView[0];
        Label0_Mb0 = new Label[0];
        Label0_Mb1 = new Label[0];
        TrackBar0_Mb0 = new TrackBar[0];
        TrackBar0_Mb1 = new TrackBar[0];
        TextBox0_Mb0 = new TextBox[0];
        TextBox0_Mb1 = new TextBox[0];
        Label0_Mс = new Label[0];
        NumericUpDown0_Mс = new NumericUpDown[0];
        Label0_Mz = new Label[0];
        ComboBox0_Mz = new ComboBox[0];

        TreeView0 = new TreeView();
        Label1_0 = new Label();
        Button1_0 = new Button();
        Button1_1 = new Button();
        Label1_1 = new Label();
        DataGridViewL_1 = new DataGridView();
        Label1_2 = new Label();
        DataGridViewP_1 = new DataGridView();
        Panel1_M = new Panel[2];
        Label1_P0_MLne = new Label();
        DataGridViewM_P0_MLne = new DataGridView();
        Label1_P0_dnum = new Label();
        ComboBox1_P0_ExpCB = new ComboBox();
        Label1_P1_Pz = new Label();
        DataGridViewM_P1_Pz = new DataGridView();
        Label1_P1_Psz = new Label();
        DataGridViewM_P1_Psz = new DataGridView();
        Label1_P1_ti = new Label();
        DataGridViewM_P1_ti = new DataGridView();
        Label1_P1_ML = new Label();
        DataGridViewM_P1_ML = new DataGridView();
        Label1_P1_FR = new Label();
        DataGridViewM_P1_FR = new DataGridView();
        Label1_P1_op = new Label();
        DataGridViewM_P1_op = new DataGridView();
        Label1_P0_Oop = new Label();
        Label1_P0_mexp = new Label();
        Label1_P0_expv = new Label();

        //======================================================

        II.NEI = new InputInfo.NoExpInputInfo();
        II.NEI.L = new double[2, 2];
        II.NEI.P = new double[II.NEI.L.GetLength(1)];
        for (int i = 0; i < II.NEI.L.GetLength(0); i++)
            for (int j = 0; j < II.NEI.L.GetLength(1); j++)
                II.NEI.L[i, j] = 0.0;
        for (int i = 0; i < II.NEI.P.Length; i++)
            II.NEI.P[i] = 1.0 / II.NEI.P.Length;
        II.EIM = new InputInfo.ExpInputInfo[0];

        uDT.C_criteria = 3;
        //uDT.C_interpretation = 0;//Определить
        uDT.N_decision = 0;
        //uDT.N_interpretation = 0;//Определить
        uDT.Er = null;
        uDT.Er_interpretation = 0;//Определить

        Label0_0.AutoSize = true;
        Label0_0.Location = NPos(0, 0);
        Label0_0.Name = "Label0_0";
        Label0_0.Size = new Size(35, 13);
        //Label0_0.TabIndex = 0;
        Label0_0.Text = "Шаг 1. Ввод исходных данных";

        Label0_1.AutoSize = true;
        Label0_1.Location = NPos(3, 0);
        Label0_1.Name = "Label0_1";
        Label0_1.Size = new Size(35, 13);
        //Label0_1.TabIndex = 1;
        Label0_1.Text = "Количество стратегий ЛПР:";

        Label0_2.AutoSize = true;
        Label0_2.Location = NPos(3, 1);
        Label0_2.Name = "Label0_2";
        Label0_2.Size = new Size(35, 13);
        //Label0_2.TabIndex = 2;
        Label0_2.Text = "Количество состояний природы:";

        Label0_3.AutoSize = true;
        Label0_3.Location = NPos(3, 2);
        Label0_3.Name = "Label0_3";
        Label0_3.Size = new Size(35, 13);
        //Label0_3.TabIndex = 3;
        Label0_3.Text = "Количество экспериментов:";

        ComboBox0_0.DropDownWidth = 100;
        ComboBox0_0.Items.AddRange(new object[] { 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        ComboBox0_0.DropDownStyle = ComboBoxStyle.DropDownList;
        ComboBox0_0.SelectedIndex = 0;
        ComboBox0_0.Location = NPos(5, 0);
        ComboBox0_0.Name = "ComboBox0_0";
        ComboBox0_0.Size = new Size(50, 21);
        //ComboBox0_0.TabIndex = 4;
        ComboBox0_0.SelectedIndexChanged += ComboBox0_0_SelectedIndexChanged;//new System.EventHandler(ComboBox0_0_SelectedIndexChanged)

        ComboBox0_1.DropDownWidth = 100;
        ComboBox0_1.Items.AddRange(new object[] { 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        ComboBox0_1.DropDownStyle = ComboBoxStyle.DropDownList;
        ComboBox0_1.SelectedIndex = 0;
        ComboBox0_1.Location = NPos(5, 1);
        ComboBox0_1.Name = "ComboBox0_1";
        ComboBox0_1.Size = new Size(50, 21);
        //ComboBox0_1.TabIndex = 5;
        ComboBox0_1.SelectedIndexChanged += ComboBox0_1_SelectedIndexChanged;

        ComboBox0_2.DropDownWidth = 100;
        ComboBox0_2.Items.AddRange(new object[] { 0, 1, 2, 3, 4 });
        ComboBox0_2.DropDownStyle = ComboBoxStyle.DropDownList;
        ComboBox0_2.SelectedIndex = 0;
        ComboBox0_2.Location = NPos(5, 2);
        ComboBox0_2.Name = "ComboBox0_2";
        ComboBox0_2.Size = new Size(50, 21);
        //ComboBox0_2.TabIndex = 6;
        ComboBox0_2.SelectedIndexChanged += ComboBox0_2_SelectedIndexChanged;

        Button0.Location = NPos(0, 1);
        Button0.Name = "button0";
        Button0.Size = new Size(100, 25);
        //Button0.TabIndex = 7;
        Button0.UseVisualStyleBackColor = true;
        Button0.Text = "Случайно";
        Button0.Click += Button0Click;
        F.toolTip1.SetToolTip(Button0, "Сгенирировать значения случайно");

        Button1.Location = NPos(0, 2);
        Button1.Name = "Button1";
        Button1.Size = new Size(100, 25);
        //Button1.TabIndex = 8;
        Button1.UseVisualStyleBackColor = true;
        Button1.Text = "Из файла";
        Button1.Click += Button1Click;
        F.toolTip1.SetToolTip(Button1, "Загрузить условия задачи из эксперимента");

        Button2.Location = NPos(1, 1);
        Button2.Name = "Button2";
        Button2.Size = new Size(100, 25);
        //Button2.TabIndex = 9;
        Button2.UseVisualStyleBackColor = true;
        Button2.Text = "Начать";
        Button2.Click += Button2Click;
        F.toolTip1.SetToolTip(Button2, "Приступить к решению эксперимента");


        ComboBox0_3.DropDownWidth = 100;
        ComboBox0_3.Items.AddRange(new object[] { "Выигрыши", "Потери" });
        ComboBox0_3.DropDownStyle = ComboBoxStyle.DropDownList;
        ComboBox0_3.SelectedIndex = 0;
        ComboBox0_3.Location = NPos(1, 2);
        ComboBox0_3.Name = "ComboBox0_3";
        ComboBox0_3.Size = new Size(100, 21);
        //ComboBox0_3.TabIndex = 10;
        ComboBox0_3.SelectedIndexChanged += ComboBox0_3_SelectedIndexChanged;

        TabControl0.Controls.Add(TabPage0);
        TabControl0.Location = NPos(0, 4);
        TabControl0.Name = "TabControl0";
        TabControl0.SelectedIndex = 0;
        TabControl0.Size = new Size(700, 330);
        TabControl0.TabIndex = 11;

        TabPage0.Name = "TabPage0";
        TabPage0.Padding = new Padding(3);
        TabPage0.Size = new Size(TabControl0.Size.Height - 8, TabControl0.Size.Width - 26);
        TabPage0.Text = "Игра без эксперимента";
        TabPage0.UseVisualStyleBackColor = true;
        TabPage0.Controls.AddRange(new Control[] { Label0_4, DataGridViewL, CheckBox0_1, Label0_5, DataGridViewP, Label0_6, ComboBox0_4 });
        TabPage0.AutoScroll = true;

        Label0_4.AutoSize = true;
        Label0_4.Location = NPos(0, 0);
        Label0_4.Name = "Label0_4";
        Label0_4.Size = new Size(35, 13);
        //Label0_4.TabIndex = 0;
        Label0_4.Text = "Платёжная матрица:";

        DataGridViewL.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        DataGridViewL.Location = NPos(0, 1);
        DataGridViewL.Name = "DataGridViewL";
        //DataGridViewL.TabIndex = 1;
        DataGridViewL.AllowUserToAddRows = false;
        DataGridViewL.AllowUserToDeleteRows = false;
        DataGridViewL.RowCount = II.NEI.L.GetLength(0);
        DataGridViewL.ColumnCount = II.NEI.L.GetLength(1);
        for (int i = 0; i < II.NEI.L.GetLength(0); i++)
            for (int j = 0; j < II.NEI.L.GetLength(1); j++)
            {
                DataGridViewL[j, i].ValueType = typeof(double);
                DataGridViewL[j, i].Value = II.NEI.L[i, j];
            }
        Form1.formClientSizeOfDGV(DataGridViewL);
        DataGridViewL.CellValueChanged += this.DataGridViewL_CellValueChanged;//new DataGridViewCellEventHandler()
        //DataGridViewL_CellValueChanged

        CheckBox0_1.AutoSize = true;
        CheckBox0_1.Location = NPos(2, 7);
        CheckBox0_1.Name = "CheckBox0_1";
        CheckBox0_1.Size = new Size(75, 23);
        //CheckBox0_1.TabIndex = 2;
        CheckBox0_1.Text = txt_r;
        CheckBox0_1.UseVisualStyleBackColor = true;
        CheckBox0_1.Checked = false;
        CheckBox0_1.CheckedChanged += CheckBox0_1_CheckedChanged;

        Label0_5.AutoSize = true;
        Label0_5.Location = NPos(0, 7);
        Label0_5.Name = "Label0_5";
        Label0_5.Size = new Size(35, 13);
        //Label0_5.TabIndex = 3;
        Label0_5.Text = "Априорные вероятности:";

        DataGridViewP.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        DataGridViewP.Location = NPos(0, 8);
        DataGridViewP.Name = "DataGridViewP";
        //DataGridViewP.TabIndex = 4;
        DataGridViewP.AllowUserToAddRows = false;
        DataGridViewP.AllowUserToDeleteRows = false;
        DataGridViewP.ColumnCount = II.NEI.P.Length;
        DataGridViewP.RowCount = 1;
        for (int j = 0; j < DataGridViewP.ColumnCount; j++)
        {
            DataGridViewP[j, 0].ValueType = typeof(double);
            DataGridViewP[j, 0].Value = II.NEI.P[j];
        }
        Form1.formClientSizeOfDGV(DataGridViewP);
        DataGridViewP.CellValueChanged += this.DataGridViewP_CellValueChanged;//new DataGridViewCellEventHandler()

        Label0_6.AutoSize = true;
        Label0_6.Location = NPos(0, 6);
        Label0_6.Name = "Label0_6";
        Label0_6.Size = new Size(35, 13);
        //Label0_6.TabIndex = 5;
        Label0_6.Text = "Критерий принятия решений:";

        ComboBox0_4.DropDownWidth = 100;
        ComboBox0_4.Items.AddRange(new object[]
        {
            "Лапласа",          //double[,] _kw
            "МаксиМакса",       //double[,] _kw
            "МаксиМина",        //double[,] _kw
            "Сэвиджа",          //double[,] _kw
            "Произведений",     //double[,] _kw
            "Гурвица",          //double[,] _kw,                double _alpha
            "Байеса-Лапласа",   //double[,] _kw, double[] _P
            "Гермейера",        //double[,] _kw, double[] _P
            "Ходжа-Лемана",     //double[,] _kw, double[] _P,   double _nu
            "БЛ(ММ)"            //double[,] _kw, double[] _P,   double _epsilon
        });
        ComboBox0_4.DropDownStyle = ComboBoxStyle.DropDownList;
        ComboBox0_4.SelectedIndex = 0;
        ComboBox0_4.Location = NPos(2, 6);
        ComboBox0_4.Name = "ComboBox0_4";
        ComboBox0_4.Size = new Size(200, 21);
        //ComboBox0_4.TabIndex = 6;
        ComboBox0_4.SelectedIndexChanged += ComboBox0_4_SelectedIndexChanged;

        Pnls[0].Controls.AddRange(new Control[]
        {
            Label0_0, Label0_1, Label0_2, Label0_3,
            ComboBox0_0, ComboBox0_1, ComboBox0_2,
            Button0, Button1, Button2, ComboBox0_3,
            TabControl0
        });
        //============================================================

        this.TreeView0.Location = NPos(5, 12);
        this.TreeView0.Name = "TreeView0";
        this.TreeView0.Size = new Size(200, 100);
        //this.TreeView0.TabIndex = 0;
        this.TreeView0.BeginUpdate();
        this.TreeView0.Nodes.Add("Игра без эксперимента");
        //this.TreeView0.Nodes.Add("Эксперимент 0");
        //this.TreeView0.Nodes[1].Nodes.Add("Эксперимент 1 (0)");
        //this.TreeView0.Nodes[1].Nodes.Add("Эксперимент 1 (1)");
        //this.TreeView0.Nodes.Add("Эксперимент 0");
        //formTWNode(0, TreeView0.Nodes[1], "");
        this.TreeView0.EndUpdate();


        Label1_0.AutoSize = true;
        Label1_0.Location = NPos(0, 0);
        Label1_0.Name = "Label1_0";
        Label1_0.Size = new Size(35, 13);
        //Label1_0.TabIndex = 0;
        Label1_0.Text = "Шаг 2: Поиск результата";

        Button1_0.Location = NPos(0, 1);
        Button1_0.Name = "Button1_0";
        Button1_0.Size = new Size(100, 25);
        //Button1_0.TabIndex = 1;
        Button1_0.UseVisualStyleBackColor = true;
        Button1_0.Text = "Прервать";

        Button1_1.Location = NPos(1, 1);
        Button1_1.Name = "Button1_1";
        Button1_1.Size = new Size(100, 25);
        //Button1_1.TabIndex = 2;
        Button1_1.UseVisualStyleBackColor = true;
        Button1_1.Text = "Завершить";

        Label1_1.AutoSize = true;
        Label1_1.Location = NPos(0, 2);
        Label1_1.Name = "Label1_1";
        Label1_1.Size = new Size(35, 13);
        //Label1_1.TabIndex = 3;
        Label1_1.Text = "Матрица платежей";

        DataGridViewL_1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        DataGridViewL_1.Location = NPos(0, 3);
        DataGridViewL_1.Name = "DataGridViewL_1";
        //DataGridViewL_1.TabIndex = 4;
        DataGridViewL_1.AllowUserToAddRows = false;
        DataGridViewL_1.AllowUserToDeleteRows = false;
        DataGridViewL_1.RowCount = 2;
        DataGridViewL_1.ColumnCount = 2;
        for (int i = 0; i < II.NEI.L.GetLength(0); i++)
            for (int j = 0; j < II.NEI.L.GetLength(1); j++)
                DataGridViewL_1[j, i].ValueType = typeof(double);
        Form1.formClientSizeOfDGV(DataGridViewL_1);
        DataGridViewL_1[0, 0].Value = 2.0;
        DataGridViewL_1[1, 0].Value = 4.0;
        DataGridViewL_1[0, 1].Value = 6.0;
        DataGridViewL_1[1, 1].Value = -4.0;

        Label1_2.AutoSize = true;
        Label1_2.Location = NPos(0, 9);
        Label1_2.Name = "Label1_2";
        Label1_2.Size = new Size(35, 13);
        //Label1_2.TabIndex = 5;
        Label1_2.Text = "Матрица априорных вероятностей";

        DataGridViewP_1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        DataGridViewP_1.Location = NPos(0, 10);
        DataGridViewP_1.Name = "DataGridViewP_1";
        //DataGridViewP_1.TabIndex = 6;
        DataGridViewP_1.AllowUserToAddRows = false;
        DataGridViewP_1.AllowUserToDeleteRows = false;
        DataGridViewP_1.ColumnCount = 2;
        DataGridViewP_1.RowCount = 1;
        for (int j = 0; j < DataGridViewP.ColumnCount; j++)
        {
            DataGridViewP_1[j, 0].ValueType = typeof(double);
            DataGridViewP_1[j, 0].Value = 0.5;
        }
        Form1.formClientSizeOfDGV(DataGridViewP_1);

        **
        Label1_3.AutoSize = true;
        Label1_3.Location = Pos(5, 2);
        Label1_3.Name = "Label1_3";
        Label1_3.Size = new Size(35, 13);
        //Label1_3.TabIndex = 7;
        Label1_3.Text = "Матрица ожидаемых выигрышей";

        DataGridViewM_1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        DataGridViewM_1.Location = Pos(5, 3);
        DataGridViewM_1.Name = "DataGridViewM_1";
        //DataGridViewM_1.TabIndex = 8;
        DataGridViewM_1.AllowUserToAddRows = false;
        DataGridViewM_1.AllowUserToDeleteRows = false;
        DataGridViewM_1.ColumnCount = 1;
        DataGridViewM_1.RowCount = 2;
        for (int i = 0; i < DataGridViewP.RowCount; i++)
            DataGridViewM_1[0, i].ValueType = typeof(double);
        Form1.formClientSizeOfDGV(DataGridViewM_1);
        DataGridViewM_1[0, 0].Value = 3.0;
        DataGridViewM_1[0, 1].Value = 1.0;

        Label1_4.AutoSize = true;
        Label1_4.Location = Pos(5, 9);
        Label1_4.Name = "Label1_4";
        Label1_4.Size = new Size(35, 13);
        //Label1_4.TabIndex = 9;
        Label1_4.Text = "Решение и обоснование:";

        ComboBox1_0.DropDownWidth = 100;
        ComboBox1_0.Items.AddRange(new object[] { "Вариант 0", "Вариант 1" });
        ComboBox1_0.DropDownStyle = ComboBoxStyle.DropDownList;
        ComboBox1_0.SelectedIndex = 0;
        ComboBox1_0.Location = Pos(5, 10);
        ComboBox1_0.Name = "ComboBox1_0";
        ComboBox1_0.Size = new Size(200, 21);
        //ComboBox1_0.TabIndex = 10;

        ComboBox1_1.DropDownWidth = 100;
        ComboBox1_1.Items.AddRange(new object[] { "Наибольший средний выигрыш", "Иное" });
        ComboBox1_1.DropDownStyle = ComboBoxStyle.DropDownList;
        ComboBox1_1.SelectedIndex = 0;
        ComboBox1_1.Location = Pos(5, 11);
        ComboBox1_1.Name = "ComboBox1_1";
        ComboBox1_1.Size = new Size(200, 21);
        //ComboBox1_1.TabIndex = 11;
        **

        this.Pnls[1].Controls.AddRange(new Control[]
        {
            TreeView0,Label1_0,Button1_0,Button1_1,
            Label1_1,DataGridViewL_1,Label1_2,DataGridViewP_1,
            //Label1_3,DataGridViewM_1,Label1_4,ComboBox1_0,ComboBox1_1
            //,this.DataGridViewL,this.DataGridViewP,this.DataGridViewM
        });
    }
    /// <summary>
    /// Формирование названия узла в дереве и в подузлах
    /// </summary>
    /// <param name="_k"></param>
    /// <param name="_TNC"></param>
    /// <param name="_pline"></param>
    public void formTWNode(int _k, TreeNode _TNC, string _pline)
    {
        for (int i = 0; i < II.EIM[_k].Pzs.GetLength(0); i++)
        {
            string sd = _pline + "->" + i.ToString();
            formTWNode(
                _k + 1,
                _TNC.Nodes.Add("Эксперимент " + (_k + 1).ToString() + " исходы: " + sd),
                sd
                );
        }

    }
    /// <summary>
    /// Разобрать панели из памяти
    /// </summary>
    public new void disassembleContent()
    {
        base.disassembleContent();
        //for (int i = 0; i < this.Pnls.Length; i++)
        //    this.Pnls[i].SuspendLayout();
        //this.WB.SuspendLayout();

        //this.WB.ResumeLayout(false);
        //for (int i = 0; i < this.Pnls.Length; i++)
        //    this.Pnls[i].ResumeLayout(false);

        Button0 = null;
    }
    /// <summary>
    /// Перейти к панели по порядковому номеру
    /// </summary>
    /// <param name="_p"></param>
    public new void choosePanel(int _p)
    {
        base.choosePanel(_p);
    }
    /// <summary>
    /// Действие при изменении количества решений в списке
    /// </summary>
    /// <param name="sender">Объект списка</param>
    /// <param name="e"></param>
    private void ComboBox0_0_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (isChanging)
            return;
        isChanging = true;
        int DecisionsCount = (int)ComboBox0_0.SelectedItem;
        int oldDecisionsCount = II.NEI.L.GetLength(0);
        double[,] NM = new double[DecisionsCount, II.NEI.L.GetLength(1)];
        this.DataGridViewL.RowCount = DecisionsCount;
        Form1.formClientSizeOfDGV(this.DataGridViewL);
        if (DecisionsCount > oldDecisionsCount)
        {
            for (int i = 0; i < oldDecisionsCount; i++)
                for (int j = 0; j < II.NEI.L.GetLength(1); j++)
                    NM[i, j] = II.NEI.L[i, j];
            for (int i = oldDecisionsCount; i < DecisionsCount; i++)
                for (int j = 0; j < II.NEI.L.GetLength(1); j++)
                    NM[i, j] = 0.0;
            II.NEI.L = NM;

            for (int i = oldDecisionsCount; i < DecisionsCount; i++)
                for (int j = 0; j < II.NEI.L.GetLength(1); j++)
                {
                    DataGridViewL[j, i].ValueType = typeof(double);
                    DataGridViewL[j, i].Value = 0.0;
                }
        }
        else
        {
            for (int i = 0; i < NM.GetLength(0); i++)
                for (int j = 0; j < NM.GetLength(1); j++)
                    NM[i, j] = II.NEI.L[i, j];
            II.NEI.L = NM;
        }
        isChanging = false;
    }
    /// <summary>
    /// Действие при изменении количества состояний природы
    /// </summary>
    /// <param name="sender">Объект списка</param>
    /// <param name="e"></param>
    private void ComboBox0_1_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (isChanging)
            return;
        isChanging = true;
        int StatesCount = (int)ComboBox0_1.SelectedItem;
        int oldStatesCount = II.NEI.L.GetLength(1);
        double[,] NM = new double[II.NEI.L.GetLength(0), StatesCount];
        this.DataGridViewL.ColumnCount = StatesCount;
        Form1.formClientSizeOfDGV(this.DataGridViewL);
        this.DataGridViewP.ColumnCount = StatesCount;
        Form1.formClientSizeOfDGV(this.DataGridViewP);
        if (StatesCount > oldStatesCount)
        {//Если увеличили
            for (int i = 0; i < II.NEI.L.GetLength(0); i++)
            {
                for (int j = 0; j < oldStatesCount; j++)
                    NM[i, j] = II.NEI.L[i, j];
                for (int j = oldStatesCount; j < StatesCount; j++)
                    NM[i, j] = 0.0;
            }
            II.NEI.L = NM;

            for (int i = 0; i < II.NEI.L.GetLength(0); i++)
                for (int j = oldStatesCount; j < StatesCount; j++)
                {
                    DataGridViewL[j, i].ValueType = typeof(double);
                    DataGridViewL[j, i].Value = 0.0;
                }

            double[] NP = new double[StatesCount];
            for (int j = 0; j < oldStatesCount; j++)
                NP[j] = II.NEI.P[j];
            for (int j = oldStatesCount; j < StatesCount; j++)
                NP[j] = 0.0;
            II.NEI.P = NP;

            for (int j = oldStatesCount; j < StatesCount; j++)
            {
                DataGridViewL[j, 0].ValueType = typeof(double);
                DataGridViewP[j, 0].Value = 0.0;
            }

            for (int k = 0; k < II.EIM.Length; k++)
            {
                DataGridView DGVk = DataGridViewMXM[k];
                NM = new double[II.EIM[k].Pzs.GetLength(0), StatesCount];
                for (int i = 0; i < II.EIM[k].Pzs.GetLength(0); i++)
                {
                    for (int j = 0; j < oldStatesCount; j++)
                        NM[i, j] = II.EIM[k].Pzs[i, j];
                    for (int j = oldStatesCount; j < StatesCount; j++)
                        NM[i, j] = 1.0 / DGVk.RowCount;
                }
                II.EIM[k].Pzs = NM;
                DGVk.ColumnCount = StatesCount;
                for (int i = 0; i < II.EIM[k].Pzs.GetLength(0); i++)
                {
                    for (int j = oldStatesCount; j < StatesCount; j++)
                    {
                        DGVk[j, i].ValueType = typeof(double);
                        DGVk[j, i].Value = II.EIM[k].Pzs[i, j];
                    }
                }
            }
        }
        else
        {//Если уменьшили
            for (int i = 0; i < NM.GetLength(0); i++)
                for (int j = 0; j < NM.GetLength(1); j++)
                    NM[i, j] = II.NEI.L[i, j];
            II.NEI.L = NM;

            double[] NP = new double[StatesCount];
            double nps = 0.0;
            for (int j = StatesCount; j < oldStatesCount; j++)
                nps += II.NEI.P[j];
            nps /= StatesCount;
            for (int j = 0; j < StatesCount; j++)
            {
                NP[j] = II.NEI.P[j];
                NP[j] += nps;
            }
            II.NEI.P = NP;


            for (int j = 0; j < StatesCount; j++)
                DataGridViewP[j, 0].Value = II.NEI.P[j];

            for (int k = 0; k < II.EIM.Length; k++)
            {
                DataGridView DGVk = DataGridViewMXM[k];

                NM = new double[II.EIM[k].Pzs.GetLength(0), StatesCount];
                for (int i = 0; i < II.EIM[k].Pzs.GetLength(0); i++)
                    for (int j = 0; j < StatesCount; j++)
                        NM[i, j] = II.EIM[k].Pzs[i, j];
                II.EIM[k].Pzs = NM;

                DGVk.ColumnCount = StatesCount;
                //for (int i = 0; i < II.EIM[k].Pzs.GetLength(0); i++)
                    //for (int j = 0; j < StatesCount; j++)
                        //DGVk[j, i].Value = II.EIM[k].Pzs[i,j];
            }
        }
        isChanging = false;
    }



    public TabPage createExpPage(int _k, String _text)
    {
        TabPage R = new TabPage(_text);

        Label0_MXM[_k] = new Label();
        Label0_MXM[_k].AutoSize = true;
        Label0_MXM[_k].Location = NPos(0, 0);
        Label0_MXM[_k].Name = "Label0_MXM" + _k.ToString();
        Label0_MXM[_k].Size = new Size(35, 13);
        Label0_MXM[_k].TabIndex = 0;
        Label0_MXM[_k].Text = "Матрица уcловных вероятностей";

        DataGridViewMXM[_k] = new DataGridView();
        DataGridViewMXM[_k].ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        DataGridViewMXM[_k].Location = NPos(0, 1);
        DataGridViewMXM[_k].Name = "DataGridViewMXM" + _k.ToString();
        DataGridViewMXM[_k].Size = new Size(450, 120);
        DataGridViewMXM[_k].TabIndex = 1;
        DataGridViewMXM[_k].AllowUserToAddRows = false;
        DataGridViewMXM[_k].AllowUserToDeleteRows = false;
        DataGridViewMXM[_k].RowCount = II.EIM[_k].Pzs.GetLength(0);
        DataGridViewMXM[_k].ColumnCount = II.EIM[_k].Pzs.GetLength(1);
        for (int i = 0; i < II.EIM[_k].Pzs.GetLength(0); i++)
            for (int j = 0; j < II.EIM[_k].Pzs.GetLength(1); j++)
            {
                DataGridViewMXM[_k][j, i].ValueType = typeof(double);
                DataGridViewMXM[_k][j, i].Value = II.EIM[_k].Pzs[i, j];
            }
        Form1.formClientSizeOfDGV(DataGridViewMXM[_k]);
        DataGridViewMXM[_k].CellValueChanged += this.DataGridViewMXM_k_CellValueChanged;//new DataGridViewCellEventHandler()


        Label0_Mb0[_k] = new Label();
        Label0_Mb0[_k].AutoSize = true;
        Label0_Mb0[_k].Location = NPos(0, 6);
        Label0_Mb0[_k].Name = "Label0_Mb0" + _k.ToString();
        Label0_Mb0[_k].Size = new Size(35, 13);
        Label0_Mb0[_k].TabIndex = 2;
        Label0_Mb0[_k].Text = "Область продолжения (минимум)";

        Label0_Mb1[_k] = new Label();
        Label0_Mb1[_k].AutoSize = true;
        Label0_Mb1[_k].Location = NPos(0, 8);
        Label0_Mb1[_k].Name = "Label0_Mb1" + _k.ToString();
        Label0_Mb1[_k].Size = new Size(35, 13);
        Label0_Mb1[_k].TabIndex = 3;
        Label0_Mb1[_k].Text = "Область продолжения (максимум)";

        TrackBar0_Mb0[_k] = new TrackBar();
        TrackBar0_Mb0[_k].Location = NPos(2, 6);
        TrackBar0_Mb0[_k].Name = "TrackBar0_Mb0" + _k.ToString();
        TrackBar0_Mb0[_k].Size = new Size(100, 42);
        TrackBar0_Mb0[_k].TabIndex = 4;
        TrackBar0_Mb0[_k].Minimum = 0;
        TrackBar0_Mb0[_k].Maximum = 10;
        TrackBar0_Mb0[_k].TickFrequency = 1;
        TrackBar0_Mb0[_k].Value = (int)(II.EIM[_k].b[0] * TrackBar0_Mb0[_k].Maximum);
        TrackBar0_Mb0[_k].Scroll += TrackBar0_Mb0_k_Scroll;

        TrackBar0_Mb1[_k] = new TrackBar();
        TrackBar0_Mb1[_k].Location = NPos(2, 8);
        TrackBar0_Mb1[_k].Name = "TrackBar0_Mb1" + _k.ToString();
        TrackBar0_Mb1[_k].Size = new Size(100, 42);
        TrackBar0_Mb1[_k].TabIndex = 5;
        TrackBar0_Mb1[_k].Minimum = 0;
        TrackBar0_Mb1[_k].Maximum = 10;
        TrackBar0_Mb1[_k].TickFrequency = 1;
        TrackBar0_Mb1[_k].Value = (int)(II.EIM[_k].b[1] * TrackBar0_Mb1[_k].Maximum);
        TrackBar0_Mb1[_k].Scroll += TrackBar0_Mb1_k_Scroll;

        TextBox0_Mb0[_k] = new TextBox();
        TextBox0_Mb0[_k].Location = NPos(3, 6);
        TextBox0_Mb0[_k].Name = "TextBox0_Mb0" + _k.ToString();
        TextBox0_Mb0[_k].Size = new Size(100, 20);
        TextBox0_Mb0[_k].TabIndex = 6;
        TextBox0_Mb0[_k].Text = II.EIM[_k].b[0].ToString();
        TextBox0_Mb0[_k].Enabled = false;

        TextBox0_Mb1[_k] = new TextBox();
        TextBox0_Mb1[_k].Location = NPos(3, 8);
        TextBox0_Mb1[_k].Name = "TextBox0_Mb1" + _k.ToString();
        TextBox0_Mb1[_k].Size = new Size(100, 20);
        TextBox0_Mb1[_k].TabIndex = 7;
        TextBox0_Mb1[_k].Text = II.EIM[_k].b[1].ToString();
        TextBox0_Mb1[_k].Enabled = false;

        Label0_Mс[_k] = new Label();
        Label0_Mс[_k].AutoSize = true;
        Label0_Mс[_k].Location = NPos(0, 10);
        Label0_Mс[_k].Name = "Label0_Mс" + _k.ToString();
        Label0_Mс[_k].Size = new Size(35, 13);
        Label0_Mс[_k].TabIndex = 8;
        Label0_Mс[_k].Text = "Стоимость эксперимента";

        NumericUpDown0_Mс[_k] = new NumericUpDown();
        NumericUpDown0_Mс[_k].Location = NPos(2, 10);
        NumericUpDown0_Mс[_k].Name = "NumericUpDown0_Mс" + _k.ToString();
        NumericUpDown0_Mс[_k].Size = new Size(100, 20);
        NumericUpDown0_Mс[_k].TabIndex = 9;
        NumericUpDown0_Mс[_k].Value = (decimal)II.EIM[_k].c;
        NumericUpDown0_Mс[_k].Minimum = 0;
        NumericUpDown0_Mс[_k].ValueChanged += this.NumericUpDown0_Mс_k_ValueChanged;

        Label0_Mz[_k] = new Label();
        Label0_Mz[_k].AutoSize = true;
        Label0_Mz[_k].Location = NPos(5, 0);
        Label0_Mz[_k].Name = "Label0_Mz" + _k.ToString();
        Label0_Mz[_k].Size = new Size(35, 13);
        Label0_Mz[_k].TabIndex = 10;
        Label0_Mz[_k].Text = "Количество исходов";

        ComboBox0_Mz[_k] = new ComboBox();
        ComboBox0_Mz[_k].DropDownWidth = 100;
        ComboBox0_Mz[_k].Items.AddRange(new object[] { 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        ComboBox0_Mz[_k].DropDownStyle = ComboBoxStyle.DropDownList;
        ComboBox0_Mz[_k].SelectedIndex = II.EIM[_k].Pzs.GetLength(0) - 2;
        ComboBox0_Mz[_k].Location = NPos(5, 1);
        ComboBox0_Mz[_k].Name = "ComboBox0_Mz" + _k.ToString();
        ComboBox0_Mz[_k].Size = new Size(100, 21);
        ComboBox0_Mz[_k].TabIndex = 11;
        ComboBox0_Mz[_k].SelectedIndexChanged += ComboBox0_Mz_k_SelectedIndexChanged;

        R.Controls.AddRange(new Control[]
        {
            Label0_MXM[_k], DataGridViewMXM[_k],
            Label0_Mb0[_k], Label0_Mb1[_k],
            TrackBar0_Mb0[_k], TrackBar0_Mb1[_k],
            TextBox0_Mb0[_k], TextBox0_Mb1[_k],
            Label0_Mс[_k], NumericUpDown0_Mс[_k],
            Label0_Mz[_k], ComboBox0_Mz[_k]
        });

        return R;
    }
    /// <summary>
    /// Действие при изменении количества экспериментов
    /// </summary>
    /// <param name="sender">Объект списка</param>
    /// <param name="e"></param>
    private void ComboBox0_2_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (isChanging)
            return;
        isChanging = true;
        int ExperimentsCount = (int)ComboBox0_2.SelectedItem;
        int oldExperimentsCount = DataGridViewMXM.Length;

        InputInfo.ExpInputInfo[] IIEIM = new InputInfo.ExpInputInfo[ExperimentsCount];//II.EIM

        TabPage[] TabPageM2 = new TabPage[ExperimentsCount];
        Label[] Label0_MXM2 = new Label[ExperimentsCount];
        DataGridView[] DataGridViewMXM2 = new DataGridView[ExperimentsCount];
        Label[] Label0_Mb02 = new Label[ExperimentsCount];
        Label[] Label0_Mb12 = new Label[ExperimentsCount];
        TrackBar[] TrackBar0_Mb02 = new TrackBar[ExperimentsCount];
        TrackBar[] TrackBar0_Mb12 = new TrackBar[ExperimentsCount];
        TextBox[] TextBox0_Mb02 = new TextBox[ExperimentsCount];
        TextBox[] TextBox0_Mb12 = new TextBox[ExperimentsCount];
        Label[] Label0_Mс2 = new Label[ExperimentsCount];
        NumericUpDown[] NumericUpDown0_Mс2 = new NumericUpDown[ExperimentsCount];
        Label[] Label0_Mz2 = new Label[ExperimentsCount];
        ComboBox[] ComboBox0_Mz2 = new ComboBox[ExperimentsCount];

        for (int k = 0; k < oldExperimentsCount; k++)
        {
            TabPageM2[k] = TabPageM[k];

            IIEIM[k] = II.EIM[k];
            Label0_MXM2[k] = Label0_MXM[k];
            DataGridViewMXM2[k] = DataGridViewMXM[k];
            Label0_Mb02[k] = Label0_Mb0[k];
            Label0_Mb12[k] = Label0_Mb1[k];
            TextBox0_Mb02[k] = TextBox0_Mb0[k];
            TextBox0_Mb12[k] = TextBox0_Mb1[k];
            TrackBar0_Mb02[k] = TrackBar0_Mb0[k];
            TrackBar0_Mb12[k] = TrackBar0_Mb1[k];
            Label0_Mс2[k] = Label0_Mс[k];
            NumericUpDown0_Mс2[k] = NumericUpDown0_Mс[k];
            Label0_Mz2[k] = Label0_Mz[k];
            ComboBox0_Mz2[k] = ComboBox0_Mz[k];
        }

        II.EIM = IIEIM;
        Label0_MXM = Label0_MXM2;
        DataGridViewMXM = DataGridViewMXM2;
        Label0_Mb0 = Label0_Mb02;
        Label0_Mb1 = Label0_Mb12;
        TextBox0_Mb0 = TextBox0_Mb02;
        TextBox0_Mb1 = TextBox0_Mb12;
        TrackBar0_Mb0 = TrackBar0_Mb02;
        TrackBar0_Mb1 = TrackBar0_Mb12;
        Label0_Mс = Label0_Mс2;
        NumericUpDown0_Mс = NumericUpDown0_Mс2;
        Label0_Mz = Label0_Mz2;
        ComboBox0_Mz = ComboBox0_Mz2;

        if (ExperimentsCount > oldExperimentsCount)
        {//Если увеличили
            for (int k = oldExperimentsCount; k < ExperimentsCount; k++)
            {
                II.EIM[k] = new InputInfo.ExpInputInfo();
                II.EIM[k].Pzs = new double[II.NEI.L.GetLength(0), II.NEI.L.GetLength(1)];
                double v1 = 1.0 / II.NEI.L.GetLength(0);
                for (int i = 0; i < II.NEI.L.GetLength(0); i++)
                    for (int j = 0; j < II.NEI.L.GetLength(1); j++)
                        II.EIM[k].Pzs[i, j] = v1;
                II.EIM[k].b = new double[2];
                II.EIM[k].b[0] = 0.0;
                II.EIM[k].b[1] = 1.0;
                II.EIM[k].c = 0.0;
                TabPageM2[k] = createExpPage(k, "Эксперимент " + k.ToString());
                TabControl0.Controls.Add(TabPageM2[k]);
            }
        }
        else
        {//Если уменьшили
            for (int k = oldExperimentsCount; k < ExperimentsCount; k++)
                TabControl0.Controls.Remove(TabPageM[k]);
        }
        TabPageM = TabPageM2;
        isChanging = false;
    }
    private void DataGridViewL_CellValueChanged(object sender, DataGridViewCellEventArgs e)
    {
        if (isChanging)
            return;
        isChanging = true;
        //this.DataGridViewL.SuspendLayout();
        if (ComboBox0_3.SelectedIndex == 1)
            this.II.NEI.L[e.RowIndex, e.ColumnIndex] = -(double)this.DataGridViewL[e.ColumnIndex, e.RowIndex].Value;
        else
            this.II.NEI.L[e.RowIndex, e.ColumnIndex] = (double)this.DataGridViewL[e.ColumnIndex, e.RowIndex].Value;
        //this.DataGridViewL.ResumeLayout(false);
        isChanging = false;
    }
    private void DataGridViewP_CellValueChanged(object sender, DataGridViewCellEventArgs e)
    {
        if (isChanging)
            return;
        isChanging = true;
        //this.DataGridViewL.SuspendLayout();
        this.II.NEI.P[e.ColumnIndex] = (double)this.DataGridViewL[e.ColumnIndex, e.RowIndex].Value;
        //this.DataGridViewL.ResumeLayout(false);
        isChanging = false;
    }
    private void DataGridViewMXM_k_CellValueChanged(object sender, DataGridViewCellEventArgs e)
    {
        if (isChanging)
            return;
        isChanging = true;
        DataGridView DGV = (DataGridView)sender;
        int kc = 0;
        for (int k = 0; k < DataGridViewMXM.Length; k++)
            if (DataGridViewMXM[k] == DGV)
            {
                kc = k;
                break;
            }
        II.EIM[kc].Pzs[e.RowIndex, e.ColumnIndex] = (double)DGV[e.ColumnIndex, e.RowIndex].Value;
        isChanging = false;
    }
    private void TrackBar0_Mb0_k_Scroll(object sender, EventArgs e)
    {
        if (isChanging)
            return;
        isChanging = true;
        TrackBar TB = (TrackBar)sender;
        int kc = 0;
        for (int k = 0; k < TrackBar0_Mb0.Length; k++)
            if (TrackBar0_Mb0[k] == TB)
            {
                kc = k;
                break;
            }
        double vl = (double)TB.Value/TB.Maximum;
        if (vl <= II.EIM[kc].b[1])
        {
            II.EIM[kc].b[0] = vl;
            TextBox0_Mb0[kc].Text = vl.ToString();
        }
        else
            TB.Value = (int)(II.EIM[kc].b[0] * TB.Maximum);
        isChanging = false;
    }
    private void TrackBar0_Mb1_k_Scroll(object sender, EventArgs e)
    {
        if (isChanging)
            return;
        isChanging = true;
        TrackBar TB = (TrackBar)sender;
        int kc = -1;
        for (int k = 0; k < TrackBar0_Mb1.Length; k++)
            if (TrackBar0_Mb1[k] == TB)
            {
                kc = k;
                break;
            }
        double vl = (double)TB.Value / TB.Maximum;
        if (vl >= II.EIM[kc].b[0])
        {
            II.EIM[kc].b[1] = vl;
            TextBox0_Mb1[kc].Text = vl.ToString();
        }
        else
            TB.Value = (int)(II.EIM[kc].b[1] * TB.Maximum);
        isChanging = false;
    }
    private void NumericUpDown0_Mс_k_ValueChanged(object sender, EventArgs e)
    {
        if (isChanging)
            return;
        isChanging = true;
        NumericUpDown NUD = (NumericUpDown)sender;
        int kc = 0;
        for (int k = 0; k < NumericUpDown0_Mс.Length; k++)
            if (NumericUpDown0_Mс[k] == NUD)
            {
                kc = k;
                break;
            }
        double vl = (double)NUD.Value;
        if (vl >= 0.0)
            II.EIM[kc].c = vl;
        else
            NUD.Value = (decimal)II.EIM[kc].c;
        isChanging = false;
    }
    /// <summary>
    /// Действие при изменения выбора использования значений потерь
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ComboBox0_3_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (isChanging)
            return;
        isChanging = true;
        //uDT.win = (((ComboBox)sender).SelectedIndex == 0);
        for (int i = 0; i < II.NEI.L.GetLength(0); i++)
            for (int j = 0; j < II.NEI.L.GetLength(1); j++)
                if (ComboBox0_3.SelectedIndex == 0)
                    DataGridViewL[j, i].Value = II.NEI.L[i, j];
                else
                    DataGridViewL[j, i].Value = -II.NEI.L[i, j];
        isChanging = false;
    }
    /// <summary>
    /// Действие при изменении критерия
    /// </summary>
    /// <param name="sender">Объект списка</param>
    /// <param name="e"></param>
    private void ComboBox0_4_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (isChanging)
            return;
        isChanging = true;

        uDT.C_criteria = ComboBox0_4.SelectedIndex;

        if (isCrAndPCorrect())
            CheckBox0_1.Text = txt_r;
        else
            CheckBox0_1.Text = txt_w;

        isChanging = false;
    }
    /// <summary>
    /// Действие при изменении количества исходов эксперимента
    /// </summary>
    /// <param name="sender">Объект списка</param>
    /// <param name="e"></param>
    private void ComboBox0_Mz_k_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (isChanging)
            return;
        isChanging = true;

        ComboBox CB = (ComboBox)sender;
        int kc = 0;
        for (int k = 0; k < ComboBox0_Mz.Length; k++)
            if (ComboBox0_Mz[k] == CB)
            {
                kc = k;
                break;
            }

        int ResultsCount = (int)ComboBox0_Mz[kc].SelectedItem;
        int oldResultsCount = II.EIM[kc].Pzs.GetLength(0);
        double[,] NM = new double[ResultsCount, II.EIM[kc].Pzs.GetLength(1)];
        DataGridViewMXM[kc].RowCount = ResultsCount;
        if (ResultsCount > oldResultsCount)
        {//Если увеличили
            for (int j = 0; j < II.EIM[kc].Pzs.GetLength(1); j++)
            {
                for (int i = 0; i < oldResultsCount; i++)
                    NM[i, j] = II.EIM[kc].Pzs[i, j];
                for (int i = oldResultsCount; i < ResultsCount; i++)
                    NM[i, j] = 0.0;
            }
            II.EIM[kc].Pzs = NM;

            for (int i = oldResultsCount; i < ResultsCount; i++)
                for (int j = 0; j < II.NEI.L.GetLength(1); j++)
                {
                    DataGridViewMXM[kc][j, i].ValueType = typeof(double);
                    DataGridViewMXM[kc][j, i].Value = 0.0;
                }
        }
        else
        {//Если уменьшили
            for (int j = 0; j < II.EIM[kc].Pzs.GetLength(1); j++)
            {
                double nps = 0.0;
                for (int i = ResultsCount; i < oldResultsCount; i++)
                    nps += II.EIM[kc].Pzs[i, j];
                nps /= ResultsCount;
                for (int i = 0; i < ResultsCount; i++)
                {
                    NM[i, j] = II.EIM[kc].Pzs[i, j];
                    NM[i, j] += nps;
                }
            }
            II.EIM[kc].Pzs = NM;

            for (int i = 0; i < II.EIM[kc].Pzs.GetLength(0); i++)
                for (int j = 0; j < II.EIM[kc].Pzs.GetLength(1); j++)
                    DataGridViewMXM[kc][j, i].Value = II.EIM[kc].Pzs[i, j];
        }
        isChanging = false;
    }
    private void CheckBox0_1_CheckedChanged(object sender, EventArgs e)
    {
        if (isChanging)
            return;
        isChanging = true;

        if (isCrAndPCorrect())
            CheckBox0_1.Text = txt_r;
        else
            CheckBox0_1.Text = txt_w;

        isChanging = false;
    }
    public void Button0Click(object sender, EventArgs e)
    {
        if (isChanging)
            return;
        isChanging = true;
        randomiseInputInfo();
        for (int i = 0; i < II.NEI.L.GetLength(0); i++)
            for (int j = 0; j < II.NEI.L.GetLength(1); j++)
                DataGridViewL[j, i].Value = II.NEI.L[i, j];
        for (int j = 0; j < II.NEI.P.GetLength(0); j++)
            DataGridViewP[j, 0].Value = II.NEI.P[j];
        for (int k = 0; k < this.DataGridViewMXM.Length; k++)
        {
            for (int i = 0; i < II.EIM[k].Pzs.GetLength(0); i++)
                for (int j = 0; j < II.EIM[k].Pzs.GetLength(1); j++)
                    DataGridViewMXM[k][j, i].Value = II.EIM[k].Pzs[i, j];
            TrackBar0_Mb0[k].Value = (int)(II.EIM[k].b[0] * 10);
            TrackBar0_Mb1[k].Value = (int)(II.EIM[k].b[1] * 10);
            TextBox0_Mb0[k].Text = II.EIM[k].b[0].ToString();
            TextBox0_Mb1[k].Text = II.EIM[k].b[1].ToString();
            NumericUpDown0_Mс[k].Value = (decimal)(II.EIM[k].c);
        }
        isChanging = false;
    }
    public void Button1Click(object sender, EventArgs e)
    {
        if (isChanging)
            return;
        isChanging = true;
        //Загрузка из файла (определить в дальнейшем)
        isChanging = false;
    }
    public void Button2Click(object sender, EventArgs e)
    {
        if (isChanging)
            return;
        isChanging = true;
        if (!isCrAndPCorrect())
            MessageBox.Show("Для использования критерия необходимо распределение вероятностей!");
        else
            this.choosePanel(1);
        isChanging = false;
    }
}
*/

//public class TNEPcontainer
//{
//    public class TNEP
//    {
//        public TreeNode TN;
//        public ExpPanel EP;
//        public TNEP(TreeNode _TN, ExpPanel _EP)
//        {
//            TN = _TN;
//            EP = _EP;
//        }
//    }
//    public TNEP[] TNEPM;
//    public TNEPcontainer()
//    {
//        TNEPM = new TNEP[0];
//    }
//    public int count()
//    {
//        return TNEPM.Length;
//    }
//    public bool Add(TreeNode _TN, ExpPanel _EP)
//    {
//        TNEP[] TNEPM2 = new TNEP[TNEPM.Length + 1];
//        for (int i = 0; i < TNEPM.Length; i++)
//            TNEPM2[i] = TNEPM[i];
//        TNEPM2[TNEPM.Length] = new TNEP(_TN, _EP);
//        TNEPM = TNEPM2;
//        return true;
//    }
//    public bool Change(TreeNode _TN, ExpPanel _EP)
//    {
//        for (int i = 0; i < TNEPM.Length; i++)
//            if (TNEPM[i].TN == _TN)
//            {
//                TNEPM[i].EP = _EP;
//                return true;
//            }
//        return false;
//    }
//    public bool Change(ExpPanel _EP, TreeNode _TN)
//    {
//        for (int i = 0; i < TNEPM.Length; i++)
//            if (TNEPM[i].EP == _EP)
//            {
//                TNEPM[i].TN = _TN;
//                return true;
//            }
//        return false;
//    }
//    public ExpPanel Find(TreeNode _TN)
//    {
//        for (int i = 0; i < TNEPM.Length; i++)
//            if (TNEPM[i].TN == _TN)
//                return TNEPM[i].EP;
//        return null;
//    }
//    public TreeNode Find(ExpPanel _EP)
//    {
//        for (int i = 0; i < TNEPM.Length; i++)
//            if (TNEPM[i].EP == _EP)
//                return TNEPM[i].TN;
//        return null;
//    }
//    public ExpPanel Remove(TreeNode _TN)
//    {
//        for (int i = 0; i < TNEPM.Length; i++)
//            if (TNEPM[i].TN == _TN)
//            {
//                TNEP[] TNEPM2 = new TNEP[TNEPM.Length - 1];
//                for (int j = 0; j < i; j++)
//                    TNEPM2[j] = TNEPM[j];
//                for (int j = i + 1; j < TNEPM2.Length; j++)
//                    TNEPM2[j] = TNEPM[j + 1];
//                return TNEPM[i].EP;
//            }
//        return null;
//    }
//    public TreeNode Remove(ExpPanel _EP)
//    {
//        for (int i = 0; i < TNEPM.Length; i++)
//            if (TNEPM[i].EP == _EP)
//            {
//                TNEP[] TNEPM2 = new TNEP[TNEPM.Length - 1];
//                for (int j = 0; j < i; j++)
//                    TNEPM2[j] = TNEPM[j];
//                for (int j = i + 1; j < TNEPM2.Length; j++)
//                    TNEPM2[j] = TNEPM[j + 1];
//                return TNEPM[i].TN;
//            }
//        return null;
//    }
//    public TreeNode[] RemoveArray(ExpPanel[] _EPM)
//    {
//        if (_EPM == null)
//            return null;

//        TreeNode[] R = new TreeNode[_EPM.Length];
//        int Xi = 0;
//        for (int i = 0; i < _EPM.Length; i++)
//        {
//            R[i] = null;
//            for (int j = 0; j < TNEPM.Length; j++)
//                if (TNEPM[j].EP == _EPM[i])
//                {
//                    R[i] = TNEPM[j].TN;
//                    TNEPM[j] = null;
//                    Xi++;
//                    break;
//                }
//        }
//        TNEP[] TNEPM2 = new TNEP[TNEPM.Length - Xi];
//        Xi = 0;
//        for (int i = 0; i < TNEPM.Length; i++)
//            if (TNEPM[i] != null)
//            {
//                TNEPM2[Xi] = TNEPM[i];
//                Xi++;
//            }
//        TNEPM = TNEPM2;
//        return R;
//    }
//    public ExpPanel[] RemoveArray(TreeNode[] _TNM)
//    {
//        if (_TNM == null)
//            return null;

//        ExpPanel[] R = new ExpPanel[_TNM.Length];
//        int Xi = 0;
//        for (int i = 0; i < _TNM.Length; i++)
//        {
//            R[i] = null;
//            for (int j = 0; j < TNEPM.Length; j++)
//                if (TNEPM[j].TN == _TNM[i])
//                {
//                    R[i] = TNEPM[j].EP;
//                    TNEPM[j] = null;
//                    Xi++;
//                    break;
//                }
//        }
//        TNEP[] TNEPM2 = new TNEP[TNEPM.Length - Xi];
//        Xi = 0;
//        for (int i = 0; i < TNEPM.Length; i++)
//            if (TNEPM[i] != null)
//            {
//                TNEPM2[Xi] = TNEPM[i];
//                Xi++;
//            }
//        TNEPM = TNEPM2;
//        return R;
//    }
//    public TNEP[] RemoveAll()
//    {
//        TNEP[] R = TNEPM;
//        TNEPM = new TNEP[0];
//        return R;
//    }
//}